package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.ValidateTokenRequest;
import com.addcel.mch2hpayment.client.model.ValidateTokenResponse;

public interface TokenClientMC {
	ValidateTokenResponse validateToken(ValidateTokenRequest paramValidateTokenRequest, Integer paramInteger1,
			Integer paramInteger2, String paramString1, String paramString2);
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/TokenClientMC.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */