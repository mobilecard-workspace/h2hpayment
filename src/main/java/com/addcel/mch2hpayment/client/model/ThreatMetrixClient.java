package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.ThreatMetrixRequest;
import com.addcel.mch2hpayment.client.model.ThreatMetrixResponse;

public interface ThreatMetrixClient {
	ThreatMetrixResponse investigatePayment(ThreatMetrixRequest paramThreatMetrixRequest);
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/ThreatMetrixClient.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */