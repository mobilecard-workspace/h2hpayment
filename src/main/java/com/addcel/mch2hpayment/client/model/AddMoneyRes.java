package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.DataPreviVale;

public class AddMoneyRes {
	private Integer code;
	private String message;
	DataPreviVale data;

	public AddMoneyRes() {
	}

	public AddMoneyRes(Integer code) {
		/* 14 */ this.code = code;
	}

	public Integer getCode() {
		/* 18 */ return this.code;
	}

	public void setCode(Integer code) {
		/* 22 */ this.code = code;
	}

	public String getMessage() {
		/* 26 */ return this.message;
	}

	public void setMessage(String message) {
		/* 30 */ this.message = message;
	}

	public DataPreviVale getData() {
		/* 34 */ return this.data;
	}

	public void setData(DataPreviVale data) {
		/* 38 */ this.data = data;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/AddMoneyRes.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */