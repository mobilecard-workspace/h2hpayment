package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.ServiceResult;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogoutResponse {
	private ServiceResult result;

	public void setResult(ServiceResult result) {
		/* 15 */ this.result = result;
	}

	public ServiceResult getResult() {
		/* 19 */ return this.result;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/LogoutResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */