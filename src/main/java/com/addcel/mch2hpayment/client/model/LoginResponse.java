package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.ServiceResult;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {
	private ServiceResult result;
	private String token;

	public void setResult(ServiceResult result) {
		/* 16 */ this.result = result;
	}

	public ServiceResult getResult() {
		/* 20 */ return this.result;
	}

	public void setToken(String token) {
		/* 24 */ this.token = token;
	}

	public String getToken() {
		/* 28 */ return this.token;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/LoginResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */