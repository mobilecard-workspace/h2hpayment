package com.addcel.mch2hpayment.client.model;

public class AmexAutorization {
	private double monto;
	private double comision;
	private String tarjeta;
	private String vigencia;
	private String cvv2;
	private String dom_amex;
	private String afiliacion;
	private String producto;

	public double getMonto() {
		/* 19 */ return this.monto;
	}

	public void setMonto(double monto) {
		/* 23 */ this.monto = monto;
	}

	public double getComision() {
		/* 27 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 31 */ this.comision = comision;
	}

	public String getTarjeta() {
		/* 35 */ return this.tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		/* 39 */ this.tarjeta = tarjeta;
	}

	public String getVigencia() {
		/* 43 */ return this.vigencia;
	}

	public void setVigencia(String vigencia) {
		/* 47 */ this.vigencia = vigencia;
	}

	public String getCvv2() {
		/* 51 */ return this.cvv2;
	}

	public void setCvv2(String cvv2) {
		/* 55 */ this.cvv2 = cvv2;
	}

	public String getDom_amex() {
		/* 59 */ return this.dom_amex;
	}

	public void setDom_amex(String dom_amex) {
		/* 63 */ this.dom_amex = dom_amex;
	}

	public String getAfiliacion() {
		/* 67 */ return this.afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		/* 71 */ this.afiliacion = afiliacion;
	}

	public String getProducto() {
		/* 75 */ return this.producto;
	}

	public void setProducto(String producto) {
		/* 79 */ this.producto = producto;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/AmexAutorization.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */