package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.MoneySendReq;
import com.addcel.mch2hpayment.client.model.MoneySendRes;

public interface STPClient {
	MoneySendRes moneySend(MoneySendReq paramMoneySendReq, int paramInt1, int paramInt2, String paramString);
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/STPClient.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */