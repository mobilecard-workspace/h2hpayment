package com.addcel.mch2hpayment.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountRecord {
	private String holderName;
	private String rfc;
	private String phone;
	private String contact;
	private String email;
	private String actType;
	private String bankCode;
	private String actNumber;

	public String getHolderName() {
		/* 26 */ return this.holderName;
	}

	public void setHolderName(String holderName) {
		/* 35 */ this.holderName = holderName;
	}

	public String getRfc() {
		/* 43 */ return this.rfc;
	}

	public void setRfc(String rfc) {
		/* 53 */ this.rfc = rfc;
	}

	public String getPhone() {
		/* 61 */ return this.phone;
	}

	public void setPhone(String phone) {
		/* 72 */ this.phone = phone;
	}

	public String getContact() {
		/* 80 */ return this.contact;
	}

	public void setContact(String contact) {
		/* 92 */ this.contact = contact;
	}

	public String getEmail() {
		/* 100 */ return this.email;
	}

	public void setEmail(String email) {
		/* 110 */ this.email = email;
	}

	public String getActType() {
		/* 118 */ return this.actType;
	}

	public void setActType(String actType) {
		/* 129 */ this.actType = actType;
	}

	public String getBankCode() {
		/* 137 */ return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		/* 147 */ this.bankCode = bankCode;
	}

	public String getActNumber() {
		/* 155 */ return this.actNumber;
	}

	public void setActNumber(String actNumber) {
		/* 166 */ this.actNumber = actNumber;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/AccountRecord.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */