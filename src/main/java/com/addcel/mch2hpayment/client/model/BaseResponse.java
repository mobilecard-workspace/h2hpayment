package com.addcel.mch2hpayment.client.model;

public class BaseResponse {
	private Integer code;
	private String message;

	public Integer getCode() {
		/* 14 */ return this.code;
	}

	public void setCode(Integer code) {
		/* 18 */ this.code = code;
	}

	public String getMessage() {
		/* 22 */ return this.message;
	}

	public void setMessage(String message) {
		/* 26 */ this.message = message;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/BaseResponse.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */