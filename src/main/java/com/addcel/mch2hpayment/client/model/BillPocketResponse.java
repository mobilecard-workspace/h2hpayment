package com.addcel.mch2hpayment.client.model;

public class BillPocketResponse {
	private Integer status;
	private Integer opId;
	private String txnISOCode;
	private String authNumber;
	private String ticketUrl;
	private double amount;
	private String maskedPAN;
	private long dateTime;
	private long idTransaccion;
	private Integer code;
	private String message;

	public Integer getCode() {
		/* 35 */ return this.code;
	}

	public void setCode(Integer code) {
		/* 41 */ this.code = code;
	}

	public String getMessage() {
		/* 47 */ return this.message;
	}

	public void setMessage(String message) {
		/* 53 */ this.message = message;
	}

	public Integer getStatus() {
		/* 59 */ return this.status;
	}

	public void setStatus(Integer status) {
		/* 63 */ this.status = status;
	}

	public Integer getOpId() {
		/* 67 */ return this.opId;
	}

	public void setOpId(Integer opId) {
		/* 71 */ this.opId = opId;
	}

	public String getTxnISOCode() {
		/* 75 */ return this.txnISOCode;
	}

	public void setTxnISOCode(String txnISOCode) {
		/* 79 */ this.txnISOCode = txnISOCode;
	}

	public String getAuthNumber() {
		/* 83 */ return this.authNumber;
	}

	public void setAuthNumber(String authNumber) {
		/* 87 */ this.authNumber = authNumber;
	}

	public String getTicketUrl() {
		/* 91 */ return this.ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		/* 95 */ this.ticketUrl = ticketUrl;
	}

	public double getAmount() {
		/* 99 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 103 */ this.amount = amount;
	}

	public String getMaskedPAN() {
		/* 107 */ return this.maskedPAN;
	}

	public void setMaskedPAN(String maskedPAN) {
		/* 111 */ this.maskedPAN = maskedPAN;
	}

	public long getDateTime() {
		/* 115 */ return this.dateTime;
	}

	public void setDateTime(long dateTime) {
		/* 119 */ this.dateTime = dateTime;
	}

	public long getIdTransaccion() {
		/* 123 */ return this.idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		/* 127 */ this.idTransaccion = idTransaccion;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/BillPocketResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */