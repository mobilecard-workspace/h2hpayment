package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.ServiceResult;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnqueueTransactionResponse {
	private ServiceResult result;
	private String clientReference;
	private String transactionReference;

	public ServiceResult getResult() {
		/* 17 */ return this.result;
	}

	public void setResult(ServiceResult result) {
		/* 21 */ this.result = result;
	}

	public String getClientReference() {
		/* 25 */ return this.clientReference;
	}

	public void setClientReference(String clientReference) {
		/* 29 */ this.clientReference = clientReference;
	}

	public String getTransactionReference() {
		/* 33 */ return this.transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		/* 37 */ this.transactionReference = transactionReference;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/EnqueueTransactionResponse.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */