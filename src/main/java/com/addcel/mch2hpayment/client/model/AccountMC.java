package com.addcel.mch2hpayment.client.model;

public class AccountMC {
	private long id;
	private String holder_name;
	private String rfc;
	private String phone;
	private String contact;
	private String email;
	private String act_type;
	private String bank_code;
	private String act_number;
	private String banorte_clave_id;
	private int status;

	public long getId() {
		/* 22 */ return this.id;
	}

	public void setId(long id) {
		/* 26 */ this.id = id;
	}

	public String getHolder_name() {
		/* 30 */ return this.holder_name;
	}

	public void setHolder_name(String holder_name) {
		/* 34 */ this.holder_name = holder_name;
	}

	public String getRfc() {
		/* 38 */ return this.rfc;
	}

	public void setRfc(String rfc) {
		/* 42 */ this.rfc = rfc;
	}

	public String getPhone() {
		/* 46 */ return this.phone;
	}

	public void setPhone(String phone) {
		/* 50 */ this.phone = phone;
	}

	public String getContact() {
		/* 54 */ return this.contact;
	}

	public void setContact(String contact) {
		/* 58 */ this.contact = contact;
	}

	public String getEmail() {
		/* 62 */ return this.email;
	}

	public void setEmail(String email) {
		/* 66 */ this.email = email;
	}

	public String getAct_type() {
		/* 70 */ return this.act_type;
	}

	public void setAct_type(String act_type) {
		/* 74 */ this.act_type = act_type;
	}

	public String getBank_code() {
		/* 78 */ return this.bank_code;
	}

	public void setBank_code(String bank_code) {
		/* 82 */ this.bank_code = bank_code;
	}

	public String getAct_number() {
		/* 86 */ return this.act_number;
	}

	public void setAct_number(String act_number) {
		/* 90 */ this.act_number = act_number;
	}

	public String getBanorte_clave_id() {
		/* 94 */ return this.banorte_clave_id;
	}

	public void setBanorte_clave_id(String banorte_clave_id) {
		/* 98 */ this.banorte_clave_id = banorte_clave_id;
	}

	public int getStatus() {
		/* 102 */ return this.status;
	}

	public void setStatus(int status) {
		/* 106 */ this.status = status;
	}
}
