package com.addcel.mch2hpayment.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceResult {
	private boolean success;
	private String message;

	public void setMessage(String message) {
		/* 16 */ this.message = message;
	}

	public String getMessage() {
		/* 20 */ return this.message;
	}

	public void setSuccess(boolean success) {
		/* 24 */ this.success = success;
	}

	public boolean isSuccess() {
		/* 28 */ return this.success;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/ServiceResult.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */