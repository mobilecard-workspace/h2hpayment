package com.addcel.mch2hpayment.client.model;

public class AddMoneyReq {
	private Long idUsuario;
	private Double Cantidad;
	private String Tarjeta;
	private Long idEstablecimiento;

	public void setIdEstablecimiento(Long idEstablecimiento) {
		/* 15 */ this.idEstablecimiento = idEstablecimiento;
	}

	public Long getIdEstablecimiento() {
		/* 19 */ return this.idEstablecimiento;
	}

	public void setIdUsuario(Long idUsuario) {
		/* 23 */ this.idUsuario = idUsuario;
	}

	public Long getIdUsuario() {
		/* 27 */ return this.idUsuario;
	}

	public Double getCantidad() {
		/* 31 */ return this.Cantidad;
	}

	public void setCantidad(Double cantidad) {
		/* 35 */ this.Cantidad = cantidad;
	}

	public String getTarjeta() {
		/* 39 */ return this.Tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		/* 43 */ this.Tarjeta = tarjeta;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/AddMoneyReq.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */