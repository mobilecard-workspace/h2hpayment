package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnqueuePaymentRequest {
	private String Token;
	private AccountRecord fromAccount;
	private AccountRecord toAccount;
	private double Amount;
	private String Description;
	private String ClientReference;

	public String getToken() {
		/* 20 */ return this.Token;
	}

	public void setToken(String token) {
		/* 24 */ this.Token = token;
	}

	public AccountRecord getFromAccount() {
		/* 28 */ return this.fromAccount;
	}

	public void setFromAccount(AccountRecord fromAccount) {
		/* 32 */ this.fromAccount = fromAccount;
	}

	public AccountRecord getToAccount() {
		/* 36 */ return this.toAccount;
	}

	public void setToAccount(AccountRecord toAccount) {
		/* 40 */ this.toAccount = toAccount;
	}

	public double getAmount() {
		/* 44 */ return this.Amount;
	}

	public void setAmount(double amount) {
		/* 48 */ this.Amount = amount;
	}

	public String getDescription() {
		/* 52 */ return this.Description;
	}

	public void setDescription(String description) {
		/* 56 */ this.Description = description;
	}

	public String getClientReference() {
		/* 60 */ return this.ClientReference;
	}

	public void setClientReference(String clientReference) {
		/* 64 */ this.ClientReference = clientReference;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/EnqueuePaymentRequest.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */