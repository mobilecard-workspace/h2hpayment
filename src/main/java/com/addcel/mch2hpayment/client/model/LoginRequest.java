package com.addcel.mch2hpayment.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest {
	private String login;
	private String password;

	public void setLogin(String login) {
		/* 16 */ this.login = login;
	}

	public String getLogin() {
		/* 20 */ return this.login;
	}

	public void setPassword(String password) {
		/* 24 */ this.password = password;
	}

	public String getPassword() {
		/* 28 */ return this.password;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/LoginRequest.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */