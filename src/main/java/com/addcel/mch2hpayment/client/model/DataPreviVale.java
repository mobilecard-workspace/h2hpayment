package com.addcel.mch2hpayment.client.model;

public class DataPreviVale {
	private String authorization;
	private Integer previvaleCode;
	private Integer idPeticion;

	public String getAuthorization() {
		/* 14 */ return this.authorization;
	}

	public void setAuthorization(String authorization) {
		/* 18 */ this.authorization = authorization;
	}

	public Integer getPrevivaleCode() {
		/* 22 */ return this.previvaleCode;
	}

	public void setPrevivaleCode(Integer previvaleCode) {
		/* 26 */ this.previvaleCode = previvaleCode;
	}

	public Integer getIdPeticion() {
		/* 30 */ return this.idPeticion;
	}

	public void setIdPeticion(Integer idPeticion) {
		/* 34 */ this.idPeticion = idPeticion;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/DataPreviVale.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */