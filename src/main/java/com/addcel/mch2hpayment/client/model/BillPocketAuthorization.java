package com.addcel.mch2hpayment.client.model;

public class BillPocketAuthorization {
	private long idUsuario;
	private Integer idProveedor;
	private Integer idProducto;
	private String concepto;
	private String referencia;
	private double cargo;
	private double comision;
	private long idTarjeta;
	private String tipoTarjeta;
	private String imei;
	private String software;
	private String modelo;
	private double lat;
	private double lon;
	private String emisor;
	private String operacion;

	public long getIdUsuario() {
		/* 38 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 42 */ this.idUsuario = idUsuario;
	}

	public Integer getIdProveedor() {
		/* 46 */ return this.idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		/* 50 */ this.idProveedor = idProveedor;
	}

	public Integer getIdProducto() {
		/* 54 */ return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		/* 58 */ this.idProducto = idProducto;
	}

	public String getConcepto() {
		/* 62 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 66 */ this.concepto = concepto;
	}

	public String getReferencia() {
		/* 70 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 74 */ this.referencia = referencia;
	}

	public double getCargo() {
		/* 78 */ return this.cargo;
	}

	public void setCargo(double cargo) {
		/* 82 */ this.cargo = cargo;
	}

	public double getComision() {
		/* 86 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 90 */ this.comision = comision;
	}

	public long getIdTarjeta() {
		/* 94 */ return this.idTarjeta;
	}

	public void setIdTarjeta(long idTarjeta) {
		/* 98 */ this.idTarjeta = idTarjeta;
	}

	public String getTipoTarjeta() {
		/* 102 */ return this.tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		/* 106 */ this.tipoTarjeta = tipoTarjeta;
	}

	public String getImei() {
		/* 110 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 114 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 118 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 122 */ this.software = software;
	}

	public String getModelo() {
		/* 126 */ return this.modelo;
	}

	public void setModelo(String modelo) {
		/* 130 */ this.modelo = modelo;
	}

	public double getLat() {
		/* 134 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 138 */ this.lat = lat;
	}

	public double getLon() {
		/* 142 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 146 */ this.lon = lon;
	}

	public String getEmisor() {
		/* 150 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 154 */ this.emisor = emisor;
	}

	public String getOperacion() {
		/* 158 */ return this.operacion;
	}

	public void setOperacion(String operacion) {
		/* 162 */ this.operacion = operacion;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/BillPocketAuthorization.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */