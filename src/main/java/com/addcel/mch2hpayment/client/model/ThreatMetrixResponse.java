package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.BaseResponse;
import com.addcel.mch2hpayment.client.model.TmxReasons;

public class ThreatMetrixResponse extends BaseResponse {
	private String tmxStatus;
	private String tmxRisk;
	private Integer tmxScore;
	private TmxReasons[] tmxReasons;
	private String tmxError;

	public String getTmxStatus() {
		/* 16 */ return this.tmxStatus;
	}

	public void setTmxStatus(String tmxStatus) {
		/* 20 */ this.tmxStatus = tmxStatus;
	}

	public String getTmxRisk() {
		/* 24 */ return this.tmxRisk;
	}

	public void setTmxRisk(String tmxRisk) {
		/* 28 */ this.tmxRisk = tmxRisk;
	}

	public Integer getTmxScore() {
		/* 32 */ return this.tmxScore;
	}

	public void setTmxScore(Integer tmxScore) {
		/* 36 */ this.tmxScore = tmxScore;
	}

	public TmxReasons[] getTmxReasons() {
		/* 40 */ return this.tmxReasons;
	}

	public void setTmxReasons(TmxReasons[] tmxReasons) {
		/* 44 */ this.tmxReasons = tmxReasons;
	}

	public String getTmxError() {
		/* 48 */ return this.tmxError;
	}

	public void setTmxError(String tmxError) {
		/* 52 */ this.tmxError = tmxError;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/ThreatMetrixResponse.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */