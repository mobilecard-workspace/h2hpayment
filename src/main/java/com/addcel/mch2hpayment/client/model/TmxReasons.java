package com.addcel.mch2hpayment.client.model;

public class TmxReasons {
	private String rule;
	private String score;

	public String getRule() {
		/* 11 */ return this.rule;
	}

	public void setRule(String rule) {
		/* 15 */ this.rule = rule;
	}

	public String getScore() {
		/* 19 */ return this.score;
	}

	public void setScore(String score) {
		/* 23 */ this.score = score;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/TmxReasons.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */