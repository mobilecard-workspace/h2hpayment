package com.addcel.mch2hpayment.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogoutRequest {
	private String login;
	private String token;

	public void setLogin(String login) {
		/* 16 */ this.login = login;
	}

	public String getLogin() {
		/* 20 */ return this.login;
	}

	public void setToken(String token) {
		/* 24 */ this.token = token;
	}

	public String getToken() {
		/* 28 */ return this.token;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/LogoutRequest.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */