package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.AutorizationManualRequest;
import com.addcel.mch2hpayment.client.model.BillPocketResponse;

public interface BillPocketClient {
	BillPocketResponse AutorizationManual(AutorizationManualRequest paramAutorizationManualRequest,
			Integer paramInteger1, Integer paramInteger2, String paramString);

	BillPocketResponse Refund(long paramLong, Integer paramInteger1, Integer paramInteger2, String paramString);
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/BillPocketClient.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */