package com.addcel.mch2hpayment.client.model;

public class ThreatMetrixRequest {
	private String sessionId;
	private String nombre;
	private String correo;
	private String username;
	private String telefono;
	private String applicationName;
	private String cardNumber;
	private double cantidad;

	public String getSessionId() {
		/* 23 */ return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		/* 27 */ this.sessionId = sessionId;
	}

	public String getNombre() {
		/* 31 */ return this.nombre;
	}

	public void setNombre(String nombre) {
		/* 35 */ this.nombre = nombre;
	}

	public String getCorreo() {
		/* 39 */ return this.correo;
	}

	public void setCorreo(String correo) {
		/* 43 */ this.correo = correo;
	}

	public String getUsername() {
		/* 47 */ return this.username;
	}

	public void setUsername(String username) {
		/* 51 */ this.username = username;
	}

	public String getTelefono() {
		/* 55 */ return this.telefono;
	}

	public void setTelefono(String telefono) {
		/* 59 */ this.telefono = telefono;
	}

	public String getApplicationName() {
		/* 63 */ return this.applicationName;
	}

	public void setApplicationName(String applicationName) {
		/* 67 */ this.applicationName = applicationName;
	}

	public String getCardNumber() {
		/* 71 */ return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		/* 75 */ this.cardNumber = cardNumber;
	}

	public double getCantidad() {
		/* 79 */ return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		/* 83 */ this.cantidad = cantidad;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/ThreatMetrixRequest.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */