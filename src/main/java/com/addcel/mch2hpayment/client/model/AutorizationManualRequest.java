package com.addcel.mch2hpayment.client.model;

public class AutorizationManualRequest {
	private long idUsuario;
	private long idEstablecimiento;
	private String idioma;
	private int idProveedor;
	private int idProducto;
	private String concepto;
	private String referencia;
	private double cargo;
	private double comision;
	private String tipoTarjeta;
	private String imei;
	private String software;
	private String modelo;
	private double lat;
	private double lon;
	private String operacion;
	private String emisor;
	private String pan;
	private String expDate;
	private String ct;

	public long getIdUsuario() {
		/* 50 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 54 */ this.idUsuario = idUsuario;
	}

	public long getIdEstablecimiento() {
		/* 58 */ return this.idEstablecimiento;
	}

	public void setIdEstablecimiento(long idEstablecimiento) {
		/* 62 */ this.idEstablecimiento = idEstablecimiento;
	}

	public String getIdioma() {
		/* 66 */ return this.idioma;
	}

	public void setIdioma(String idioma) {
		/* 70 */ this.idioma = idioma;
	}

	public int getIdProveedor() {
		/* 74 */ return this.idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		/* 78 */ this.idProveedor = idProveedor;
	}

	public int getIdProducto() {
		/* 82 */ return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		/* 86 */ this.idProducto = idProducto;
	}

	public String getConcepto() {
		/* 90 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 94 */ this.concepto = concepto;
	}

	public String getReferencia() {
		/* 98 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 102 */ this.referencia = referencia;
	}

	public double getCargo() {
		/* 106 */ return this.cargo;
	}

	public void setCargo(double cargo) {
		/* 110 */ this.cargo = cargo;
	}

	public double getComision() {
		/* 114 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 118 */ this.comision = comision;
	}

	public String getTipoTarjeta() {
		/* 122 */ return this.tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		/* 126 */ this.tipoTarjeta = tipoTarjeta;
	}

	public String getImei() {
		/* 130 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 134 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 138 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 142 */ this.software = software;
	}

	public String getModelo() {
		/* 146 */ return this.modelo;
	}

	public void setModelo(String modelo) {
		/* 150 */ this.modelo = modelo;
	}

	public double getLat() {
		/* 154 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 158 */ this.lat = lat;
	}

	public double getLon() {
		/* 162 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 166 */ this.lon = lon;
	}

	public String getOperacion() {
		/* 170 */ return this.operacion;
	}

	public void setOperacion(String operacion) {
		/* 174 */ this.operacion = operacion;
	}

	public String getEmisor() {
		/* 178 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 182 */ this.emisor = emisor;
	}

	public String getPan() {
		/* 186 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 190 */ this.pan = pan;
	}

	public String getExpDate() {
		/* 194 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 198 */ this.expDate = expDate;
	}

	public String getCt() {
		/* 202 */ return this.ct;
	}

	public void setCt(String ct) {
		/* 206 */ this.ct = ct;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/AutorizationManualRequest.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */