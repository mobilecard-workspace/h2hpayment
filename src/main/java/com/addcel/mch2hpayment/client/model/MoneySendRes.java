package com.addcel.mch2hpayment.client.model;

public class MoneySendRes {
	private Integer code;
	private String message;
	private Integer idOperacion;
	private String claveRastreo;
	private String folio;
	private String referencia;
	private String idStpTransaccionesDispersion;
	private String idPeticionWsStp;

	public Integer getCode() {
		/* 21 */ return this.code;
	}

	public void setCode(Integer code) {
		/* 26 */ this.code = code;
	}

	public String getMessage() {
		/* 31 */ return this.message;
	}

	public void setMessage(String message) {
		/* 36 */ this.message = message;
	}

	public Integer getIdOperacion() {
		/* 41 */ return this.idOperacion;
	}

	public void setIdOperacion(Integer idOperacion) {
		/* 46 */ this.idOperacion = idOperacion;
	}

	public String getClaveRastreo() {
		/* 51 */ return this.claveRastreo;
	}

	public void setClaveRastreo(String claveRastreo) {
		/* 56 */ this.claveRastreo = claveRastreo;
	}

	public String getFolio() {
		/* 61 */ return this.folio;
	}

	public void setFolio(String folio) {
		/* 66 */ this.folio = folio;
	}

	public String getReferencia() {
		/* 71 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 76 */ this.referencia = referencia;
	}

	public String getIdStpTransaccionesDispersion() {
		/* 81 */ return this.idStpTransaccionesDispersion;
	}

	public void setIdStpTransaccionesDispersion(String idStpTransaccionesDispersion) {
		/* 86 */ this.idStpTransaccionesDispersion = idStpTransaccionesDispersion;
	}

	public String getIdPeticionWsStp() {
		/* 91 */ return this.idPeticionWsStp;
	}

	public void setIdPeticionWsStp(String idPeticionWsStp) {
		/* 96 */ this.idPeticionWsStp = idPeticionWsStp;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/MoneySendRes.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */