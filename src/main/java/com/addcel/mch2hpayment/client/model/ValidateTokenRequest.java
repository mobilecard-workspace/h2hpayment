package com.addcel.mch2hpayment.client.model;

public class ValidateTokenRequest {
	private String token;
	private String accountId;
	private Long idUsuario;
	private String json;

	public String getToken() {
		/* 15 */ return this.token;
	}

	public void setToken(String token) {
		/* 19 */ this.token = token;
	}

	public String getAccountId() {
		/* 23 */ return this.accountId;
	}

	public void setAccountId(String accountId) {
		/* 27 */ this.accountId = accountId;
	}

	public Long getIdUsuario() {
		/* 31 */ return this.idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		/* 35 */ this.idUsuario = idUsuario;
	}

	public String getJson() {
		/* 39 */ return this.json;
	}

	public void setJson(String json) {
		/* 43 */ this.json = json;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/ValidateTokenRequest.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */