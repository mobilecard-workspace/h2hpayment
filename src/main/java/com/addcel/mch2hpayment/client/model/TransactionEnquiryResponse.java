package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.ServiceResult;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionEnquiryResponse {
	private ServiceResult result;
	private Integer transactionType;
	private String transactionReference;
	private String clientReference;
	private double amount;
	private Integer status;
	private String idAccount;
	private String trackingCode;
	private String movementNumber;
	private String h2HResponse;

	public ServiceResult getResult() {
		/* 24 */ return this.result;
	}

	public void setResult(ServiceResult result) {
		/* 28 */ this.result = result;
	}

	public Integer getTransactionType() {
		/* 32 */ return this.transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		/* 36 */ this.transactionType = transactionType;
	}

	public String getTransactionReference() {
		/* 40 */ return this.transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		/* 44 */ this.transactionReference = transactionReference;
	}

	public String getClientReference() {
		/* 48 */ return this.clientReference;
	}

	public void setClientReference(String clientReference) {
		/* 52 */ this.clientReference = clientReference;
	}

	public double getAmount() {
		/* 56 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 60 */ this.amount = amount;
	}

	public Integer getStatus() {
		/* 64 */ return this.status;
	}

	public void setStatus(Integer status) {
		/* 68 */ this.status = status;
	}

	public String getIdAccount() {
		/* 72 */ return this.idAccount;
	}

	public void setIdAccount(String idAccount) {
		/* 76 */ this.idAccount = idAccount;
	}

	public String getTrackingCode() {
		/* 80 */ return this.trackingCode;
	}

	public void setTrackingCode(String trackingCode) {
		/* 84 */ this.trackingCode = trackingCode;
	}

	public String getMovementNumber() {
		/* 88 */ return this.movementNumber;
	}

	public void setMovementNumber(String movementNumber) {
		/* 92 */ this.movementNumber = movementNumber;
	}

	public String getH2HResponse() {
		/* 96 */ return this.h2HResponse;
	}

	public void setH2HResponse(String h2hResponse) {
		/* 100 */ this.h2HResponse = h2hResponse;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/TransactionEnquiryResponse.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */