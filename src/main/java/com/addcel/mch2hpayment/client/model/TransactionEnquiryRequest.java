package com.addcel.mch2hpayment.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionEnquiryRequest {
	private String token;
	private String transactionReference;

	public void setToken(String token) {
		/* 16 */ this.token = token;
	}

	public String getToken() {
		/* 20 */ return this.token;
	}

	public void setTransactionReference(String transactionReference) {
		/* 24 */ this.transactionReference = transactionReference;
	}

	public String getTransactionReference() {
		/* 28 */ return this.transactionReference;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/TransactionEnquiryRequest.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */