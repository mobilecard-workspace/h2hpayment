package com.addcel.mch2hpayment.client.model;

public class MoneySendReq {
	private Long idUsuario;
	private String conceptoPago;
	private String cuentaBeneficiario;
	private String institucionContraparte;
	private String monto;
	private String nombreBeneficiario;
	private String tipoCuentaBeneficiario;

	public Long getIdUsuario() {
		/* 18 */ return this.idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		/* 22 */ this.idUsuario = idUsuario;
	}

	public String getConceptoPago() {
		/* 26 */ return this.conceptoPago;
	}

	public void setConceptoPago(String conceptoPago) {
		/* 30 */ this.conceptoPago = conceptoPago;
	}

	public String getCuentaBeneficiario() {
		/* 34 */ return this.cuentaBeneficiario;
	}

	public void setCuentaBeneficiario(String cuentaBeneficiario) {
		/* 38 */ this.cuentaBeneficiario = cuentaBeneficiario;
	}

	public String getInstitucionContraparte() {
		/* 42 */ return this.institucionContraparte;
	}

	public void setInstitucionContraparte(String institucionContraparte) {
		/* 46 */ this.institucionContraparte = institucionContraparte;
	}

	public void setMonto(String monto) {
		/* 50 */ this.monto = monto;
	}

	public String getMonto() {
		/* 54 */ return this.monto;
	}

	public String getNombreBeneficiario() {
		/* 58 */ return this.nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		/* 62 */ this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getTipoCuentaBeneficiario() {
		/* 66 */ return this.tipoCuentaBeneficiario;
	}

	public void setTipoCuentaBeneficiario(String tipoCuentaBeneficiario) {
		/* 70 */ this.tipoCuentaBeneficiario = tipoCuentaBeneficiario;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/MoneySendReq.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */