package com.addcel.mch2hpayment.client.model;

public class ValidateTokenResponse {
	private Integer code;
	private String message;

	public Integer getCode() {
		/* 13 */ return this.code;
	}

	public void setCode(Integer code) {
		/* 17 */ this.code = code;
	}

	public String getMessage() {
		/* 21 */ return this.message;
	}

	public void setMessage(String message) {
		/* 25 */ this.message = message;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/ValidateTokenResponse.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */