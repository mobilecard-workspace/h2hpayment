package com.addcel.mch2hpayment.client.model;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnqueueAccountSignUpRequest {
	private String token;
	private AccountRecord account;
	private String clientReference;
	private String idAccount;

	public String getToken() {
		/* 19 */ return this.token;
	}

	public void setToken(String token) {
		/* 23 */ this.token = token;
	}

	public AccountRecord getAccount() {
		/* 27 */ return this.account;
	}

	public void setAccount(AccountRecord account) {
		/* 31 */ this.account = account;
	}

	public String getClientReference() {
		/* 35 */ return this.clientReference;
	}

	public void setClientReference(String clientReference) {
		/* 39 */ this.clientReference = clientReference;
	}

	public String getIdAccount() {
		/* 43 */ return this.idAccount;
	}

	public void setIdAccount(String idAccount) {
		/* 47 */ this.idAccount = idAccount;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/client/model/EnqueueAccountSignUpRequest.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */