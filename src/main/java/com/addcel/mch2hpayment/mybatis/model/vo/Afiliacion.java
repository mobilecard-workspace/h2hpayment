package com.addcel.mch2hpayment.mybatis.model.vo;

public class Afiliacion {
	private long ID;
	private String AFILIACION;
	private String USUARIO;
	private String CLAVE_USUARIO;
	private String MERCHANT_ID;
	private String ID_TERMINAL;
	private String ADDRESS;
	private String COMERCIO;
	private int ACTIVO;
	private String BANCO;
	private String CURRENCY;
	private String STORE;
	private String FORWARD_PATH;

	public long getID() {
		/* 24 */ return this.ID;
	}

	public void setID(long iD) {
		/* 28 */ this.ID = iD;
	}

	public String getAFILIACION() {
		/* 32 */ return this.AFILIACION;
	}

	public void setAFILIACION(String aFILIACION) {
		/* 36 */ this.AFILIACION = aFILIACION;
	}

	public String getUSUARIO() {
		/* 40 */ return this.USUARIO;
	}

	public void setUSUARIO(String uSUARIO) {
		/* 44 */ this.USUARIO = uSUARIO;
	}

	public String getCLAVE_USUARIO() {
		/* 48 */ return this.CLAVE_USUARIO;
	}

	public void setCLAVE_USUARIO(String cLAVE_USUARIO) {
		/* 52 */ this.CLAVE_USUARIO = cLAVE_USUARIO;
	}

	public String getMERCHANT_ID() {
		/* 56 */ return this.MERCHANT_ID;
	}

	public void setMERCHANT_ID(String mERCHANT_ID) {
		/* 60 */ this.MERCHANT_ID = mERCHANT_ID;
	}

	public String getID_TERMINAL() {
		/* 64 */ return this.ID_TERMINAL;
	}

	public void setID_TERMINAL(String iD_TERMINAL) {
		/* 68 */ this.ID_TERMINAL = iD_TERMINAL;
	}

	public String getADDRESS() {
		/* 72 */ return this.ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		/* 76 */ this.ADDRESS = aDDRESS;
	}

	public String getCOMERCIO() {
		/* 80 */ return this.COMERCIO;
	}

	public void setCOMERCIO(String cOMERCIO) {
		/* 84 */ this.COMERCIO = cOMERCIO;
	}

	public int getACTIVO() {
		/* 88 */ return this.ACTIVO;
	}

	public void setACTIVO(int aCTIVO) {
		/* 92 */ this.ACTIVO = aCTIVO;
	}

	public String getBANCO() {
		/* 96 */ return this.BANCO;
	}

	public void setBANCO(String bANCO) {
		/* 100 */ this.BANCO = bANCO;
	}

	public String getCURRENCY() {
		/* 104 */ return this.CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		/* 108 */ this.CURRENCY = cURRENCY;
	}

	public String getSTORE() {
		/* 112 */ return this.STORE;
	}

	public void setSTORE(String sTORE) {
		/* 116 */ this.STORE = sTORE;
	}

	public String getFORWARD_PATH() {
		/* 120 */ return this.FORWARD_PATH;
	}

	public void setFORWARD_PATH(String fORWARD_PATH) {
		/* 124 */ this.FORWARD_PATH = fORWARD_PATH;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/Afiliacion.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */