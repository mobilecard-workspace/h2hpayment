package com.addcel.mch2hpayment.mybatis.model.vo;

public class Proveedor {
	private Long id_proveedor;
	private double prv_num_cliente;
	private double prv_pos_id;
	private String prv_nombre_comercio;
	private String prv_domicilio;
	private String prv_dealer_login;
	private String prv_dealer_password;
	private String prv_pos_login;
	private String prv_pos_password;
	private String prv_nombre_completo;
	private String prv_clave_personal;
	private int prv_status;
	private String prv_claveWS;
	private String prv_path;
	private int id_categoria;
	private double comision;
	private int compatible_Amex;
	private int compatible_Visa;
	private double comision_porcentaje;
	private double min_comision_porcentaje;
	private double min_comision_fija;
	private String AFILIACION;
	private double tipo_cambio;
	private double MAX_TRANSACCION;

	public Long getId_proveedor() {
		/* 35 */ return this.id_proveedor;
	}

	public void setId_proveedor(Long id_proveedor) {
		/* 39 */ this.id_proveedor = id_proveedor;
	}

	public double getPrv_num_cliente() {
		/* 43 */ return this.prv_num_cliente;
	}

	public void setPrv_num_cliente(double prv_num_cliente) {
		/* 47 */ this.prv_num_cliente = prv_num_cliente;
	}

	public double getPrv_pos_id() {
		/* 51 */ return this.prv_pos_id;
	}

	public void setPrv_pos_id(double prv_pos_id) {
		/* 55 */ this.prv_pos_id = prv_pos_id;
	}

	public String getPrv_nombre_comercio() {
		/* 59 */ return this.prv_nombre_comercio;
	}

	public void setPrv_nombre_comercio(String prv_nombre_comercio) {
		/* 63 */ this.prv_nombre_comercio = prv_nombre_comercio;
	}

	public String getPrv_domicilio() {
		/* 67 */ return this.prv_domicilio;
	}

	public void setPrv_domicilio(String prv_domicilio) {
		/* 71 */ this.prv_domicilio = prv_domicilio;
	}

	public String getPrv_dealer_login() {
		/* 75 */ return this.prv_dealer_login;
	}

	public void setPrv_dealer_login(String prv_dealer_login) {
		/* 79 */ this.prv_dealer_login = prv_dealer_login;
	}

	public String getPrv_dealer_password() {
		/* 83 */ return this.prv_dealer_password;
	}

	public void setPrv_dealer_password(String prv_dealer_password) {
		/* 87 */ this.prv_dealer_password = prv_dealer_password;
	}

	public String getPrv_pos_login() {
		/* 91 */ return this.prv_pos_login;
	}

	public void setPrv_pos_login(String prv_pos_login) {
		/* 95 */ this.prv_pos_login = prv_pos_login;
	}

	public String getPrv_pos_password() {
		/* 99 */ return this.prv_pos_password;
	}

	public void setPrv_pos_password(String prv_pos_password) {
		/* 103 */ this.prv_pos_password = prv_pos_password;
	}

	public String getPrv_nombre_completo() {
		/* 107 */ return this.prv_nombre_completo;
	}

	public void setPrv_nombre_completo(String prv_nombre_completo) {
		/* 111 */ this.prv_nombre_completo = prv_nombre_completo;
	}

	public String getPrv_clave_personal() {
		/* 115 */ return this.prv_clave_personal;
	}

	public void setPrv_clave_personal(String prv_clave_personal) {
		/* 119 */ this.prv_clave_personal = prv_clave_personal;
	}

	public int getPrv_status() {
		/* 123 */ return this.prv_status;
	}

	public void setPrv_status(int prv_status) {
		/* 127 */ this.prv_status = prv_status;
	}

	public String getPrv_claveWS() {
		/* 131 */ return this.prv_claveWS;
	}

	public void setPrv_claveWS(String prv_claveWS) {
		/* 135 */ this.prv_claveWS = prv_claveWS;
	}

	public String getPrv_path() {
		/* 139 */ return this.prv_path;
	}

	public void setPrv_path(String prv_path) {
		/* 143 */ this.prv_path = prv_path;
	}

	public int getId_categoria() {
		/* 147 */ return this.id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		/* 151 */ this.id_categoria = id_categoria;
	}

	public double getComision() {
		/* 155 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 159 */ this.comision = comision;
	}

	public int getCompatible_Amex() {
		/* 163 */ return this.compatible_Amex;
	}

	public void setCompatible_Amex(int compatible_Amex) {
		/* 167 */ this.compatible_Amex = compatible_Amex;
	}

	public int getCompatible_Visa() {
		/* 171 */ return this.compatible_Visa;
	}

	public void setCompatible_Visa(int compatible_Visa) {
		/* 175 */ this.compatible_Visa = compatible_Visa;
	}

	public double getComision_porcentaje() {
		/* 179 */ return this.comision_porcentaje;
	}

	public void setComision_porcentaje(double comision_porcentaje) {
		/* 183 */ this.comision_porcentaje = comision_porcentaje;
	}

	public double getMin_comision_porcentaje() {
		/* 187 */ return this.min_comision_porcentaje;
	}

	public void setMin_comision_porcentaje(double min_comision_porcentaje) {
		/* 191 */ this.min_comision_porcentaje = min_comision_porcentaje;
	}

	public double getMin_comision_fija() {
		/* 195 */ return this.min_comision_fija;
	}

	public void setMin_comision_fija(double min_comision_fija) {
		/* 199 */ this.min_comision_fija = min_comision_fija;
	}

	public String getAFILIACION() {
		/* 203 */ return this.AFILIACION;
	}

	public void setAFILIACION(String aFILIACION) {
		/* 207 */ this.AFILIACION = aFILIACION;
	}

	public double getTipo_cambio() {
		/* 211 */ return this.tipo_cambio;
	}

	public void setTipo_cambio(double tipo_cambio) {
		/* 215 */ this.tipo_cambio = tipo_cambio;
	}

	public double getMAX_TRANSACCION() {
		/* 219 */ return this.MAX_TRANSACCION;
	}

	public void setMAX_TRANSACCION(double mAX_TRANSACCION) {
		/* 223 */ this.MAX_TRANSACCION = mAX_TRANSACCION;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/Proveedor.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */