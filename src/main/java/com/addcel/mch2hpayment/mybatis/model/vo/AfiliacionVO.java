package com.addcel.mch2hpayment.mybatis.model.vo;

public class AfiliacionVO {
	private String id;
	private String idAfiliacion;
	private String usuario;
	private String claveUsuario;
	private String merchantId;
	private String forwardPath;
	private String idTerminal;
	private String addresss;
	private String store;
	private String currency;
	private String comercio;
	private String banco;
	private boolean activo;

	public String getId() {
		/* 32 */ return this.id;
	}

	public void setId(String id) {
		/* 36 */ this.id = id;
	}

	public String getIdAfiliacion() {
		/* 40 */ return this.idAfiliacion;
	}

	public void setIdAfiliacion(String idAfiliacion) {
		/* 44 */ this.idAfiliacion = idAfiliacion;
	}

	public String getUsuario() {
		/* 48 */ return this.usuario;
	}

	public void setUsuario(String usuario) {
		/* 52 */ this.usuario = usuario;
	}

	public String getClaveUsuario() {
		/* 56 */ return this.claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		/* 60 */ this.claveUsuario = claveUsuario;
	}

	public String getMerchantId() {
		/* 64 */ return this.merchantId;
	}

	public void setMerchantId(String merchantId) {
		/* 68 */ this.merchantId = merchantId;
	}

	public String getForwardPath() {
		/* 72 */ return this.forwardPath;
	}

	public void setForwardPath(String forwardPath) {
		/* 76 */ this.forwardPath = forwardPath;
	}

	public String getIdTerminal() {
		/* 80 */ return this.idTerminal;
	}

	public void setIdTerminal(String idTerminal) {
		/* 84 */ this.idTerminal = idTerminal;
	}

	public String toString() {
		/* 89 */ return "[ID: " + this.id +
		/* 90 */ ", ID AFILIACION: " + this.idAfiliacion +
		/* 91 */ ", USUARIO: " + this.usuario +
		/* 92 */ ", CLAVE USUARIO: " + this.claveUsuario +
		/* 93 */ ", MERCHANT ID: " + this.merchantId +
		/* 94 */ ", FORWARD PATH: " + this.forwardPath +
		/* 95 */ ", ID TERMINAL: " + this.idTerminal +
		/* 96 */ "]";
	}

	public String getAddresss() {
		/* 100 */ return this.addresss;
	}

	public void setAddresss(String addresss) {
		/* 104 */ this.addresss = addresss;
	}

	public String getStore() {
		/* 108 */ return this.store;
	}

	public void setStore(String store) {
		/* 112 */ this.store = store;
	}

	public String getCurrency() {
		/* 116 */ return this.currency;
	}

	public void setCurrency(String currency) {
		/* 120 */ this.currency = currency;
	}

	public String getComercio() {
		/* 124 */ return this.comercio;
	}

	public void setComercio(String comercio) {
		/* 128 */ this.comercio = comercio;
	}

	public boolean isActivo() {
		/* 132 */ return this.activo;
	}

	public void setActivo(boolean activo) {
		/* 136 */ this.activo = activo;
	}

	public String getBanco() {
		/* 140 */ return this.banco;
	}

	public void setBanco(String banco) {
		/* 144 */ this.banco = banco;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/AfiliacionVO.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */