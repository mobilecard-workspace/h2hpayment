package com.addcel.mch2hpayment.mybatis.model.mapper;

import com.addcel.mch2hpayment.client.model.AccountMC;
import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.mybatis.model.vo.AccountPrevivale;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.Bank;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.TransaccionBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface H2HMapper {
	String H2HBanorte_Business(@Param("idUsuario") long paramLong1, @Param("idBitacora") long paramLong2,
			@Param("transactionNumber") long paramLong3, @Param("alias") String paramString1,
			@Param("proceso") int paramInt1, @Param("idAccount") long paramLong4,
			@Param("telefono") String paramString2, @Param("correo") String paramString3,
			@Param("nidioma") String paramString4, @Param("idApp") int paramInt2);

	int isBinValido(@Param("bin") String paramString);

	long getMaxIdBitacora();

	List<BankCodes> getBankcodes();

	Card getCard(@Param("idUsuario") long paramLong1, @Param("idTarjeta") long paramLong2,
			@Param("idApp") int paramInt);

	String RulesH2HBanorte(@Param("idUser") long paramLong1, @Param("accountId") long paramLong2,
			@Param("cardId") long paramLong3, @Param("amount_transfer") Double paramDouble,
			@Param("bin") String paramString1, @Param("idioma") String paramString2, @Param("idApp") int paramInt);

	User getUserData(@Param("idUsuario") long paramLong1, @Param("idApp") long paramLong2);

	AccountRecord getAccount(@Param("idaccount") long paramLong);

	BankCodes getBankcodesByClave(@Param("clave_banco") String paramString);

	Proveedor getProveedor(String paramString);

	AfiliacionVO buscaAfiliacion(@Param("banco") String paramString);

	void addBitacora(TBitacoraVO paramTBitacoraVO);

	void updateTbitacora(@Param("idBitacora") long paramLong, @Param("status") int paramInt,
			@Param("aut") String paramString);

	void insertTbitacoraMCBanorte(TBitacoraBanorte paramTBitacoraBanorte);

	TBitacoraBanorte getBitacoraBanorte(@Param("idBitacora") long paramLong);

	void updateBitacoraBanorteOBJ(TBitacoraBanorte paramTBitacoraBanorte);

	ProjectMC getProjectMC(@Param("cont") String paramString1, @Param("recu") String paramString2);

	String getParameter(@Param("parameter") String paramString);

	void insertaTransactionBanorte(TransaccionBanorte paramTransaccionBanorte);

	TransaccionBanorte getTransactionBanorte(@Param("id_bitacora") long paramLong);

	String getToken();

	int difFechaMin(String paramString);

	List<Bank> getCatalogoBancos();

	void insertAccount(AccountMC paramAccountMC);

	Bank getCatalogoBancosByClave(@Param("clave") String paramString);

	String isAppActive(@Param("idApp") Integer paramInteger);

	AccountPrevivale getAccountPrevivale(@Param("cuenta") String paramString);
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/mapper/H2HMapper.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */