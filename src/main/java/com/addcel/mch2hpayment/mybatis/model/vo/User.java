package com.addcel.mch2hpayment.mybatis.model.vo;

public class User {
	private long id_usuario;
	private String eMail;
	private String usr_telefono;
	private String usr_nombre;
	private int id_usr_status;

	public void setId_usr_status(int id_usr_status) {
		/* 16 */ this.id_usr_status = id_usr_status;
	}

	public int getId_usr_status() {
		/* 20 */ return this.id_usr_status;
	}

	public void setId_usuario(long id_usuario) {
		/* 24 */ this.id_usuario = id_usuario;
	}

	public void setUsr_nombre(String usr_nombre) {
		/* 28 */ this.usr_nombre = usr_nombre;
	}

	public String getUsr_nombre() {
		/* 32 */ return this.usr_nombre;
	}

	public long getId_usuario() {
		/* 36 */ return this.id_usuario;
	}

	public String geteMail() {
		/* 40 */ return this.eMail;
	}

	public void seteMail(String eMail) {
		/* 44 */ this.eMail = eMail;
	}

	public String getUsr_telefono() {
		/* 48 */ return this.usr_telefono;
	}

	public void setUsr_telefono(String usr_telefono) {
		/* 52 */ this.usr_telefono = usr_telefono;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/User.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */