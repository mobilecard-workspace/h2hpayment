package com.addcel.mch2hpayment.mybatis.model.vo;

public class TBitacoraVO {
	private long idBitacora;
	private String idUsuario;
	private int idProveedor;
	private int idProducto;
	private String bitFecha;
	private String bitHora;
	private String bitConcepto;
	private String bitCargo;
	private String bitTicket;
	private String bitNoAutorizacion;
	private int bitCodigoError;
	private String bitCardId;
	private int bitStatus;
	private String imei;
	private String destino;
	private String tarjetaCompra;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	private int car_id;
	private double cargo;
	private String tarjeta_compra;
	private String concepto;
	private String afiliacion;
	private double comision;
	private String cx;
	private String cy;
	private String pin;
	private String folio;
	private String login;
	private String nombre;
	private String apellido;
	private String mail;
	private String TRX_NO;
	private String AUTONO;
	private String tarjetaIave;
	private int id_aplicacion;
	private int idPais;

	public TBitacoraVO() {
	}

	public TBitacoraVO(String idUsuario, String bitConcepto, String imei, String destino, String tarjetaCompra,
			String tipo, String software, String modelo, String wkey, String bitCargo) {
		/* 90 */ this.idUsuario = idUsuario;
		/* 91 */ this.bitConcepto = bitConcepto;
		/* 92 */ this.imei = imei;
		/* 93 */ this.destino = destino;
		/* 94 */ this.tarjetaCompra = tarjetaCompra;
		/* 95 */ this.tipo = tipo;
		/* 96 */ this.software = software;
		/* 97 */ this.modelo = modelo;
		/* 98 */ this.wkey = wkey;
		/* 99 */ this.bitCargo = bitCargo;
	}

	public void setIdPais(int idPais) {
		/* 103 */ this.idPais = idPais;
	}

	public int getIdPais() {
		/* 107 */ return this.idPais;
	}

	public void setId_aplicacion(int id_aplicacion) {
		/* 111 */ this.id_aplicacion = id_aplicacion;
	}

	public int getId_aplicacion() {
		/* 115 */ return this.id_aplicacion;
	}

	public long getIdBitacora() {
		/* 119 */ return this.idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		/* 122 */ this.idBitacora = idBitacora;
	}

	public String getIdUsuario() {
		/* 125 */ return this.idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		/* 128 */ this.idUsuario = idUsuario;
	}

	public int getIdProveedor() {
		/* 131 */ return this.idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		/* 134 */ this.idProveedor = idProveedor;
	}

	public int getIdProducto() {
		/* 137 */ return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		/* 140 */ this.idProducto = idProducto;
	}

	public String getBitFecha() {
		/* 143 */ return this.bitFecha;
	}

	public void setBitFecha(String bitFecha) {
		/* 146 */ this.bitFecha = bitFecha;
	}

	public String getBitHora() {
		/* 149 */ return this.bitHora;
	}

	public void setBitHora(String bitHora) {
		/* 152 */ this.bitHora = bitHora;
	}

	public String getBitConcepto() {
		/* 155 */ return this.bitConcepto;
	}

	public void setBitConcepto(String bitConcepto) {
		/* 158 */ this.bitConcepto = bitConcepto;
	}

	public String getBitCargo() {
		/* 161 */ return this.bitCargo;
	}

	public void setBitCargo(String bitCargo) {
		/* 164 */ this.bitCargo = bitCargo;
	}

	public String getBitTicket() {
		/* 167 */ return this.bitTicket;
	}

	public void setBitTicket(String bitTicket) {
		/* 170 */ this.bitTicket = bitTicket;
	}

	public String getBitNoAutorizacion() {
		/* 173 */ return this.bitNoAutorizacion;
	}

	public void setBitNoAutorizacion(String bitNoAutorizacion) {
		/* 176 */ this.bitNoAutorizacion = bitNoAutorizacion;
	}

	public int getBitCodigoError() {
		/* 179 */ return this.bitCodigoError;
	}

	public void setBitCodigoError(int bitCodigoError) {
		/* 182 */ this.bitCodigoError = bitCodigoError;
	}

	public String getBitCardId() {
		/* 185 */ return this.bitCardId;
	}

	public void setBitCardId(String bitCardId) {
		/* 188 */ this.bitCardId = bitCardId;
	}

	public int getBitStatus() {
		/* 191 */ return this.bitStatus;
	}

	public void setBitStatus(int bitStatus) {
		/* 194 */ this.bitStatus = bitStatus;
	}

	public String getImei() {
		/* 197 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 200 */ this.imei = imei;
	}

	public String getDestino() {
		/* 203 */ return this.destino;
	}

	public void setDestino(String destino) {
		/* 206 */ this.destino = destino;
	}

	public String getTarjetaCompra() {
		/* 209 */ return this.tarjetaCompra;
	}

	public void setTarjetaCompra(String tarjetaCompra) {
		/* 212 */ this.tarjetaCompra = tarjetaCompra;
	}

	public String getTipo() {
		/* 215 */ return this.tipo;
	}

	public void setTipo(String tipo) {
		/* 218 */ this.tipo = tipo;
	}

	public String getSoftware() {
		/* 221 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 224 */ this.software = software;
	}

	public String getModelo() {
		/* 227 */ return this.modelo;
	}

	public void setModelo(String modelo) {
		/* 230 */ this.modelo = modelo;
	}

	public String getWkey() {
		/* 233 */ return this.wkey;
	}

	public void setWkey(String wkey) {
		/* 236 */ this.wkey = wkey;
	}

	public int getCar_id() {
		/* 240 */ return this.car_id;
	}

	public void setCar_id(int car_id) {
		/* 244 */ this.car_id = car_id;
	}

	public double getCargo() {
		/* 248 */ return this.cargo;
	}

	public void setCargo(double cargo) {
		/* 252 */ this.cargo = cargo;
	}

	public String getTarjeta_compra() {
		/* 256 */ return this.tarjeta_compra;
	}

	public void setTarjeta_compra(String tarjeta_compra) {
		/* 260 */ this.tarjeta_compra = tarjeta_compra;
	}

	public String getConcepto() {
		/* 264 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 268 */ this.concepto = concepto;
	}

	public String getAfiliacion() {
		/* 272 */ return this.afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		/* 276 */ this.afiliacion = afiliacion;
	}

	public double getComision() {
		/* 280 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 284 */ this.comision = comision;
	}

	public String getCx() {
		/* 288 */ return this.cx;
	}

	public void setCx(String cx) {
		/* 292 */ this.cx = cx;
	}

	public String getCy() {
		/* 296 */ return this.cy;
	}

	public void setCy(String cy) {
		/* 300 */ this.cy = cy;
	}

	public String getPin() {
		/* 304 */ return this.pin;
	}

	public void setPin(String pin) {
		/* 308 */ this.pin = pin;
	}

	public String getFolio() {
		/* 312 */ return this.folio;
	}

	public void setFolio(String folio) {
		/* 316 */ this.folio = folio;
	}

	public String getLogin() {
		/* 320 */ return this.login;
	}

	public void setLogin(String login) {
		/* 324 */ this.login = login;
	}

	public String getNombre() {
		/* 328 */ return this.nombre;
	}

	public void setNombre(String nombre) {
		/* 332 */ this.nombre = nombre;
	}

	public String getApellido() {
		/* 336 */ return this.apellido;
	}

	public void setApellido(String apellido) {
		/* 340 */ this.apellido = apellido;
	}

	public String getMail() {
		/* 344 */ return this.mail;
	}

	public void setMail(String mail) {
		/* 348 */ this.mail = mail;
	}

	public String getTRX_NO() {
		/* 352 */ return this.TRX_NO;
	}

	public void setTRX_NO(String tRX_NO) {
		/* 356 */ this.TRX_NO = tRX_NO;
	}

	public String getAUTONO() {
		/* 360 */ return this.AUTONO;
	}

	public void setAUTONO(String aUTONO) {
		/* 364 */ this.AUTONO = aUTONO;
	}

	public String getTarjetaIave() {
		/* 368 */ return this.tarjetaIave;
	}

	public void setTarjetaIave(String tarjetaIave) {
		/* 372 */ this.tarjetaIave = tarjetaIave;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/TBitacoraVO.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */