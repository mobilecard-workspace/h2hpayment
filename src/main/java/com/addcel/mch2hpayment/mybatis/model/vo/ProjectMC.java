package com.addcel.mch2hpayment.mybatis.model.vo;

public class ProjectMC {
	private int id;
	private String contexto;
	private String recurso;
	private String descripcion;
	private String url;
	private String protocolo;
	private int activo;

	public int getId() {
		/* 18 */ return this.id;
	}

	public void setId(int id) {
		/* 22 */ this.id = id;
	}

	public String getContexto() {
		/* 26 */ return this.contexto;
	}

	public void setContexto(String contexto) {
		/* 30 */ this.contexto = contexto;
	}

	public String getRecurso() {
		/* 34 */ return this.recurso;
	}

	public void setRecurso(String recurso) {
		/* 38 */ this.recurso = recurso;
	}

	public String getDescripcion() {
		/* 42 */ return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		/* 46 */ this.descripcion = descripcion;
	}

	public String getUrl() {
		/* 50 */ return this.url;
	}

	public void setUrl(String url) {
		/* 54 */ this.url = url;
	}

	public String getProtocolo() {
		/* 58 */ return this.protocolo;
	}

	public void setProtocolo(String protocolo) {
		/* 62 */ this.protocolo = protocolo;
	}

	public int getActivo() {
		/* 66 */ return this.activo;
	}

	public void setActivo(int activo) {
		/* 70 */ this.activo = activo;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/ProjectMC.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */