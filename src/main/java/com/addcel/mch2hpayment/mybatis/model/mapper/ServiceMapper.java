package com.addcel.mch2hpayment.mybatis.model.mapper;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.mybatis.model.vo.Afiliacion;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ServiceMapper {
	void addBitacora(TBitacoraVO paramTBitacoraVO);

	Card getCard(@Param("idUsuario") long paramLong1, @Param("idTarjeta") long paramLong2);

	long getMaxIdBitacora();

	String H2HBanorte_Business(@Param("idUsuario") long paramLong1, @Param("idBitacora") long paramLong2,
			@Param("transactionNumber") long paramLong3, @Param("alias") String paramString1,
			@Param("proceso") int paramInt, @Param("idAccount") long paramLong4, @Param("telefono") String paramString2,
			@Param("correo") String paramString3, @Param("nidioma") String paramString4);

	AccountRecord getAccount(@Param("idaccount") long paramLong);

	User getUserData(@Param("idUsuario") long paramLong);

	List<BankCodes> getBankcodes();

	BankCodes getBankcodesByClave(@Param("clave_banco") String paramString);

	TBitacoraBanorte getBitacoraBanorte(@Param("idBitacora") long paramLong);

	void insertTbitacoraMCBanorte(TBitacoraBanorte paramTBitacoraBanorte);

	void updateTbitacora(@Param("idBitacora") long paramLong, @Param("status") int paramInt,
			@Param("aut") String paramString);

	void updateBitacoraBanorte(@Param("transaction") long paramLong1, @Param("idBitacora") long paramLong2,
			@Param("reference") String paramString);

	void updateBitacoraBanorteOBJ(TBitacoraBanorte paramTBitacoraBanorte);

	void updateuserSMS(@Param("idUsuario") long paramLong);

	Afiliacion getAfiliacion(@Param("idAfiliacion") long paramLong);

	AfiliacionVO buscaAfiliacion(@Param("banco") String paramString);

	String getParameter(@Param("parameter") String paramString);

	Proveedor getProveedor(String paramString);

	int GetAmountUserTransfer(@Param("idUsuario") long paramLong);

	int isBinValido(@Param("bin") String paramString);

	String RulesH2HBanorte(@Param("idUser") long paramLong1, @Param("accountId") long paramLong2,
			@Param("cardId") long paramLong3, @Param("amount_transfer") Double paramDouble,
			@Param("bin") String paramString1, @Param("idioma") String paramString2);

	ProjectMC getProjectMC(@Param("cont") String paramString1, @Param("recu") String paramString2);
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/mapper/ServiceMapper.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */