package com.addcel.mch2hpayment.mybatis.model.vo;

public class Bank {
	private Integer id;
	private Integer clave;
	private String descripcion;
	private Integer activo;

	public Integer getId() {
		/* 15 */ return this.id;
	}

	public void setId(Integer id) {
		/* 19 */ this.id = id;
	}

	public Integer getClave() {
		/* 23 */ return this.clave;
	}

	public void setClave(Integer clave) {
		/* 27 */ this.clave = clave;
	}

	public String getDescripcion() {
		/* 31 */ return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		/* 35 */ this.descripcion = descripcion;
	}

	public Integer getActivo() {
		/* 39 */ return this.activo;
	}

	public void setActivo(Integer activo) {
		/* 43 */ this.activo = activo;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/Bank.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */