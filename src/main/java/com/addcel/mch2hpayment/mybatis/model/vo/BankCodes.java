package com.addcel.mch2hpayment.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankCodes {
	private long id;
	private String clave;
	private String nombre_razon_social;
	private String nombre_corto;
	private double comision_fija;
	private double comision_porcentaje;

	public double getComision_fija() {
		/* 22 */ return this.comision_fija;
	}

	public void setComision_fija(double comision_fija) {
		/* 28 */ this.comision_fija = comision_fija;
	}

	public double getComision_porcentaje() {
		/* 34 */ return this.comision_porcentaje;
	}

	public void setComision_porcentaje(double comision_porcentaje) {
		/* 40 */ this.comision_porcentaje = comision_porcentaje;
	}

	public long getId() {
		/* 46 */ return this.id;
	}

	public void setId(long id) {
		/* 50 */ this.id = id;
	}

	public String getClave() {
		/* 54 */ return this.clave;
	}

	public void setClave(String clave) {
		/* 58 */ this.clave = clave;
	}

	public String getNombre_razon_social() {
		/* 62 */ return this.nombre_razon_social;
	}

	public void setNombre_razon_social(String nombre_razon_social) {
		/* 66 */ this.nombre_razon_social = nombre_razon_social;
	}

	public String getNombre_corto() {
		/* 70 */ return this.nombre_corto;
	}

	public void setNombre_corto(String nombre_corto) {
		/* 74 */ this.nombre_corto = nombre_corto;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/BankCodes.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */