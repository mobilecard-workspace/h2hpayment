package com.addcel.mch2hpayment.mybatis.model.vo;

import java.io.Serializable;

public class GenericResponse implements Serializable {

	private static final long serialVersionUID = 8588418809687435245L;

	private Integer idError;
	private String mensajeError;

	public Integer getIdError() {
		return idError;
	}

	public void setIdError(Integer idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

}
