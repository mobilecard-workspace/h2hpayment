package com.addcel.mch2hpayment.mybatis.model.vo;

public class AccountPrevivale {
	private String pan;
	private long idUsuario;
	private long idComercio;

	public String getPan() {
		/* 14 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 18 */ this.pan = pan;
	}

	public long getIdUsuario() {
		/* 22 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 26 */ this.idUsuario = idUsuario;
	}

	public long getIdComercio() {
		/* 30 */ return this.idComercio;
	}

	public void setIdComercio(long idComercio) {
		/* 34 */ this.idComercio = idComercio;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/AccountPrevivale.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */