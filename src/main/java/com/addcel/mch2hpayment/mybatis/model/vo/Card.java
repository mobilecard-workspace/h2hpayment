package com.addcel.mch2hpayment.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Card {
	private long idtarjetasusuario;
	private int mobilecard;
	private String idtarjetas_tipo;
	private String nombre_tarjeta;
	private String usrDomAmex;
	private String usrCpAmex;
	private String ct;
	private String vigencia;
	private String numerotarjeta;
	private int idfranquicia;
	private int estado;
	private String act_type;
	private String clabe;
	private String num_cuenta;
	private String idbanco;
	private Integer previvale;

	public void setPrevivale(Integer previvale) {
		/* 33 */ this.previvale = previvale;
	}

	public Integer getPrevivale() {
		/* 37 */ return this.previvale;
	}

	public void setIdbanco(String idbanco) {
		/* 41 */ this.idbanco = idbanco;
	}

	public String getIdbanco() {
		/* 45 */ return this.idbanco;
	}

	public String getAct_type() {
		/* 49 */ return this.act_type;
	}

	public void setAct_type(String act_type) {
		/* 53 */ this.act_type = act_type;
	}

	public String getClabe() {
		/* 57 */ return this.clabe;
	}

	public void setClabe(String clabe) {
		/* 61 */ this.clabe = clabe;
	}

	public String getNum_cuenta() {
		/* 65 */ return this.num_cuenta;
	}

	public void setNum_cuenta(String num_cuenta) {
		/* 69 */ this.num_cuenta = num_cuenta;
	}

	public long getIdtarjetasusuario() {
		/* 73 */ return this.idtarjetasusuario;
	}

	public void setIdtarjetasusuario(long idtarjetasusuario) {
		/* 77 */ this.idtarjetasusuario = idtarjetasusuario;
	}

	public int getMobilecard() {
		/* 81 */ return this.mobilecard;
	}

	public void setMobilecard(int mobilecard) {
		/* 85 */ this.mobilecard = mobilecard;
	}

	public String getIdtarjetas_tipo() {
		/* 89 */ return this.idtarjetas_tipo;
	}

	public void setIdtarjetas_tipo(String idtarjetas_tipo) {
		/* 93 */ this.idtarjetas_tipo = idtarjetas_tipo;
	}

	public String getNombre_tarjeta() {
		/* 97 */ return this.nombre_tarjeta;
	}

	public void setNombre_tarjeta(String nombre_tarjeta) {
		/* 101 */ this.nombre_tarjeta = nombre_tarjeta;
	}

	public String getUsrDomAmex() {
		/* 105 */ return this.usrDomAmex;
	}

	public void setUsrDomAmex(String usrDomAmex) {
		/* 109 */ this.usrDomAmex = usrDomAmex;
	}

	public String getUsrCpAmex() {
		/* 113 */ return this.usrCpAmex;
	}

	public void setUsrCpAmex(String usrCpAmex) {
		/* 117 */ this.usrCpAmex = usrCpAmex;
	}

	public String getCt() {
		/* 121 */ return this.ct;
	}

	public void setCt(String ct) {
		/* 125 */ this.ct = ct;
	}

	public String getVigencia() {
		/* 129 */ return this.vigencia;
	}

	public void setVigencia(String vigencia) {
		/* 133 */ this.vigencia = vigencia;
	}

	public String getNumerotarjeta() {
		/* 137 */ return this.numerotarjeta;
	}

	public void setNumerotarjeta(String numerotarjeta) {
		/* 141 */ this.numerotarjeta = numerotarjeta;
	}

	public int getIdfranquicia() {
		/* 145 */ return this.idfranquicia;
	}

	public void setIdfranquicia(int idfranquicia) {
		/* 149 */ this.idfranquicia = idfranquicia;
	}

	public int getEstado() {
		/* 153 */ return this.estado;
	}

	public void setEstado(int estado) {
		/* 157 */ this.estado = estado;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/Card.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */