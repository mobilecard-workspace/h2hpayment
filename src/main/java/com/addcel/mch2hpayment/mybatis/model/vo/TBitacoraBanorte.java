package com.addcel.mch2hpayment.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TBitacoraBanorte {
	private long id_usuario;
	private long id_bitacora;
	private long transaction_number;
	private long id_card;
	private long account_id;
	private double amount;
	private String idioma;
	private String bank_reference;
	private double comision;
	private String texto;
	private String referencia;
	private String resultado_payw;
	private String resultado_aut;
	private String banco_emisor;
	private String tipo_tarjeta;
	private String marca_tarjeta;
	private String codigo_aut;
	private String status3DS;
	private String concepto;
	private String tarjeta_transferencia;
	private String nombre_destino;
	private String banco_destino;
	private String cuenta_clabe_destino;
	private int id_aplicacion;
	private double lat;
	private double lon;

	public double getLat() {
		/* 42 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 48 */ this.lat = lat;
	}

	public double getLon() {
		/* 54 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 60 */ this.lon = lon;
	}

	public void setId_aplicacion(int id_aplicacion) {
		/* 66 */ this.id_aplicacion = id_aplicacion;
	}

	public int getId_aplicacion() {
		/* 70 */ return this.id_aplicacion;
	}

	public String getNombre_destino() {
		/* 74 */ return this.nombre_destino;
	}

	public void setNombre_destino(String nombre_destino) {
		/* 80 */ this.nombre_destino = nombre_destino;
	}

	public String getBanco_destino() {
		/* 86 */ return this.banco_destino;
	}

	public void setBanco_destino(String banco_destino) {
		/* 92 */ this.banco_destino = banco_destino;
	}

	public String getCuenta_clabe_destino() {
		/* 98 */ return this.cuenta_clabe_destino;
	}

	public void setCuenta_clabe_destino(String cuenta_clabe_destino) {
		/* 104 */ this.cuenta_clabe_destino = cuenta_clabe_destino;
	}

	public String getConcepto() {
		/* 110 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 116 */ this.concepto = concepto;
	}

	public String getTarjeta_transferencia() {
		/* 122 */ return this.tarjeta_transferencia;
	}

	public void setTarjeta_transferencia(String tarjeta_transferencia) {
		/* 128 */ this.tarjeta_transferencia = tarjeta_transferencia;
	}

	public void setStatus3DS(String status3ds) {
		/* 134 */ this.status3DS = status3ds;
	}

	public String getStatus3DS() {
		/* 138 */ return this.status3DS;
	}

	public String getTexto() {
		/* 142 */ return this.texto;
	}

	public void setTexto(String texto) {
		/* 148 */ this.texto = texto;
	}

	public String getReferencia() {
		/* 154 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 160 */ this.referencia = referencia;
	}

	public String getResultado_payw() {
		/* 166 */ return this.resultado_payw;
	}

	public void setResultado_payw(String resultado_payw) {
		/* 172 */ this.resultado_payw = resultado_payw;
	}

	public String getResultado_aut() {
		/* 178 */ return this.resultado_aut;
	}

	public void setResultado_aut(String resultado_aut) {
		/* 184 */ this.resultado_aut = resultado_aut;
	}

	public String getBanco_emisor() {
		/* 190 */ return this.banco_emisor;
	}

	public void setBanco_emisor(String banco_emisor) {
		/* 196 */ this.banco_emisor = banco_emisor;
	}

	public String getTipo_tarjeta() {
		/* 202 */ return this.tipo_tarjeta;
	}

	public void setTipo_tarjeta(String tipo_tarjeta) {
		/* 208 */ this.tipo_tarjeta = tipo_tarjeta;
	}

	public String getMarca_tarjeta() {
		/* 214 */ return this.marca_tarjeta;
	}

	public void setMarca_tarjeta(String marca_tarjeta) {
		/* 220 */ this.marca_tarjeta = marca_tarjeta;
	}

	public String getCodigo_aut() {
		/* 226 */ return this.codigo_aut;
	}

	public void setCodigo_aut(String codigo_aut) {
		/* 232 */ this.codigo_aut = codigo_aut;
	}

	public void setComision(double comision) {
		/* 238 */ this.comision = comision;
	}

	public double getComision() {
		/* 242 */ return this.comision;
	}

	public void setBank_reference(String bank_reference) {
		/* 246 */ this.bank_reference = bank_reference;
	}

	public String getBank_reference() {
		/* 250 */ return this.bank_reference;
	}

	public long getId_card() {
		/* 254 */ return this.id_card;
	}

	public void setId_card(long id_card) {
		/* 260 */ this.id_card = id_card;
	}

	public long getAccount_id() {
		/* 266 */ return this.account_id;
	}

	public void setAccount_id(long account_id) {
		/* 272 */ this.account_id = account_id;
	}

	public double getAmount() {
		/* 278 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 284 */ this.amount = amount;
	}

	public String getIdioma() {
		/* 290 */ return this.idioma;
	}

	public void setIdioma(String idioma) {
		/* 296 */ this.idioma = idioma;
	}

	public long getId_usuario() {
		/* 302 */ return this.id_usuario;
	}

	public void setId_usuario(long id_usuario) {
		/* 306 */ this.id_usuario = id_usuario;
	}

	public long getId_bitacora() {
		/* 310 */ return this.id_bitacora;
	}

	public void setId_bitacora(long id_bitacora) {
		/* 314 */ this.id_bitacora = id_bitacora;
	}

	public long getTransaction_number() {
		/* 318 */ return this.transaction_number;
	}

	public void setTransaction_number(long transaction_number) {
		/* 322 */ this.transaction_number = transaction_number;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/TBitacoraBanorte.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */