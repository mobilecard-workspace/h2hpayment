package com.addcel.mch2hpayment.mybatis.model.vo;

public class TransaccionBanorte {
	private String NUMERO_CONTROL;
	private String REFERENCIA;
	private String FECHA_RSP_CTE;
	private String CODIGO_PAYW;
	private String RESULTADO_AUT;
	private String TEXTO;
	private String RESULTADO_PAYW;
	private String BANCO_EMISOR;
	private String FECHA_REQ_CTE;
	private String CODIGO_AUT;
	private String ID_AFILIACION;
	private String TIPO_TARJETA;
	private String MARCA_TARJETA;

	public String getNUMERO_CONTROL() {
		/* 32 */ return this.NUMERO_CONTROL;
	}

	public void setNUMERO_CONTROL(String nUMERO_CONTROL) {
		/* 36 */ this.NUMERO_CONTROL = nUMERO_CONTROL;
	}

	public String getREFERENCIA() {
		/* 40 */ return this.REFERENCIA;
	}

	public void setREFERENCIA(String rEFERENCIA) {
		/* 44 */ this.REFERENCIA = rEFERENCIA;
	}

	public String getFECHA_RSP_CTE() {
		/* 48 */ return this.FECHA_RSP_CTE;
	}

	public void setFECHA_RSP_CTE(String fECHA_RSP_CTE) {
		/* 52 */ this.FECHA_RSP_CTE = fECHA_RSP_CTE;
	}

	public String getCODIGO_PAYW() {
		/* 56 */ return this.CODIGO_PAYW;
	}

	public void setCODIGO_PAYW(String cODIGO_PAYW) {
		/* 60 */ this.CODIGO_PAYW = cODIGO_PAYW;
	}

	public String getRESULTADO_AUT() {
		/* 64 */ return this.RESULTADO_AUT;
	}

	public void setRESULTADO_AUT(String rESULTADO_AUT) {
		/* 68 */ this.RESULTADO_AUT = rESULTADO_AUT;
	}

	public String getTEXTO() {
		/* 72 */ return this.TEXTO;
	}

	public void setTEXTO(String tEXTO) {
		/* 76 */ this.TEXTO = tEXTO;
	}

	public String getRESULTADO_PAYW() {
		/* 80 */ return this.RESULTADO_PAYW;
	}

	public void setRESULTADO_PAYW(String rESULTADO_PAYW) {
		/* 84 */ this.RESULTADO_PAYW = rESULTADO_PAYW;
	}

	public String getBANCO_EMISOR() {
		/* 88 */ return this.BANCO_EMISOR;
	}

	public void setBANCO_EMISOR(String bANCO_EMISOR) {
		/* 92 */ this.BANCO_EMISOR = bANCO_EMISOR;
	}

	public String getFECHA_REQ_CTE() {
		/* 96 */ return this.FECHA_REQ_CTE;
	}

	public void setFECHA_REQ_CTE(String fECHA_REQ_CTE) {
		/* 100 */ this.FECHA_REQ_CTE = fECHA_REQ_CTE;
	}

	public String getCODIGO_AUT() {
		/* 104 */ return this.CODIGO_AUT;
	}

	public void setCODIGO_AUT(String cODIGO_AUT) {
		/* 108 */ this.CODIGO_AUT = cODIGO_AUT;
	}

	public String getID_AFILIACION() {
		/* 112 */ return this.ID_AFILIACION;
	}

	public void setID_AFILIACION(String iD_AFILIACION) {
		/* 116 */ this.ID_AFILIACION = iD_AFILIACION;
	}

	public String getTIPO_TARJETA() {
		/* 120 */ return this.TIPO_TARJETA;
	}

	public void setTIPO_TARJETA(String tIPO_TARJETA) {
		/* 124 */ this.TIPO_TARJETA = tIPO_TARJETA;
	}

	public String getMARCA_TARJETA() {
		/* 128 */ return this.MARCA_TARJETA;
	}

	public void setMARCA_TARJETA(String mARCA_TARJETA) {
		/* 132 */ this.MARCA_TARJETA = mARCA_TARJETA;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/mybatis/model/vo/TransaccionBanorte.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */