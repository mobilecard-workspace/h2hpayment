package com.addcel.mch2hpayment.utils;

import java.util.HashMap;

public class Constantes {
	public static final String CURRENCY = "484";
	public static final String ADDRESS = "PROSA";
	public static final String MERCHANT = "8039159";
	public static final String STORE = "1234";
	public static final String TERM = "001";
	public static final String URLBACK = "http://199.231.161.38:8080/MCH2Hpayment/comercio-con";
	public static final String key = "Xsw23Dsa3Adv2scDE";
	public static final String URLBACKPAY = "http://199.231.161.38:8081/apiPagoTerceros/payworks/respuesta";
	public static final String URLBACK3DSPAYW = "http://199.231.161.38:8081/MCH2HPayment/payworks/payworksRec3DRespuesta";
	public static final String VAR_PAYW_URL_BACK_W2 = "http://199.231.161.38:8081/MCH2HPayment/payworks/payworks2RecRespuesta";
	public static final String urlSendmail = "http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel";
	public static final String Path_images = "/usr/java/resources/images/Addcel/";
	/* 22 */ public static HashMap<String, String> errorPayW = new HashMap<>();

	static {
		/* 25 */ errorPayW.put("200", "Indica que la transacción es segura y se puede enviar a Payworks.");
		/* 26 */ errorPayW.put("201",
				"Se detecto un error general en el sistema de Visa o Master Card, favor de esperar unos momentos para reintentar la transacción.");
		/* 27 */ errorPayW.put("421",
				"El servicio 3D Secure no está disponible, favor de esperar unos momentos para reintentar la transacción.");
		/* 28 */ errorPayW.put("422", "Se produjo un problema genérico al momento de realizar la Autenticación.");
		/* 29 */ errorPayW.put("423", "La Autenticación de la Tarjeta no fue exitosa.");
		/* 30 */ errorPayW.put("424",
				"Autenticación 3D Secure no fue completada, no está ingresando correctamente la contraseña 3D Secure.");
		/* 31 */ errorPayW.put("425",
				"Autenticación Inválida, no está ingresando correctamente la contraseña3D Secure.");
		/* 32 */ errorPayW.put("430", "Tarjeta de Crédito nulo, la numero de Tarjeta se envió vacía.");
		/* 33 */ errorPayW.put("431", "Fecha de expiración nulo, la fecha de expricacion se envió vacía.");
		/* 34 */ errorPayW.put("432", "Monto nulo, la variable Total se envió vacía.");
		/* 35 */ errorPayW.put("433", "Id del comercio nulo, la variable MerchantId se envió vacía.");
		/* 36 */ errorPayW.put("434", "Liga de retorno nula, la variable ForwardPath se envió vacía.");
		/* 37 */ errorPayW.put("435", "Nombre del comercio nulo, la variable MerchantName se envió vacía.");
		/* 38 */ errorPayW.put("436", "Formato de TC incorrecto, la variable Card debe ser de 16 dígitos.");
		/* 39 */ errorPayW.put("437",
				"Formato de Fecha de Expiración incorrecto, la variable Expires debe tener el siguiente formato: YY/MM donde YY se refiere al año y MM se refiere al mes de vencimiento de la tarjeta.");
		/* 40 */ errorPayW.put("438", "Fecha de Expiración incorrecto, indica que el plástico esta vencido.");
		/* 41 */ errorPayW.put("439",
				"Monto incorrecto, la variable Total debe ser un número menor a 999,999,999,999.## con la fracción decimal opcional, esta debe ser a lo más de 2 décimas.");
		/* 42 */ errorPayW.put("440",
				"Formato de nombre del comercio incorrecto, debe ser una cadena de máximo 25 caracteres alfanuméricos.");
		/* 43 */ errorPayW.put("441", "Marca de Tarjeta nulo, la variable CardType se envió vacía.");
		/* 44 */ errorPayW.put("442",
				"Marca de Tarjeta incorrecta, debe ser uno de los siguientes valores: VISA (para tarjetas Visa) o MC (para tarjetas Master Card).");
		/* 45 */ errorPayW.put("443",
				"CardType incorrecto, se ha especificado el CardType como VISA, sin embargo, el Bin de la tarjeta indica que esta no es Visa.");
		/* 46 */ errorPayW.put("444",
				"CardType incorrecto, se ha especificado el CardType como MC, sin embargo, el Bin de la tarjeta indica que esta no es Master Card.");
		/* 47 */ errorPayW.put("446", "Monto incorrecto, la variable Total debe ser superior a 1.0 pesos.");
		/* 48 */ errorPayW.put("498",
				"Transacción expirada. Indica que la transacción sobre paso el límite de tiempo de respuesta esperado.");
		/* 49 */ errorPayW.put("499",
				"Usuario excedió en tiempo de respuesta. Indica que el usuario tardo en capturar la información de 3D Secure mayor al tiempo esperado.");
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/utils/Constantes.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */