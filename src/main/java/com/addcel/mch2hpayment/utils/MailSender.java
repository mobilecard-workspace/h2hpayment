package com.addcel.mch2hpayment.utils;

import java.io.Serializable;

public class MailSender implements Serializable {
	private String[] to;
	private String[] bcc;
	private String[] cc;
	private String[] attachments;
	private Object[] cid;
	private String from;
	private String subject;
	private String body;

	public String[] getTo() {
		/* 16 */ return this.to;
	}

	public void setTo(String[] to) {
		/* 19 */ this.to = to;
	}

	public String[] getBcc() {
		/* 22 */ return this.bcc;
	}

	public void setBcc(String[] bcc) {
		/* 25 */ this.bcc = bcc;
	}

	public String[] getCc() {
		/* 28 */ return this.cc;
	}

	public void setCc(String[] cc) {
		/* 31 */ this.cc = cc;
	}

	public String[] getAttachments() {
		/* 34 */ return this.attachments;
	}

	public void setAttachments(String[] attachments) {
		/* 37 */ this.attachments = attachments;
	}

	public String getFrom() {
		/* 40 */ return this.from;
	}

	public void setFrom(String from) {
		/* 43 */ this.from = from;
	}

	public String getSubject() {
		/* 46 */ return this.subject;
	}

	public void setSubject(String subject) {
		/* 49 */ this.subject = subject;
	}

	public String getBody() {
		/* 52 */ return this.body;
	}

	public void setBody(String body) {
		/* 55 */ this.body = body;
	}

	public Object[] getCid() {
		/* 58 */ return this.cid;
	}

	public void setCid(Object[] cid) {
		/* 61 */ this.cid = cid;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/utils/MailSender.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */