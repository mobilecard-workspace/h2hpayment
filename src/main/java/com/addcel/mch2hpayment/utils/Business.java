package com.addcel.mch2hpayment.utils;

import com.addcel.mch2hpayment.spring.services.EmailSenderClient;
import com.addcel.mch2hpayment.utils.MailSender;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import crypto.Crypto;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Business {
	/* 25 */ private static final Logger LOGGER = LoggerFactory.getLogger(com.addcel.mch2hpayment.utils.Business.class);

	private static final String TelefonoServicio = "5525963513";

	public static String digest(String text) {
		/* 32 */ String digest = "";
		/* 33 */ BigInteger bigIntDgst = null;

		try {
			/* 36 */ LOGGER.debug("DIGEST cadena: " + text);
			/* 37 */ MessageDigest md = MessageDigest.getInstance("SHA-1");
			/* 38 */ md.update(text.getBytes(), 0, text.length());
			/* 39 */ bigIntDgst = new BigInteger(1, md.digest());

			/* 41 */ digest = String.format("%040x", new Object[] { bigIntDgst });
			/* 42 */ } catch (NoSuchAlgorithmException e) {
			/* 43 */ LOGGER.debug("Error al encriptar - digest", e);
		}
		/* 45 */ LOGGER.debug("DIGEST calculado: " + digest);
		/* 46 */ return digest;
	}

	public static String formatoMontoProsa(String monto) {
		/* 50 */ String varTotal = "000";
		/* 51 */ String pesos = null;
		/* 52 */ String centavos = null;
		/* 53 */ if (monto.contains(".")) {
			/* 54 */ pesos = monto.substring(0, monto.indexOf("."));
			/* 55 */ centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			/* 56 */ if (centavos.length() < 2) {
				/* 57 */ centavos = centavos.concat("0");
			} else {
				/* 59 */ centavos = centavos.substring(0, 2);
			}
			/* 61 */ varTotal = String.valueOf(pesos) + centavos;
		} else {
			/* 63 */ varTotal = monto.concat("00");
		}
		/* 65 */ LOGGER.info("Monto a cobrar 3dSecure: " + varTotal);
		/* 66 */ return varTotal;
	}

	public static String getSMS(String Telefono) {
		/* 70 */ return Crypto.aesDecrypt(parsePass("5525963513"), Telefono);
	}

	public static String parsePass(String pass) {
		/* 74 */ int len = pass.length();
		/* 75 */ String key = "";

		/* 77 */ for (int i = 0; i < 32 / len; i++) {
			/* 78 */ key = String.valueOf(key) + pass;
		}

		/* 81 */ int carry = 0;
		/* 82 */ while (key.length() < 32) {
			/* 83 */ key = String.valueOf(key) + pass.charAt(carry);
			/* 84 */ carry++;
		}
		/* 86 */ return key;
	}

	public static String formatoMontoPayworks(String monto) {
		/* 90 */ String varTotal = "000";
		/* 91 */ String pesos = null;
		/* 92 */ String centavos = null;
		/* 93 */ if (monto.contains(".")) {
			/* 94 */ pesos = monto.substring(0, monto.indexOf("."));
			/* 95 */ centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			/* 96 */ if (centavos.length() < 2) {
				/* 97 */ centavos = centavos.concat("0");
			} else {
				/* 99 */ centavos = centavos.substring(0, 2);
			}
			/* 101 */ varTotal = String.valueOf(pesos) + "." + centavos;
		} else {
			/* 103 */ varTotal = monto.concat(".00");
		}
		/* 105 */ return varTotal;
	}

	public static boolean SendMailMicrosoft(String sMail, String Cadena, String wAsunto, HashMap<String, String> datos,
			String urlEmailSender) {
		/* 110 */ boolean flag = false;

		try {
			/* 114 */ if (datos.containsKey("NOMBRE"))
				/* 115 */ Cadena = Cadena.replace("#NOMBRE#", datos.get("NOMBRE"));
			/* 116 */ if (datos.containsKey("FECHA"))
				/* 117 */ Cadena = Cadena.replace("#FECHA#", datos.get("FECHA"));
			/* 118 */ if (datos.containsKey("AUTBAN"))
				/* 119 */ Cadena = Cadena.replace("#AUTBAN#", datos.get("AUTBAN"));
			/* 120 */ if (datos.containsKey("IMPORTE"))
				/* 121 */ Cadena = Cadena.replace("#IMPORTE#", datos.get("IMPORTE"));
			/* 122 */ if (datos.containsKey("COMISION"))
				/* 123 */ Cadena = Cadena.replace("#COMISION#", datos.get("COMISION"));
			/* 124 */ if (datos.containsKey("CUENTA")) {
				/* 125 */ Cadena = Cadena.replace("#CUENTA#", datos.get("CUENTA"));
			}

			/* 129 */ MailSender correo = new MailSender();
			/* 130 */ String from = "no-reply@addcel.com";
			/* 131 */ String[] to = { sMail };
			/* 132 */ correo.setCc(new String[0]);
			/* 133 */ correo.setBody(Cadena);
			/* 134 */ correo.setFrom(from);
			/* 135 */ correo.setSubject(wAsunto);
			/* 136 */ correo.setTo(to);
			/* 137 */ correo.setCid((Object[]) new String[] { "/usr/java/resources/images/Addcel/BanorteH2H.PNG" });

			/* 139 */ Gson gson = new Gson();
			/* 140 */ String json = gson.toJson(correo);
			/* 141 */ LOGGER.info("antes de enviar mail");
			/* 142 */ EmailSenderClient clientRest = new EmailSenderClient();
			/* 143 */ clientRest.SendMail(correo, urlEmailSender);

			/* 146 */ LOGGER.info("despues de enviar mail");
			/* 147 */ LOGGER.debug("mensaje hotmail enviado : " + sMail);
			/* 148 */ } catch (Exception ex) {
			/* 149 */ LOGGER.error("Error al enviar email ", ex);
			/* 150 */ LOGGER.error("Ocurrio un error al enviar email hotmail {}:  {}", sMail, ex);
		}
		/* 152 */ return flag;
	}

	public static void MailSender(String data) {
		/* 156 */ String line = null;
		/* 157 */ StringBuilder sb = new StringBuilder();
		try {
			/* 159 */ LOGGER.info("Iniciando proceso de envio email Microsoft. ");
			/* 160 */ URL url = new URL("http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel");
			/* 161 */ LOGGER.info("Conectando con http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel");
			/* 162 */ HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			/* 164 */ urlConnection.setDoOutput(true);
			/* 165 */ urlConnection.setRequestProperty("Content-Type", "application/json");
			/* 166 */ urlConnection.setRequestProperty("Accept", "application/json");
			/* 167 */ urlConnection.setRequestMethod("POST");

			/* 169 */ OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			/* 170 */ writter.write(data);
			/* 171 */ writter.flush();

			/* 173 */ LOGGER.info("Datos enviados, esperando respuesta");

			/* 175 */ BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			/* 177 */ while ((line = reader.readLine()) != null) {
				/* 178 */ sb.append(line);
			}

			/* 181 */ LOGGER.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
			/* 182 */ } catch (Exception ex) {
			/* 183 */ LOGGER.error("Error en: sendMail, al enviar el email ", ex);
		}
	}

	public static String removeAcentos(String input) {
		/* 190 */ String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";

		/* 192 */ String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
		/* 193 */ String output = input;
		/* 194 */ for (int i = 0; i < original.length(); i++) {
			/* 196 */ output = output.replace(original.charAt(i), ascii.charAt(i));
		}
		/* 198 */ return output;
	}

	public static String getjson(Object obj) {
		/* 202 */ GsonBuilder gsonBuilder = new GsonBuilder();
		/* 203 */ gsonBuilder.serializeNulls();
		/* 204 */ Gson gson = gsonBuilder.create();

		/* 206 */ return gson.toJson(obj);
	}

	public static String maskCard(String card, String placeholder) {
		/* 222 */ String result = "";
		/* 223 */ LOGGER.debug("Longitud tarjeta: {}", Integer.valueOf(card.length()));
		/* 224 */ for (int i = 0; i < card.length(); i++) {
			/* 225 */ if (i >= 4 && i <= 11) {
				/* 226 */ if ((i + 1) % 4 == 0) {
					/* 227 */ result = String.valueOf(result) + placeholder + " ";
				} else {
					/* 229 */ result = String.valueOf(result) + placeholder;
				}

				/* 232 */ } else if ((i + 1) % 4 == 0) {
				/* 233 */ result = String.valueOf(result) + card.charAt(i) + " ";
			} else {
				/* 235 */ result = String.valueOf(result) + card.charAt(i);
			}
		}

		/* 239 */ return result;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/utils/Business.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */