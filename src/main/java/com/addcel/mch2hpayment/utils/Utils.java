package com.addcel.mch2hpayment.utils;

import com.addcel.utils.AddcelCrypto;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {
	/* 16 */ private static final SimpleDateFormat SDF = new SimpleDateFormat("ddhhmmssSSS");

	/* 18 */ private static final Logger LOGGER = LoggerFactory.getLogger(com.addcel.mch2hpayment.utils.Utils.class);

	private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_Antad_header_600.PNG";

	public static String digest(String text) {
		/* 27 */ String digest = "";
		/* 28 */ BigInteger bigIntDgst = null;
		try {
			/* 30 */ MessageDigest md = MessageDigest.getInstance("MD5");
			/* 31 */ md.update(text.getBytes(), 0, text.length());
			/* 32 */ bigIntDgst = new BigInteger(1, md.digest());
			/* 33 */ digest = bigIntDgst.toString(16);
			/* 34 */ digest = String.format("%040x", new Object[] { bigIntDgst });
			/* 35 */ } catch (NoSuchAlgorithmException e) {
			/* 36 */ LOGGER.info("Error al encriptar - digest", e);
		}
		/* 38 */ return digest;
	}

	public static String getFecha(String formato) {
		/* 42 */ String dateString = null;
		/* 43 */ SimpleDateFormat format = new SimpleDateFormat(formato);
		try {
			/* 45 */ Date now = new Date();
			/* 46 */ dateString = format.format(now);
		}
		/* 48 */ catch (Exception pe) {
			/* 49 */ System.out.println("ERROR: Cannot parse \"" + dateString + "\"");
		}
		/* 51 */ return dateString;
	}

	public static String encryptJson(String json) {
		/* 55 */ return AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
	}

	public static String decryptJson(String json) {
		/* 59 */ return AddcelCrypto.decryptSensitive(json);
	}

	public static String encryptHard(String json) {
		/* 63 */ return AddcelCrypto.encryptHard(json);
	}

	public static String formatoMontoPayworks(String monto) {
		/* 67 */ String varTotal = "000";
		/* 68 */ String pesos = null;
		/* 69 */ String centavos = null;
		/* 70 */ if (monto.contains(".")) {
			/* 71 */ pesos = monto.substring(0, monto.indexOf("."));
			/* 72 */ centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			/* 73 */ if (centavos.length() < 2) {
				/* 74 */ centavos = centavos.concat("0");
			} else {
				/* 76 */ centavos = centavos.substring(0, 2);
			}
			/* 78 */ varTotal = String.valueOf(pesos) + "." + centavos;
		} else {
			/* 80 */ varTotal = monto.concat(".00");
		}
		/* 82 */ return varTotal;
	}

	public static String cambioAcento(String cadena) {
		try {
			/* 87 */ if (cadena != null) {
				/* 88 */ cadena = cadena.replaceAll("Ã¡", "á");
				/* 89 */ cadena = cadena.replaceAll("Ã³", "ó");
			}
			/* 91 */ } catch (Exception e) {
			/* 92 */ LOGGER.error("Error al cambiar acentos: " + e.getMessage());
		}
		/* 94 */ return cadena;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/utils/Utils.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */