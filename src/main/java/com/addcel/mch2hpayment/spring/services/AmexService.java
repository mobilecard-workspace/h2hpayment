package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.spring.model.AmexAutorization;
import com.addcel.mch2hpayment.spring.model.RespuestaAmex;
import com.google.gson.Gson;
import java.text.DecimalFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class AmexService {
	/* 23 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.AmexService.class);

	private static final String AMEX_WEB_URL = "http://localhost:80/AmexWeb/AmexAuthorization";

	/* 27 */ private Gson gson = new Gson();

	public RespuestaAmex solicitaAutorizacionAmex(AmexAutorization amexAut) {
		RespuestaAmex respAmex;
		/* 34 */ String json = null;
		/* 35 */ DecimalFormat df = new DecimalFormat("#.00");
		/* 36 */ double total = 0.0D;

		try {
			/* 39 */ total = amexAut.getMonto() + amexAut.getComision();

			/* 41 */ LOGGER.info("SOLICITANDO AUTORIZACION AMEX - TARJETA: " + amexAut.getTarjeta() +
			/* 42 */ ",  VIGENCIA: " + amexAut.getVigencia().replace("/", "") + ", MONTO: " + total +
			/* 43 */ ", CID: " + amexAut.getCvv2() + ",  DIR: " + amexAut.getDom_amex());
			/* 44 */ RestTemplate restClient = new RestTemplate();
			/* 45 */ HttpHeaders headers = new HttpHeaders();
			/* 46 */ headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			/* 48 */ LinkedMultiValueMap linkedMultiValueMap = new LinkedMultiValueMap();
			/* 49 */ linkedMultiValueMap.add("tarjeta", amexAut.getTarjeta());
			/* 50 */ linkedMultiValueMap.add("vigencia", amexAut.getVigencia().replace("/", ""));
			/* 51 */ linkedMultiValueMap.add("producto", amexAut.getProducto());
			/* 52 */ linkedMultiValueMap.add("afiliacion", amexAut.getAfiliacion());
			/* 53 */ linkedMultiValueMap.add("cid", amexAut.getCvv2());
			/* 54 */ linkedMultiValueMap.add("monto", df.format(total).replace(",", "."));
			/* 55 */ linkedMultiValueMap.add("direccion", amexAut.getDom_amex());
			/* 56 */ HttpEntity<MultiValueMap<String, String>> request = new HttpEntity(linkedMultiValueMap,
					(MultiValueMap) headers);

			/* 59 */ ResponseEntity<String> response = restClient.postForEntity(
					"http://localhost:80/AmexWeb/AmexAuthorization", request, String.class, new Object[0]);
			/* 60 */ json = (String) response.getBody();
			/* 61 */ LOGGER.info("RESPUESTA DE AMEX: " + json);
			/* 62 */ respAmex = (RespuestaAmex) this.gson.fromJson(json, RespuestaAmex.class);
			/* 63 */ LOGGER
					.debug("RESPUESTA AMEX - CODE: " + respAmex.getCode() + ", TRAN: " + respAmex.getTransaction() +
					/* 64 */ ", DESCRIPT:  " + respAmex.getDsc() + ", ERROR: " + respAmex.getError() + ", ERROR DESC: "
							+ respAmex.getErrorDsc());
		}
		/* 66 */ catch (Exception e) {
			/* 67 */ e.printStackTrace();
			/* 68 */ LOGGER.error("Error al enviar pago Amex...", e);
			/* 69 */ respAmex = new RespuestaAmex();
			/* 70 */ respAmex.setCode("101");
		}
		/* 72 */ return respAmex;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/AmexService.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */