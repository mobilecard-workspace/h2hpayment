package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.TokenClientMC;
import com.addcel.mch2hpayment.client.model.ValidateTokenRequest;
import com.addcel.mch2hpayment.client.model.ValidateTokenResponse;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class TokenClientMCImpl implements TokenClientMC {
	/* 21 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.TokenClientMCImpl.class);

	private static final String PATH_VALIDATE_TOKEN = "http://localhost:80/Tokenizer/{idApp}/{idPais}/{idioma}/validateToken";

	/* 25 */ private Gson GSON = new Gson();

	public ValidateTokenResponse validateToken(ValidateTokenRequest req, Integer idApp, Integer idPais, String idioma,
			String token) {
		/* 30 */ ValidateTokenResponse response = new ValidateTokenResponse();

		try {
			/* 33 */ RestTemplate restTemplate = new RestTemplate();
			/* 34 */ HttpHeaders headers = new HttpHeaders();
			/* 35 */ headers.add("Authorization", token);
			/* 36 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 37 */ HttpEntity<ValidateTokenRequest> request = new HttpEntity(req, (MultiValueMap) headers);
			/* 38 */ ResponseEntity<String> respBP = restTemplate
					.exchange(
							"http://localhost:80/Tokenizer/{idApp}/{idPais}/{idioma}/validateToken"
//									.replace("{idApp}", (CharSequence) idApp).replace("{idPais}", (CharSequence) idPais)
									.replace("{idApp}", String.valueOf(idApp)).replace("{idPais}", String.valueOf(idPais))
									.replace("{idioma}", idioma),
							HttpMethod.POST, request, String.class, new Object[0]);
			/* 39 */ LOGGER.info("RESPUESTA DE VALIDATE TOKEN - {}", respBP.getBody());
			/* 40 */ response = (ValidateTokenResponse) this.GSON.fromJson((String) respBP.getBody(),
					ValidateTokenResponse.class);
			/* 41 */ } catch (Exception ex) {
			/* 42 */ LOGGER.error("ERROR AL VALIDAR TOKEN", ex);
			/* 43 */ response.setCode(Integer.valueOf(1000));
			/* 44 */ response.setMessage("NO SE PUDO COMPLETAR LA VALIDACION DEL TOKEN");
		}

		/* 47 */ return response;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/TokenClientMCImpl.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */