package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.ThreatMetrixClient;
import com.addcel.mch2hpayment.client.model.ThreatMetrixRequest;
import com.addcel.mch2hpayment.client.model.ThreatMetrixResponse;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class ThreatMetrixClientImpl implements ThreatMetrixClient {
	private static final String THREATHMETRIX_URL_PAYMENT = "http://localhost/ThreatMetrix/api/investigarPago";
	/* 23 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.ThreatMetrixClientImpl.class);

	/* 25 */ private Gson GSON = new Gson();

	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request) {
		/* 29 */ ThreatMetrixResponse validate = null;
		/* 30 */ ResponseEntity<String> responseEntity = null;
		try {
			/* 32 */ RestTemplate rest = new RestTemplate();
			/* 33 */ HttpHeaders headers = new HttpHeaders();
			/* 34 */ headers.set("Accept", "application/json");
			/* 35 */ headers.set("Content-Type", "application/json");
			/* 36 */ headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
			/* 37 */ HttpEntity<ThreatMetrixRequest> entity = new HttpEntity(request, (MultiValueMap) headers);
			/* 38 */ LOGGER.info("PETICION A TMX  - {}", this.GSON.toJson(request));
			/* 39 */ responseEntity = rest.exchange("http://localhost/ThreatMetrix/api/investigarPago", HttpMethod.POST,
					entity, String.class, new Object[0]);
			/* 40 */ LOGGER.info("RESPUESTA DE TMX - {}", responseEntity.getBody());
			/* 41 */ validate = (ThreatMetrixResponse) this.GSON.fromJson((String) responseEntity.getBody(),
					ThreatMetrixResponse.class);
			/* 42 */ } catch (Exception e) {
			/* 43 */ e.printStackTrace();
			/* 44 */ validate = new ThreatMetrixResponse();
			/* 45 */ validate.setCode(Integer.valueOf(-250));
			/* 46 */ validate.setMessage("Su transaccion no ha podido ser analizado para procesar su pago.");
		}
		/* 48 */ return validate;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/ThreatMetrixClientImpl.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */