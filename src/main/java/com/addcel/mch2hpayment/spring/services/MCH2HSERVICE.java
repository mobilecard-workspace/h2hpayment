package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueueAccountSignUpRequest;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryRequest;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.ServiceMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.MCTransafersResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.ProcomVO;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.spring.services.H2HCLIENT;
import com.addcel.mch2hpayment.spring.services.UtilsService;
import com.addcel.mch2hpayment.utils.Business;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public class MCH2HSERVICE {
	/* 49 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.MCH2HSERVICE.class);

	/* 52 */ private H2HCLIENT h2hclient = new H2HCLIENT();
	private LoginResponse login;
	/* 54 */ private Gson gson = new Gson();
	@Autowired
	ServiceMapper mapper;

	public void updateTranfers(long id_usuario) {
		/* 59 */ LOGGER.debug("ACTUALIZANDO USER " + id_usuario);
		/* 60 */ this.mapper.updateuserSMS(id_usuario);
		/* 61 */ LOGGER.debug("USUARIO ACTUALIZADO");
	}

	public LoginResponse login() {
		try {
			/* 66 */ LoginRequest login = new LoginRequest();
			/* 67 */ login.setLogin("addcel_banorte");
			/* 68 */ login.setPassword("B4N0rt3%");
			/* 69 */ LOGGER.debug("iniciando login");
			/* 70 */ LoginResponse response = this.h2hclient.login(login, "es");
			/* 71 */ LOGGER.debug("LOGUIN: " + response.getToken() + " " + response.getResult().isSuccess() + " "
					+ response.getResult().getMessage());
			/* 72 */ return response;
			/* 73 */ } catch (Exception ex) {
			/* 74 */ LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			/* 75 */ return new LoginResponse();
		}
	}

	public LogoutResponse logout() {
		try {
			/* 81 */ LogoutRequest logout = new LogoutRequest();
			/* 82 */ logout.setLogin("addcel_banorte");
			/* 83 */ logout.setToken(this.login.getToken());
			/* 84 */ LogoutResponse response = this.h2hclient.logout(logout, "es");
			/* 85 */ LOGGER.debug("[RESPUESTA DE LOGOUT]" + response.getResult().isSuccess());
			/* 86 */ return response;
			/* 87 */ } catch (Exception ex) {
			/* 88 */ LOGGER.error("[ERROR EN LOGOUT]", ex);
			/* 89 */ return new LogoutResponse();
		}
	}

	public MCSignUpResponse getToAccount(long idUsuario, String idioma) {
		/* 94 */ MCSignUpResponse response = new MCSignUpResponse();
		try {
			/* 96 */ this.login = login();
			/* 97 */ String responseBD = this.mapper.H2HBanorte_Business(idUsuario, 0L, 0L, "", 3, 0L, "", "", idioma);

			/* 99 */ response = (MCSignUpResponse) this.gson.fromJson(responseBD, MCSignUpResponse.class);
			/* 100 */ } catch (Exception ex) {
			/* 101 */ LOGGER.error("[ERROR AL OBTENER CUANTAS ASOCIADAS ] [" + idUsuario + "]", ex);
		} finally {
			/* 103 */ logout();
		}
		/* 105 */ return response;
	}

	public BankCodesResponse getBankCodes() {
		/* 109 */ BankCodesResponse response = new BankCodesResponse();
		try {
			/* 111 */ LOGGER.debug("[INICIANDO OBTENCION DE CATALOGO DE BANCOS]");
			/* 112 */ response.setBanks(this.mapper.getBankcodes());
			/* 113 */ response.setIdError(0);
			/* 114 */ response.setMensajeError("");
			/* 115 */ } catch (Exception ex) {
			/* 116 */ LOGGER.error("[ERROR AL OBTENER BNANCOS]", ex);
			/* 117 */ response.setIdError(99);
			/* 118 */ response.setMensajeError("Error inesperado, vuelva a intentarlo");
		}
		/* 120 */ return response;
	}

	public MCSignUpResponse EnqueueAccountSignUp(MCAccount mcAccount) {
		/* 124 */ MCSignUpResponse response = new MCSignUpResponse();

		try {
			/* 128 */ Calendar calendario = new GregorianCalendar();
			/* 129 */ int hora = calendario.get(11);

			/* 133 */ if (hora >= 22 || (hora >= 0 && hora < 2)) {
				/* 134 */ LOGGER.info("[SERVICIO EN MANTENIMIENTO BANORTE]");
				/* 135 */ response.setAccounts(new ArrayList());
				/* 136 */ response.setIdError(88);
				/* 137 */ if (mcAccount.getIdioma().equals("es")) {
					/* 138 */ response.setMensajeError("Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
				} else {
					/* 140 */ response.setMensajeError("Service available during the hours of 2:00 to 22:00.");
				}
				/* 142 */ return response;
			}

			/* 146 */ int sizeNumber = mcAccount.getActNumber().length();

			/* 148 */ int bin = this.mapper.isBinValido(mcAccount.getActNumber().substring(0, 6));

			/* 150 */ if (bin == 1 && sizeNumber != 10 && sizeNumber != 11 && sizeNumber != 18) {
				/* 151 */ response.setAccounts(new ArrayList());
				/* 152 */ response.setIdError(89);
				/* 153 */ if (mcAccount.getIdioma().equals("es")) {
					/* 154 */ response.setMensajeError(
							"El número de tarjeta no se acepta, favor de introducir número de cuenta o clabe");
				} else {
					/* 156 */ response
							.setMensajeError("The card number not accepted, please enter account number or clabe");
					/* 157 */ }
				LOGGER.info("[DETECTAMOS BIN DE TARJETA] ");

				/* 159 */ return response;
			}

			/* 162 */ if (mcAccount.getBankCode().equals("072") || mcAccount.getBankCode().equals("032")) {

				/* 165 */ if (sizeNumber > 11) {

					/* 167 */ response.setAccounts(new ArrayList());
					/* 168 */ response.setIdError(90);
					/* 169 */ if (mcAccount.getIdioma().equals("es")) {
						/* 170 */ response.setMensajeError("Para banco Banorte, introduzca el número de cuenta");
					} else {
						/* 172 */ response.setMensajeError("For Banorte bank, enter the account number");
						/* 173 */ }
					LOGGER.info("[DETECTAMOS CUENTA BANORTE INCONSISTENTE] ");

					/* 175 */ return response;
				}
				/* 177 */ if (mcAccount.getActType().equals("CLABE") && (sizeNumber == 10 || sizeNumber == 11)) {
					/* 178 */ mcAccount.setActType("ACT");

				}
			} else {

				/* 184 */ if (sizeNumber < 18) {
					/* 185 */ response.setAccounts(new ArrayList());
					/* 186 */ response.setIdError(91);
					/* 187 */ if (mcAccount.getIdioma().equals("es")) {
						/* 188 */ response
								.setMensajeError("Para bancos distintos a Banorte, introduzca la clabe interbancaria");
					} else {
						/* 190 */ response.setMensajeError("For banks other than Banorte, enter the interbank clabe");
						/* 191 */ }
					LOGGER.info("[DETECTAMOS CUENTA BANORTE INCONSISTENTE] ");
					/* 192 */ return response;
				}
				/* 194 */ if (mcAccount.getActType().equals("ACT") && sizeNumber == 18) {
					/* 195 */ mcAccount.setActType("CLABE");
				}
			}

			/* 200 */ if (mcAccount.getActType().equals("CLABE") &&
			/* 201 */ !mcAccount.getActNumber().substring(0, 3).equals(mcAccount.getBankCode())) {
				/* 202 */ response.setIdError(92);
				/* 203 */ if (mcAccount.getIdioma().equals("es")) {
					/* 204 */ response
							.setMensajeError("Verifique que la cuenta introducida pertenezca al banco seleccionado.");
				} else {
					/* 206 */ response.setMensajeError("Verify that the account entered belongs to the selected bank.");
					/* 207 */ }
				LOGGER.info("[DETECTAMOS CUENTA INCONSISTENTE CODIGOBANK vs NUMCUENTA] ");
				/* 208 */ return response;
			}

			/* 212 */ this.login = login();
			/* 213 */ EnqueueAccountSignUpRequest signup = new EnqueueAccountSignUpRequest();
			/* 214 */ long clientReference = this.mapper.getMaxIdBitacora();

			/* 216 */ if (mcAccount.getRfc().length() > 13) {
				/* 217 */ mcAccount.setRfc(mcAccount.getRfc().substring(0, 13));
			}
			/* 219 */ if (mcAccount.getActType().equals("ACT") && mcAccount.getActNumber().length() > 11) {
				/* 220 */ mcAccount.setActType("CLABE");
			}

			/* 223 */ AccountRecord account = new AccountRecord();
			/* 224 */ account.setActNumber(mcAccount.getActNumber().trim());
			/* 225 */ account.setActType(mcAccount.getActType().trim());
			/* 226 */ account.setBankCode(mcAccount.getBankCode().trim());
			/* 227 */ account.setContact(
					(mcAccount.getContact() == null) ? "" : Business.removeAcentos(mcAccount.getContact()).trim());
			/* 228 */ account.setEmail(mcAccount.getEmail().trim());
			/* 229 */ account.setHolderName(Business.removeAcentos(mcAccount.getHolderName()).trim());
			/* 230 */ account.setPhone((mcAccount.getPhone() == null) ? "" : mcAccount.getPhone().trim());
			/* 231 */ account.setRfc(mcAccount.getRfc().trim());
			/* 232 */ signup.setAccount(account);
			/* 233 */ signup.setClientReference(String.valueOf((new Date()).getSeconds()) + clientReference);
			/* 234 */ signup.setIdAccount("");
			/* 235 */ signup.setToken(this.login.getToken());

			/* 237 */ EnqueueTransactionResponse responseBanorte = this.h2hclient.EnqueueAccountSignUp(signup,
					mcAccount.getIdioma());

			/* 239 */ if (responseBanorte.getResult().isSuccess()) {
				/* 240 */ String responsBD = this.mapper.H2HBanorte_Business(mcAccount.getIdUsuario(), 0L,
						Long.parseLong(responseBanorte.getTransactionReference()), mcAccount.getAlias(), 2, 0L, "", "",
						mcAccount.getIdioma());
				/* 241 */ response = (MCSignUpResponse) this.gson.fromJson(responsBD, MCSignUpResponse.class);

			} else {

				/* 248 */ response.setIdError(10);
				/* 249 */ response.setMensajeError(responseBanorte.getResult().getMessage());
			}
			/* 251 */ logout();
			/* 252 */ return response;
		}
		/* 254 */ catch (Exception ex) {
			/* 255 */ LOGGER.error("[ERROR AL REGISTAR CUENTA USUARIO]", ex);
			/* 256 */ return response;
		}
	}

	public MCSignUpResponse updateAccount(UpdateAccountRequest request) {
		/* 268 */ MCSignUpResponse response = new MCSignUpResponse();
		try {
			/* 270 */ LOGGER.debug("[INICIANDO ACTUALIZACION CUENTA ]" + request.getIdUsuario());
			/* 271 */ String responseDB = this.mapper.H2HBanorte_Business(request.getIdUsuario(), 0L, 0L,
					request.getAlias().trim(), 4, request.getIdAccount(), request.getTelefono().trim(),
					request.getEmail().trim(), request.getIdioma().trim());

			/* 273 */ response = (MCSignUpResponse) this.gson.fromJson(responseDB, MCSignUpResponse.class);
			/* 274 */ } catch (Exception ex) {
			/* 275 */ response.setIdError(99);
			/* 276 */ if (request.getIdioma().equalsIgnoreCase("es")) {
				/* 277 */ response.setMensajeError("Error inesperado, vuelva a intentarlo");
			} else {
				/* 279 */ response.setMensajeError("Unexpected error, try again");
			}
		}
		/* 282 */ return response;
	}

	public MCSignUpResponse deleteAccount(long idUsuario, long idAccount, String idioma) {
		/* 286 */ MCSignUpResponse response = new MCSignUpResponse();
		try {
			/* 288 */ LOGGER.debug("[INICIANDO ELIMINACION DE CUENTA ]" + idUsuario);
			/* 289 */ String responseDB = this.mapper.H2HBanorte_Business(idUsuario, 0L, 0L, "", 5, idAccount, "", "",
					idioma);

			/* 291 */ response = (MCSignUpResponse) this.gson.fromJson(responseDB, MCSignUpResponse.class);
			/* 292 */ } catch (Exception ex) {
			/* 293 */ if (idioma.equalsIgnoreCase("es")) {
				/* 294 */ response.setMensajeError("Error inesperado, vuelva a intentarlo");
			} else {
				/* 296 */ response.setMensajeError("Unexpected error, try again");
			}
		}

		/* 300 */ return response;
	}

	public TransactionEnquiryResponse TransactionEnquiry(String transactionReference, String idioma) {
		try {
			/* 309 */ this.login = login();
			/* 310 */ TransactionEnquiryRequest request = new TransactionEnquiryRequest();
			/* 311 */ request.setToken(this.login.getToken());
			/* 312 */ request.setTransactionReference(transactionReference);
			/* 313 */ LOGGER.debug("[CONSULTANDO ESTATUS DE TRANSACCION] " + request.getTransactionReference());
			/* 314 */ TransactionEnquiryResponse response = this.h2hclient.TransactionEnquiry(request, idioma);
			/* 315 */ LOGGER.debug("[FINALIZANDO CONSULTA DE ESTATUS]" + request.getTransactionReference());
			/* 316 */ return response;
			/* 317 */ } catch (Exception ex) {
			/* 318 */ LOGGER.error("[ERROR AL CONSULTAR ESTATUS DE TRANSACCION] " + transactionReference, ex);
			/* 319 */ return null;
		} finally {
			/* 321 */ logout();
		}
	}

	public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora) {
		/* 330 */ EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
		/* 331 */ EnqueueTransactionResponse response = new EnqueueTransactionResponse();
		/* 332 */ MCTransafersResponse responseTransfer = new MCTransafersResponse();
		/* 333 */ ModelAndView view = new ModelAndView("transfers_response");
		/* 334 */ McBaseResponse McResponse = new McBaseResponse();
		try {
			/* 336 */ this.login = login();
			/* 337 */ TBitacoraVO bitacora = new TBitacoraVO();

			/* 339 */ SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			/* 340 */ responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			/* 341 */ responseTransfer.setComision(1.0D);
			/* 342 */ responseTransfer.setFecha(DFormat.format(new Date()));
			/* 343 */ responseTransfer.setMontoTransfer(paymentRequest.getAmount());

			/* 347 */ User user = this.mapper.getUserData(paymentRequest.getIdUser());
			/* 348 */ Card card = this.mapper.getCard(paymentRequest.getIdUser(), paymentRequest.getIdCard());
			/* 349 */ AccountRecord fromAccount = new AccountRecord();

			/* 351 */ responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));

			/* 353 */ fromAccount.setActNumber("0276948693");
			/* 354 */ fromAccount.setActType("ACT");
			/* 355 */ fromAccount.setBankCode("072");
			/* 356 */ fromAccount.setContact("Jorge Silva Gallegos");
			/* 357 */ fromAccount.setEmail("jorge@mobilecardmx.com");
			/* 358 */ fromAccount.setHolderName("ADCEL SA DE CV");
			/* 359 */ fromAccount.setPhone("5555403124");
			/* 360 */ fromAccount.setRfc("ADC090715DW3");

			/* 362 */ AccountRecord toAccount = this.mapper.getAccount(paymentRequest.getAccountId());

			/* 386 */ payment.setAmount(paymentRequest.getAmount());
			/* 387 */ payment.setClientReference((new StringBuilder(String.valueOf(idBitacora))).toString());
			/* 388 */ payment.setDescription(paymentRequest.getConcept());
			/* 389 */ payment.setFromAccount(fromAccount);
			/* 390 */ payment.setToAccount(toAccount);
			/* 391 */ payment.setToken(this.login.getToken());
			/* 392 */ response = this.h2hclient.EnqueuePayment(payment, paymentRequest.getIdioma());

			/* 396 */ if (response.getResult().isSuccess()) {

				/* 398 */ LOGGER.debug("[Transaccion guardada] Exito");
				/* 399 */ this.mapper.updateTbitacora(idBitacora, 1, paymentRequest.getAuthProcom());

			} else {

				/* 404 */ LOGGER.debug("[Transaccion guardada] error al cobrar");
				/* 405 */ this.mapper.updateTbitacora(idBitacora, 2, "");
			}

			/* 417 */ responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			/* 418 */ responseTransfer.setMensajeError(
					(response.getResult().getMessage() == null) ? "" : response.getResult().getMessage());
			/* 419 */ responseTransfer.setReferenceBanorte(response.getTransactionReference());
			/* 420 */ responseTransfer.setRefProcom(paymentRequest.getRefProcom());

			/* 422 */ McResponse.setIdError(response.getResult().isSuccess() ? 0 : 99);
			/* 423 */ McResponse.setMensajeError(
					(response.getResult().getMessage() == null) ? "" : response.getResult().getMessage());

			/* 425 */ view.addObject("json", this.gson.toJson(responseTransfer));
			/* 426 */ return view;
			/* 427 */ } catch (Exception ex) {
			/* 428 */ LOGGER.error("[ERROR AL ENCOLAR PAGO] " + payment.getClientReference(), ex);
			/* 429 */ responseTransfer.setIdError(99);
			/* 430 */ responseTransfer.setMensajeError("Error al procesar Transaferencia");
			/* 431 */ view.addObject("json", this.gson.toJson(responseTransfer));
			/* 432 */ return view;
		} finally {
			/* 434 */ logout();
		}
	}

	public ModelAndView ProsaResponse(String EM_Response, String EM_Total, String EM_OrderID, String EM_Merchant,
			String EM_Store, String EM_Term, String EM_RefNum, String EM_Auth, String EM_Digest, String cc_numberback) {
		/* 441 */ ModelAndView view = new ModelAndView("transfers_response");
		/* 442 */ long id_bitacora = Long.parseLong(EM_OrderID);
		/* 443 */ TBitacoraBanorte bitacoraBanorte = this.mapper.getBitacoraBanorte(id_bitacora);
		/* 444 */ PaymentRequest paymentRequest = new PaymentRequest();
		/* 445 */ paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
		/* 446 */ paymentRequest.setAmount(bitacoraBanorte.getAmount());
		/* 447 */ paymentRequest.setConcept("Transferencia");
		/* 448 */ paymentRequest.setIdCard(bitacoraBanorte.getId_card());
		/* 449 */ paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
		/* 450 */ paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
		/* 451 */ paymentRequest.setAuthProcom(EM_Auth);
		/* 452 */ paymentRequest.setRefProcom(EM_RefNum);
		/* 453 */ if (EM_Response.equals("approved")) {

			/* 455 */ view = EnqueuePayment(paymentRequest, id_bitacora);
			/* 456 */ this.mapper.updateBitacoraBanorte(100L, id_bitacora, EM_Auth);
		} else {

			/* 460 */ McBaseResponse McResponse = new McBaseResponse();
			/* 461 */ McResponse.setIdError(90);
			/* 462 */ McResponse.setMensajeError("Error al procesar cobro 3ds");
			/* 463 */ MCTransafersResponse responseTransfer = new MCTransafersResponse();
			/* 464 */ responseTransfer.setIdError(99);
			/* 465 */ responseTransfer.setMensajeError("Error al procesar Transaferencia");
			/* 466 */ view.addObject("json", this.gson.toJson(McResponse));
		}
		/* 468 */ return view;
	}

	public ModelAndView send3ds(PaymentRequest paymentRequest) {
		/* 474 */ return comercioFin(paymentRequest);
	}

	public ModelAndView comercioFin(PaymentRequest paymentRequest) {
		/* 478 */ ModelAndView mav = new ModelAndView("comerciofin");

		try {
			/* 481 */ TBitacoraVO bitacora = new TBitacoraVO();
			/* 482 */ Card card = this.mapper.getCard(paymentRequest.getIdUser(), paymentRequest.getIdCard());
			/* 483 */ String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			/* 484 */ String vig = UtilsService.getSMS(card.getVigencia());
			/* 485 */ String ct = UtilsService.getSMS(card.getCt());
			/* 486 */ String mes = vig.split("/")[0];
			/* 487 */ String anio = "20" + vig.split("/")[1];
			/* 488 */ double comision = 0.0D;

			/* 490 */ bitacora.setTipo("");
			/* 491 */ bitacora.setSoftware("");
			/* 492 */ bitacora.setModelo("");
			/* 493 */ bitacora.setWkey("");
			/* 494 */ bitacora.setIdUsuario((new StringBuilder(String.valueOf(paymentRequest.getIdUser()))).toString());
			/* 495 */ bitacora.setIdProducto(1);
			/* 496 */ bitacora.setCar_id(1);
			/* 497 */ bitacora.setCargo(paymentRequest.getAmount() + comision);
			/* 498 */ bitacora.setIdProveedor(1);
			/* 499 */ bitacora.setConcepto("Transferencia H2HBANORTE:  Monto: " +
			/* 500 */ paymentRequest.getAmount() + " Comision: " + comision);
			/* 501 */ bitacora.setImei("");
			/* 502 */ bitacora.setDestino("Transferencia Banorte");
			/* 503 */ bitacora.setTarjeta_compra(numcard);
			/* 504 */ bitacora.setAfiliacion("8039159");
			/* 505 */ bitacora.setComision(comision);
			/* 506 */ bitacora.setCx("");
			/* 507 */ bitacora.setCy("");
			/* 508 */ bitacora.setPin("");
			/* 509 */ this.mapper.addBitacora(bitacora);

			/* 511 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			/* 512 */ bitacora_banorte.setAccount_id(paymentRequest.getAccountId());
			/* 513 */ bitacora_banorte.setAmount(paymentRequest.getAmount());
			/* 514 */ bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			/* 515 */ bitacora_banorte.setId_card(paymentRequest.getIdCard());
			/* 516 */ bitacora_banorte.setId_usuario(paymentRequest.getIdUser());
			/* 517 */ bitacora_banorte.setIdioma(paymentRequest.getIdioma());
			/* 518 */ bitacora_banorte.setTransaction_number(0L);
			/* 519 */ bitacora_banorte.setBank_reference("");

			/* 522 */ this.mapper.insertTbitacoraMCBanorte(bitacora_banorte);

			/* 524 */ LOGGER.debug("ID BITACORA: " + bitacora.getIdBitacora());
			/* 525 */ double total = comision + paymentRequest.getAmount();
			/* 526 */ String varTotal = Business
					.formatoMontoProsa((new StringBuilder(String.valueOf(total))).toString());
			/* 527 */ String digest = Business.digest(/* 528 */ "8039159" + "1234" + "001" +
			/* 529 */ varTotal + "484" + bitacora.getIdBitacora());

			/* 531 */ ProcomVO procom = new ProcomVO(varTotal, "484", "PROSA",
					(new StringBuilder(String.valueOf(bitacora.getIdBitacora()))).toString(), /* 532 */ "8039159",
					"1234", "001", digest, /* 533 */ "http://199.231.160.203:8089/MCH2HPayment/comercio-con",
					card.getNombre_tarjeta(), /* 534 */ numcard, (card.getIdfranquicia() == 1) ? "Visa" : "Mastercard",
					mes, anio, "", ct);
			/* 535 */ mav.addObject("prosa", procom);
			/* 536 */ } catch (Exception ex) {
			/* 537 */ LOGGER.error("[ERRO AL PROCESAR PAGO]", ex);
			/* 538 */ McBaseResponse McResponse = new McBaseResponse();
			/* 539 */ McResponse.setIdError(90);
			/* 540 */ McResponse.setMensajeError("Error al procesar cobro 3ds");
			/* 541 */ MCTransafersResponse transfersresponse = new MCTransafersResponse();
			/* 542 */ transfersresponse.setIdError(98);
			/* 543 */ transfersresponse.setMensajeError("Error al procesar Transferencia");

			/* 545 */ mav = new ModelAndView("transfers_response");
			/* 546 */ mav.addObject("json", this.gson.toJson(transfersresponse));
		}

		/* 549 */ return mav;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/MCH2HSERVICE.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */