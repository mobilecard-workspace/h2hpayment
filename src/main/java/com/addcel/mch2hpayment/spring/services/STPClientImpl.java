package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.MoneySendReq;
import com.addcel.mch2hpayment.client.model.MoneySendRes;
import com.addcel.mch2hpayment.client.model.STPClient;
import com.google.gson.Gson;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class STPClientImpl implements STPClient {
	/* 21 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.STPClientImpl.class);

	private static final String PATH_MONEY_SEND = "http://localhost:80/STPConsumer/api/{idApp}/{idPais}/{idioma}/enviarDinero";

	/* 25 */ private Gson GSON = new Gson();

	public MoneySendRes moneySend(MoneySendReq req, int idApp, int idPais, String idioma) {
		/* 30 */ MoneySendRes response = new MoneySendRes();
		/* 31 */ RestTemplate restTemplate = new RestTemplate();
		try {
			/* 33 */ HttpHeaders headers = new HttpHeaders();
			/* 34 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 35 */ String credentials = "mobilecardmx:82007eb4238205c75ebcdefc06a01311";
			/* 36 */ headers.add("Authorization",
					"Basic " + new String(Base64.getEncoder().encode(credentials.getBytes())));

			/* 38 */ LOGGER.debug("ENVIANDO A STP: " + this.GSON.toJson(req));
			/* 39 */ HttpEntity<MoneySendReq> request = new HttpEntity(req, (MultiValueMap) headers);
			/* 40 */ response = (MoneySendRes) restTemplate
					.postForObject("http://localhost:80/STPConsumer/api/{idApp}/{idPais}/{idioma}/enviarDinero"
							.replace("{idApp}", (new StringBuilder(String.valueOf(idApp))).toString())
							.replace("{idPais}", (new StringBuilder(String.valueOf(idPais))).toString())
							.replace("{idioma}", idioma), request, MoneySendRes.class, new Object[0]);
			/* 41 */ LOGGER.debug("respuesta STP: " + this.GSON.toJson(response));
			/* 42 */ } catch (Exception ex) {
			/* 43 */ LOGGER.debug("ERROR AL ENVIAR DINERO A TRAVES DE STP ", ex);
			/* 44 */ response.setCode(Integer.valueOf(2000));
			/* 45 */ response.setMessage("Error inesperado, intente mas tarde");
		}

		/* 48 */ return response;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/STPClientImpl.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */