package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.ServiceMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.MCTransafersResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.PayworksVO;
import com.addcel.mch2hpayment.spring.services.H2HCLIENT;
import com.addcel.mch2hpayment.spring.services.UtilsService;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Constantes;
import com.google.gson.Gson;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

@Service
public class PayworksService {
	/* 47 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.PayworksService.class);

	private String messageId;
	/* 50 */ private Gson gson = new Gson();
	/* 51 */ private SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
	/* 52 */ private H2HCLIENT h2hclient = new H2HCLIENT();

	private LoginResponse login;
	@Autowired
	ServiceMapper mapper;

	public ModelAndView send3DSBanorte(PaymentRequest request) {
		/* 59 */ this.messageId = (new StringBuilder(String.valueOf(request.getIdUser()))).toString();
		/* 60 */ ModelAndView view = new ModelAndView();
		/* 61 */ AfiliacionVO afiliacion = null;

		try {
			/* 64 */ Calendar calendario = new GregorianCalendar();
			/* 65 */ int hora = calendario.get(11);

			/* 68 */ Card card = this.mapper.getCard(request.getIdUser(), request.getIdCard());

			/* 70 */ if (request.getIdCard() != 374764L && (
			/* 71 */ hora >= 22 || (hora >= 0 && hora < 2))) {
				/* 72 */ LOGGER.info("[SERVICIO EN MANTENIMIENTO BANORTE]");
				/* 73 */ view = new ModelAndView("payworks/pagina-error");
				/* 74 */ if (request.getIdioma().equals("es")) {
					/* 75 */ view.addObject("mensajeError",
							"Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
				} else {
					/* 77 */ view.addObject("mensajeError", "Service available during the hours of 2:00 to 22:00.");
				}
				/* 79 */ return view;
			}

			/* 85 */ String rules_response = this.mapper.RulesH2HBanorte(request.getIdUser(), request.getAccountId(),
					request.getIdCard(), Double.valueOf(request.getAmount()),
					UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6), request.getIdioma());
			/* 86 */ McBaseResponse Rules = (McBaseResponse) this.gson.fromJson(rules_response, McBaseResponse.class);
			/* 87 */ if (Rules.getIdError() != 0) {
				/* 88 */ LOGGER.info("REGLAS INVALIDAS DETECTADAS " + rules_response);
				/* 89 */ view = new ModelAndView("payworks/pagina-error");
				/* 90 */ view.addObject("mensajeError", Rules.getMensajeError());
				/* 91 */ return view;
			}

			/* 98 */ User user = this.mapper.getUserData(request.getIdUser());
			/* 99 */ if (user.getId_usr_status() != 1) {
				/* 100 */ LOGGER.info("[DETECTAMOS USUARIO INACTIVO] " + this.gson.toJson(request));
				/* 101 */ view = new ModelAndView("payworks/pagina-error");
				/* 102 */ if (request.getIdioma().equals("es")) {
					/* 103 */ view.addObject("mensajeError",
							"Existen problemas su usuario Mobilecard, Para aclaraciones marque 01-800-925-5001.");
				} else {
					/* 105 */ view.addObject("mensajeError",
							"There are problems with your Mobilecard user. For clarifications call 01-800-925-5001.");
				}
				/* 107 */ return view;
			}

			/* 110 */ AccountRecord toAccount = this.mapper.getAccount(request.getAccountId());

			/* 112 */ if (toAccount.getBankCode().equals("072") || toAccount.getBankCode().equals("032")) {
				/* 114 */ if (toAccount.getActType().equals("CLABE") || toAccount.getActNumber().length() > 11) {

					/* 117 */ view = new ModelAndView("payworks/pagina-error");
					/* 118 */ if (request.getIdioma().equals("es")) {
						/* 119 */ view.addObject("mensajeError",
								/* 120 */ "Hemos detectado inconsistencias en la cuenta destino, favor de verificarla");
					} else {
						/* 122 */ view.addObject("mensajeError",
								"We have detected inconsistencies in the destination account, please verify it");
					}
					/* 124 */ return view;
				}
			}

			/* 147 */ TBitacoraVO bitacora = new TBitacoraVO();

			/* 152 */ int bin = this.mapper.isBinValido(UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6));

			/* 154 */ if (bin == 0) {
				/* 155 */ LOGGER.info("[SE DETECTO UNA TARJETA NO BIN VALIDO] " + request.getIdUser() + " ");
				/* 156 */ view = new ModelAndView("payworks/pagina-error");
				/* 157 */ if (request.getIdioma().equals("es")) {
					/* 158 */ view.addObject("mensajeError",
							"Estimado usuario, las transferencia solo aplica para tarjetas Mexicanas.");
				} else {
					/* 160 */ view.addObject("mensajeError", "Dear User, the transfer only applies to Mexican cards.");
				}
				/* 162 */ return view;
			}

			/* 166 */ if (card.getIdfranquicia() == 3) {
				/* 167 */ LOGGER
						.info("[SE DETECTO UNA TARJETA AMEX] " + request.getIdUser() + " " + request.getIdCard());
				/* 168 */ view = new ModelAndView("payworks/pagina-error");
				/* 169 */ if (request.getIdioma().equals("es")) {
					/* 170 */ view.addObject("mensajeError",
							"Estimado usuario, estamos trabajando para poder realizar transferencias con tarjetas American Express.");
				} else {
					/* 172 */ view.addObject("mensajeError",
							"Dear User, we are working to make transfers with American Express cards.");
				}
				/* 174 */ return view;
			}

			/* 177 */ if (card.getEstado() == -1) {
				/* 178 */ LOGGER.info("[SE DETECTO UNA TARJETA BLOQUEADA] " + request.getIdUser());
				/* 179 */ view = new ModelAndView("payworks/pagina-error");
				/* 180 */ if (request.getIdioma().equals("es")) {
					/* 181 */ view.addObject("mensajeError",
							"La tarjeta seleccionada se encuentra bloqueda para realizar movimientos. Para aclaraciones marque 01-800-925-5001.");
				} else {
					/* 183 */ view.addObject("mensajeError",
							"The selected card is locked to make movements. For clarifications call 01-800-925-5001.");
				}
				/* 185 */ return view;
			}

			/* 190 */ BankCodes bank = this.mapper.getBankcodesByClave(toAccount.getBankCode());
			/* 191 */ double comisionCalc = bank.getComision_fija()
					+ (Double.valueOf(request.getAmount()).doubleValue() + bank.getComision_fija())
							* bank.getComision_porcentaje();
			/* 192 */ if (request.getComision() == 0.0D) {
				/* 193 */ request.setComision(comisionCalc);
			}

			/* 199 */ Proveedor proveedor = this.mapper.getProveedor("H2HBanorte");
			/* 200 */ afiliacion = this.mapper.buscaAfiliacion("BANORTEH2H");
			/* 201 */ String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			/* 202 */ String vig = UtilsService.getSMS(card.getVigencia());
			/* 203 */ String ct = UtilsService.getSMS(card.getCt());

			/* 206 */ LOGGER.debug("Comision calculada: " + comisionCalc);
			/* 207 */ LOGGER.debug("comision request: " + request.getComision());
			/* 208 */ LOGGER.debug("amount " + request.getAmount());
			/* 209 */ String cardType = (card.getIdfranquicia() == 1) ? "VISA" : "MC";
			/* 210 */ double comision = request.getComision();
			/* 211 */ BigDecimal bd = new BigDecimal(comision);
			/* 212 */ bd = bd.setScale(2, RoundingMode.HALF_DOWN);
			/* 213 */ comision = bd.doubleValue();

			/* 216 */ bitacora.setTipo("");
			/* 217 */ bitacora.setSoftware("");
			/* 218 */ bitacora.setModelo("");
			/* 219 */ bitacora.setWkey("");
			/* 220 */ bitacora.setIdUsuario((new StringBuilder(String.valueOf(request.getIdUser()))).toString());
			/* 221 */ bitacora.setIdProducto(1);
			/* 222 */ bitacora.setCar_id(1);
			/* 223 */ bitacora.setCargo(request.getAmount() + comision);
			/* 224 */ bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			/* 225 */ bitacora.setConcepto("Transferencia H2HBANORTE: Concepto: " + request.getConcept() + " Monto: " +
			/* 226 */ request.getAmount() + " Comision: " + comision);
			/* 227 */ bitacora.setImei("");
			/* 228 */ bitacora.setDestino((new StringBuilder(String.valueOf(request.getAccountId()))).toString());
			/* 229 */ bitacora.setTarjeta_compra(card.getNumerotarjeta());
			/* 230 */ bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
			/* 231 */ bitacora.setComision(comision);
			/* 232 */ bitacora.setCx("");
			/* 233 */ bitacora.setCy("");
			/* 234 */ bitacora.setPin("");
			/* 235 */ this.mapper.addBitacora(bitacora);

			/* 237 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			/* 238 */ bitacora_banorte.setAccount_id(request.getAccountId());
			/* 239 */ bitacora_banorte.setAmount(request.getAmount());
			/* 240 */ bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			/* 241 */ bitacora_banorte.setId_card(request.getIdCard());
			/* 242 */ bitacora_banorte.setId_usuario(request.getIdUser());
			/* 243 */ bitacora_banorte.setIdioma(request.getIdioma());
			/* 244 */ bitacora_banorte.setTransaction_number(0L);
			/* 245 */ bitacora_banorte.setBank_reference("");
			/* 246 */ bitacora_banorte.setComision(comision);
			/* 247 */ bitacora_banorte.setTexto("");
			/* 248 */ bitacora_banorte.setReferencia("");
			/* 249 */ bitacora_banorte.setResultado_payw("");
			/* 250 */ bitacora_banorte.setResultado_aut("");
			/* 251 */ bitacora_banorte.setBanco_emisor("");
			/* 252 */ bitacora_banorte.setTipo_tarjeta("");
			/* 253 */ bitacora_banorte.setMarca_tarjeta("");
			/* 254 */ bitacora_banorte.setCodigo_aut("");
			/* 255 */ bitacora_banorte.setStatus3DS("");
			/* 256 */ bitacora_banorte.setConcepto(request.getConcept());
			/* 257 */ bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			/* 258 */ bitacora_banorte.setNombre_destino(toAccount.getHolderName());
			/* 259 */ bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
			/* 260 */ bitacora_banorte.setBanco_destino(toAccount.getBankCode());

			/* 262 */ this.mapper.insertTbitacoraMCBanorte(bitacora_banorte);

			/* 264 */ double total = comision + request.getAmount();

			/* 267 */ PayworksVO payVO = new PayworksVO();
			/* 268 */ payVO.setCard(numcard);
			/* 269 */ payVO.setCardType(cardType);
			/* 270 */ payVO.setExpires(vig);
			/* 271 */ payVO.setForwardPath(afiliacion.getForwardPath().split(" ")[0]);
			/* 272 */ payVO.setMerchantId(afiliacion.getMerchantId());
			/* 273 */ payVO.setReference3D((new StringBuilder(String.valueOf(bitacora.getIdBitacora()))).toString());
			/* 274 */ payVO
					.setTotal(Business.formatoMontoPayworks((new StringBuilder(String.valueOf(total))).toString()));

			/* 276 */ LOGGER.info("ENVIO DE INFO AL 3DS BANORTE[" + this.messageId + "]" + "["
					+ bitacora.getIdBitacora() + "] " + this.gson.toJson(payVO));

			/* 278 */ view.setViewName("payworks/comercio-send");
			/* 279 */ view.addObject("payworks", payVO);
		}
		/* 281 */ catch (Exception ex) {

			/* 283 */ LOGGER.error("[ERROR AL ENVIAR COBRO 3DS][" + this.messageId + "]", ex);

			/* 294 */ view = new ModelAndView("payworks/pagina-error");
			/* 295 */ if (request.getIdioma().equals("es")) {
				/* 296 */ view.addObject("mensajeError",
						/* 297 */ "Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. Por favor vuelva a intentarlo en unos minutos.");
			} else {

				/* 300 */ view.addObject("mensajeError",
						"Sorry for the inconvenience, we detected an error when authorizing your transaction. Please try again in a few minutes.");
			}
		}

		/* 304 */ return view;
	}

	public ModelAndView payworksRec3DRespuesta(String cadena, ModelMap modelo) {
		/* 309 */ HashMap<String, Object> resp = new HashMap<>();
		/* 310 */ ModelAndView mav = null;
		try {
			/* 312 */ cadena = cadena.replace(" ", "+");
			/* 313 */ LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			/* 314 */ String[] spl = cadena.split("&");
			/* 315 */ String[] data = null;
			byte b;
			int i;
			String[] arrayOfString1;
			/* 316 */ for (i = (arrayOfString1 = spl).length, b = 0; b < i;) {
				String cad = arrayOfString1[b];
				/* 317 */ LOGGER.debug("data:  " + cad);
				/* 318 */ data = cad.split("=");
				/* 319 */ resp.put(data[0], (data.length >= 2) ? data[1] : "");
				b++;
			}

			/* 322 */ if (resp.containsKey("Status") && "200".equals(resp.get("Status"))) {
				/* 323 */ AfiliacionVO afiliacion = this.mapper.buscaAfiliacion("BANORTEH2H");
				/* 324 */ LOGGER.debug("idBitacora " + resp.get("Reference3D").toString());
				/* 325 */ TBitacoraBanorte bitacoraBanorte = this.mapper
						.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
				/* 326 */ Card card = this.mapper.getCard(bitacoraBanorte.getId_usuario(),
						bitacoraBanorte.getId_card());
				/* 327 */ mav = new ModelAndView("payworks/comercioPAYW2");
				/* 328 */ mav.addObject("payworks", resp);

				/* 330 */ resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				/* 331 */ resp.put("USUARIO", afiliacion.getUsuario());
				/* 332 */ resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				/* 333 */ resp.put("CMD_TRANS", "VENTA");
				/* 334 */ resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				/* 335 */ resp.put("Total", Business.formatoMontoPayworks((String) resp.get("Total")));
				/* 336 */ resp.put("MODO", "PRD");
				/* 337 */ resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				/* 338 */ resp.put("NUMERO_TARJETA", resp.get("Number"));
				/* 339 */ resp.put("FECHA_EXP", ((String) resp.get("Expires")).replaceAll("%2F", ""));
				/* 340 */ resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt()));
				/* 341 */ resp.put("MODO_ENTRADA", "MANUAL");
				/* 342 */ resp.put("URL_RESPUESTA", afiliacion.getForwardPath().split(" ")[1]);
				/* 343 */ resp.put("IDIOMA_RESPUESTA", "ES");
				/* 344 */ resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt()));

				/* 347 */ if (resp.get("XID") != null) {
					/* 348 */ resp.put("XID", ((String) resp.get("XID")).replaceAll("%3D", "="));
					/* 349 */ resp.put("CAVV", ((String) resp.get("CAVV")).replaceAll("%3D", "="));
				}

				/* 352 */ LOGGER.info("DATOS ENVIADO A PAYWORK2: " + resp.toString());
			} else {

				/* 355 */ LOGGER.debug("[PAGO RECHAZADO]");

				/* 357 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				/* 358 */ bitacora_banorte.setId_bitacora(Long.parseLong((String) resp.get("Reference3D")));
				/* 359 */ bitacora_banorte.setTexto(
						(resp.get("Status") != null) ? (String) Constantes.errorPayW.get(resp.get("Status")) : "");
				/* 360 */ bitacora_banorte.setReferencia("");
				/* 361 */ bitacora_banorte.setResultado_payw("");
				/* 362 */ bitacora_banorte.setResultado_aut("");
				/* 363 */ bitacora_banorte.setBanco_emisor("");
				/* 364 */ bitacora_banorte.setTipo_tarjeta("");
				/* 365 */ bitacora_banorte.setMarca_tarjeta((String) resp.get("CardType"));
				/* 366 */ bitacora_banorte.setCodigo_aut("");
				/* 367 */ bitacora_banorte.setBank_reference("");
				/* 368 */ bitacora_banorte.setTransaction_number(0L);
				/* 369 */ bitacora_banorte
						.setStatus3DS((resp.get("Status") != null) ? (String) resp.get("Status") : "");

				/* 371 */ this.mapper.updateBitacoraBanorteOBJ(bitacora_banorte);

				/* 373 */ mav = new ModelAndView("payworks/pagina-error");
				/* 374 */ mav.addObject("mensajeError", "El pago fue rechazado. " + (
				/* 375 */ (resp.get("Status") != null) ? ("Clave: " + resp.get("Status")) : "") + ", Descripcion: "
						+ (String) Constantes.errorPayW.get(resp.get("Status")));
			}

		}
		/* 379 */ catch (Exception ex) {
			/* 380 */ LOGGER.error("ERROR AL PROCESAR RESPUESTA 3DS BANORTE " + cadena, ex);
			/* 381 */ LOGGER.error("ERROR ", ex);
		}

		/* 384 */ return mav;
	}

	public ModelAndView payworks2RecRespuesta(String NUMERO_CONTROL, String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW, String FECHA_REQ_CTE, String CODIGO_AUT, String CODIGO_PAYW,
			String RESULTADO_AUT, String BANCO_EMISOR, String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		/* 390 */ ModelAndView mav = new ModelAndView("payworks/exito");

		try {
			/* 393 */ LOGGER
					.debug("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL + " REFERENCIA: " + REFERENCIA +
					/* 394 */ " FECHA_RSP_CTE: " + FECHA_RSP_CTE + " TEXTO: " + TEXTO + " RESULTADO_AUT: "
							+ RESULTADO_AUT +
							/* 395 */ " RESULTADO_PAYW: " + RESULTADO_PAYW + " FECHA_REQ_CTE: " + FECHA_REQ_CTE
							+ " CODIGO_PAYW: " + CODIGO_PAYW +
							/* 396 */ " CODIGO_PAYW: " + CODIGO_AUT + " BANCO_EMISOR: " + BANCO_EMISOR
							+ " TIPO_TARJETA: " + TIPO_TARJETA +
							/* 397 */ " MARCA_TARJETA: " + MARCA_TARJETA);
			/* 398 */ long id_bitacora = Long.parseLong(NUMERO_CONTROL);
			/* 399 */ TBitacoraBanorte bitacoraBanorte = this.mapper.getBitacoraBanorte(id_bitacora);
			/* 400 */ User user = this.mapper.getUserData(bitacoraBanorte.getId_usuario());

			/* 402 */ if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				/* 404 */ PaymentRequest paymentRequest = new PaymentRequest();
				/* 405 */ paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
				/* 406 */ paymentRequest.setAmount(bitacoraBanorte.getAmount());
				/* 407 */ paymentRequest.setConcept("Transferencia");
				/* 408 */ paymentRequest.setIdCard(bitacoraBanorte.getId_card());
				/* 409 */ paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
				/* 410 */ paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
				/* 411 */ paymentRequest.setAuthProcom(CODIGO_AUT);
				/* 412 */ paymentRequest.setRefProcom(REFERENCIA);
				/* 413 */ mav = EnqueuePayment(paymentRequest, id_bitacora);

				/* 416 */ MCTransafersResponse responseTransfer = (MCTransafersResponse) this.gson
						.fromJson((String) mav.getModel().get("json"), MCTransafersResponse.class);

				/* 418 */ if (responseTransfer.getIdError() == 0) {

					/* 420 */ mav.addObject("mensajeError",
							"<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/><tr><td style='text-align:right; width:60%;'>Fecha:</td><td style='width:40%;'> "
									+

									/* 422 */ this.DFormat.format(new Date()) + "</td></tr>" +
									/* 423 */ "<tr><td style='text-align:right; width:60%;'>Referencia:</td><td style='width:40%;'> "
									+ REFERENCIA + "</td></tr>" +

									/* 425 */ "<tr><td style='text-align:right; width:60%;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='width:40%;'> "
									+ CODIGO_AUT + "</td></tr>" +
									/* 426 */ "<tr><td style='text-align:right; width:60%;'>Folio MC:</td><td style='width:40%;'> "
									+ NUMERO_CONTROL + "</td></tr>" +
									/* 427 */ "<tr><td style='text-align:right; width:60%;'>Importe Transferido:</td><td style='width:40%;'> $"
									+ bitacoraBanorte.getAmount() + "</td></tr>" +
									/* 428 */ "<tr><td style='text-align:right; width:60%;'>Comision:</td><td style='width:40%;'> $"
									+ bitacoraBanorte.getComision() + "</td></tr>" +

									/* 430 */ "<tr><td colspan='2'><BR>Servicio operado por Plataforma MC."
									+ "</td></tr>" +

									/* 432 */ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001."
									+ "</td></tr>");

					/* 436 */ HashMap<String, String> datos = new HashMap<>();
					/* 437 */ datos.put("NOMBRE", user.getUsr_nombre());
					/* 438 */ datos.put("FECHA", this.DFormat.format(new Date()));
					/* 439 */ datos.put("AUTBAN", CODIGO_AUT);
					/* 440 */ datos.put("IMPORTE",
							(new StringBuilder(String.valueOf(bitacoraBanorte.getAmount()))).toString());
					/* 441 */ datos.put("COMISION",
							(new StringBuilder(String.valueOf(bitacoraBanorte.getComision()))).toString());

					/* 443 */ ProjectMC project = this.mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");

					/* 445 */ Business.SendMailMicrosoft(user.geteMail(),
							this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_ES"),
							this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
				} else {

					/* 448 */ String msg = "Ocurrio un problema al realizar transferencia<br><br>"
							+ responseTransfer.getMensajeError() +
							/* 449 */ "<br>Para aclaraciones marque 01-800-925-5001.";
					/* 450 */ mav = new ModelAndView("payworks/pagina-error");
					/* 451 */ mav.addObject("mensajeError", msg);
				}

				/* 455 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				/* 456 */ bitacora_banorte.setId_bitacora(id_bitacora);
				/* 457 */ bitacora_banorte.setTexto(TEXTO);
				/* 458 */ bitacora_banorte.setReferencia(REFERENCIA);
				/* 459 */ bitacora_banorte.setResultado_payw(RESULTADO_PAYW);
				/* 460 */ bitacora_banorte.setResultado_aut(RESULTADO_AUT);
				/* 461 */ bitacora_banorte.setBanco_emisor(BANCO_EMISOR);
				/* 462 */ bitacora_banorte.setTipo_tarjeta(TIPO_TARJETA);
				/* 463 */ bitacora_banorte.setMarca_tarjeta(MARCA_TARJETA);
				/* 464 */ bitacora_banorte.setCodigo_aut(CODIGO_AUT);
				/* 465 */ bitacora_banorte.setBank_reference(responseTransfer.getReferenceBanorte());
				/* 466 */ bitacora_banorte
						.setTransaction_number(Long.parseLong(responseTransfer.getReferenceBanorte()));
				/* 467 */ bitacora_banorte.setStatus3DS("200");

				/* 469 */ this.mapper.updateBitacoraBanorteOBJ(bitacora_banorte);

			} else {
				/* 474 */ String msg =

						/* 476 */ String
								.valueOf("D".equalsIgnoreCase(RESULTADO_PAYW) ? "DECLINADA"
										: ("R".equalsIgnoreCase(RESULTADO_PAYW) ? "RECHAZADA"
												: ("T".equalsIgnoreCase(RESULTADO_PAYW)
														? "Sin respuesta del autorizador"
														: "Repuesta no Valida")))
								+
								/* 477 */ ": " + TEXTO;
				/* 478 */ LOGGER.debug("[Mensaje Banorte] " + msg);

				/* 490 */ msg = "El pago fue " + (
				/* 491 */ "D".equalsIgnoreCase(RESULTADO_PAYW) ? "DECLINADO" : (
				/* 492 */ "R".equalsIgnoreCase(RESULTADO_PAYW) ? "RECHAZADO" : (
				/* 493 */ "T".equalsIgnoreCase(RESULTADO_PAYW) ? "Sin respuesta del autorizador"
						: "Repuesta no Valida"))) + BANCO_EMISOR + ", " + TIPO_TARJETA +
				/* 494 */ " . " + ((CODIGO_PAYW != null) ? ("Clave: " + CODIGO_PAYW)
						: ((RESULTADO_AUT != null) ? RESULTADO_AUT : "")) + ", Descripcion: " + TEXTO;
				/* 495 */ mav = new ModelAndView("payworks/pagina-error");
				/* 496 */ mav.addObject("mensajeError", msg);

			}

		}
		/* 501 */ catch (Exception ex) {
			/* 502 */ LOGGER.error("[ERROR AL PROCESAR RESPUESTA BANORTE payworks2RecRespuesta] " + NUMERO_CONTROL, ex);
			/* 503 */ mav = new ModelAndView("payworks/pagina-error");
			/* 504 */ mav.addObject("mensajeError",
					/* 505 */ "Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. Por favor vuelva a intentarlo en unos minutos.");
		}

		/* 508 */ return mav;
	}

	public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora) {
		/* 516 */ EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
		/* 517 */ EnqueueTransactionResponse response = new EnqueueTransactionResponse();
		/* 518 */ MCTransafersResponse responseTransfer = new MCTransafersResponse();
		/* 519 */ ModelAndView view = new ModelAndView("payworks/exito");
		/* 520 */ McBaseResponse McResponse = new McBaseResponse();
		try {
			/* 522 */ this.login = login();
			/* 523 */ TBitacoraVO bitacora = new TBitacoraVO();

			/* 526 */ responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			/* 527 */ responseTransfer.setComision(1.0D);
			/* 528 */ responseTransfer.setFecha(this.DFormat.format(new Date()));
			/* 529 */ responseTransfer.setMontoTransfer(paymentRequest.getAmount());

			/* 533 */ User user = this.mapper.getUserData(paymentRequest.getIdUser());
			/* 534 */ Card card = this.mapper.getCard(paymentRequest.getIdUser(), paymentRequest.getIdCard());
			/* 535 */ AccountRecord fromAccount = new AccountRecord();

			/* 537 */ responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));

			/* 539 */ fromAccount.setActNumber("0276948693");
			/* 540 */ fromAccount.setActType("ACT");
			/* 541 */ fromAccount.setBankCode("072");
			/* 542 */ fromAccount.setContact("Jorge Silva Gallegos");
			/* 543 */ fromAccount.setEmail("jorge@mobilecardmx.com");
			/* 544 */ fromAccount.setHolderName("ADCEL SA DE CV");
			/* 545 */ fromAccount.setPhone("5555403124");
			/* 546 */ fromAccount.setRfc("ADC090715DW3");

			/* 548 */ AccountRecord toAccount = this.mapper.getAccount(paymentRequest.getAccountId());

			/* 555 */ payment.setAmount(paymentRequest.getAmount());
			/* 556 */ payment.setClientReference((new StringBuilder(String.valueOf(idBitacora))).toString());
			/* 557 */ payment.setDescription(paymentRequest.getConcept());
			/* 558 */ payment.setFromAccount(fromAccount);
			/* 559 */ payment.setToAccount(toAccount);
			/* 560 */ payment.setToken(this.login.getToken());
			/* 561 */ response = this.h2hclient.EnqueuePayment(payment, paymentRequest.getIdioma());

			/* 565 */ if (response.getResult().isSuccess()) {

				/* 567 */ LOGGER.debug("[Transaccion guardada] Exito");
				/* 568 */ this.mapper.updateTbitacora(idBitacora, 1, paymentRequest.getAuthProcom());
				/* 569 */ HashMap<String, String> datos = new HashMap<>();
				/* 570 */ datos.put("NOMBRE", toAccount.getContact());
				/* 571 */ datos.put("FECHA", this.DFormat.format(new Date()));

				/* 573 */ datos.put("IMPORTE", (new StringBuilder(String.valueOf(payment.getAmount()))).toString());
				/* 574 */ datos.put("CUENTA", toAccount.getActNumber());

				/* 576 */ ProjectMC project = this.mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");

				/* 578 */ Business.SendMailMicrosoft(user.geteMail(),
						this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_DEST_ES"),
						this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());

			} else {

				/* 585 */ LOGGER.debug("[Transaccion guardada] error al cobrar");
				/* 586 */ this.mapper.updateTbitacora(idBitacora, 2, "");
			}

			/* 598 */ responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			/* 599 */ responseTransfer.setMensajeError(
					(response.getResult().getMessage() == null) ? "" : response.getResult().getMessage());
			/* 600 */ responseTransfer.setReferenceBanorte(response.getTransactionReference());
			/* 601 */ responseTransfer.setRefProcom(paymentRequest.getRefProcom());

			/* 603 */ McResponse.setIdError(response.getResult().isSuccess() ? 0 : 99);
			/* 604 */ McResponse.setMensajeError(
					(response.getResult().getMessage() == null) ? "" : response.getResult().getMessage());

			/* 606 */ view.addObject("json", this.gson.toJson(responseTransfer));
			/* 607 */ return view;
			/* 608 */ } catch (Exception ex) {

			/* 610 */ LOGGER.error("[ERROR AL ENCOLAR PAGO] ", ex);
			/* 611 */ responseTransfer.setIdError(99);
			/* 612 */ responseTransfer.setMensajeError("Error al procesar Transaferencia");
			/* 613 */ view.addObject("json", this.gson.toJson(responseTransfer));
			/* 614 */ return view;
		} finally {
			/* 616 */ logout();
		}
	}

	public LoginResponse login() {
		try {
			/* 622 */ LoginRequest login = new LoginRequest();
			/* 623 */ login.setLogin("addcel_banorte");
			/* 624 */ login.setPassword("B4N0rt3%");
			/* 625 */ LOGGER.debug("iniciando login");
			/* 626 */ LoginResponse response = this.h2hclient.login(login, "es");
			/* 627 */ LOGGER.debug("LOGUIN: " + response.getToken() + " " + response.getResult().isSuccess() + " "
					+ response.getResult().getMessage());
			/* 628 */ return response;
			/* 629 */ } catch (Exception ex) {
			/* 630 */ LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			/* 631 */ return new LoginResponse();
		}
	}

	public LogoutResponse logout() {
		try {
			/* 637 */ LogoutRequest logout = new LogoutRequest();
			/* 638 */ logout.setLogin("addcel_banorte");
			/* 639 */ logout.setToken(this.login.getToken());
			/* 640 */ LogoutResponse response = this.h2hclient.logout(logout, "es");
			/* 641 */ LOGGER.debug("[RESPUESTA DE LOGOUT]" + response.getResult().isSuccess());
			/* 642 */ return response;
			/* 643 */ } catch (Exception ex) {
			/* 644 */ LOGGER.error("[ERROR EN LOGOUT]", ex);
			/* 645 */ return new LogoutResponse();
		}
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/PayworksService.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */