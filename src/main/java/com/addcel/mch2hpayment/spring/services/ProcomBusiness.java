package com.addcel.mch2hpayment.spring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcomBusiness {
	/* 17 */ private static final Logger logger = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.ProcomBusiness.class);

	private String formatoMontoProsa(String monto) {
		/* 42 */ String varTotal = "000";
		/* 43 */ String pesos = null;
		/* 44 */ String centavos = null;
		/* 45 */ if (monto.contains(".")) {
			/* 46 */ pesos = monto.substring(0, monto.indexOf("."));
			/* 47 */ centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			/* 48 */ if (centavos.length() < 2) {
				/* 49 */ centavos = centavos.concat("0");
			} else {
				/* 51 */ centavos = centavos.substring(0, 2);
			}
			/* 53 */ varTotal = String.valueOf(pesos) + centavos;
		} else {
			/* 55 */ varTotal = monto.concat("00");
		}
		/* 57 */ logger.info("Monto a cobrar 3dSecure: " + varTotal);
		/* 58 */ return varTotal;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/ProcomBusiness.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */