package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AccountMC;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryRequest;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.H2HMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.Bank;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.spring.services.H2HCLIENT;
import com.addcel.mch2hpayment.utils.Business;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class H2HService {
	/* 38 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.H2HService.class);

	/* 40 */ private H2HCLIENT h2hclient = new H2HCLIENT();
	private LoginResponse login;
	/* 42 */ private Gson gson = new Gson();

	@Autowired
	private H2HMapper mapper;

	public MCSignUpResponse EnqueueAccountSignUp(MCAccount mcAccount, int idApp) {
		/* 48 */ MCSignUpResponse response = new MCSignUpResponse();

		try {
			/* 51 */ LOGGER.debug("RESGISTRANDO CUENTA: " + this.gson.toJson(mcAccount) + "   : " + idApp);
			/* 52 */ String activo = this.mapper.getParameter("@ACTIVETRANSFERS");

			/* 54 */ if (Long.parseLong(activo) == 0L && mcAccount.getIdUsuario() != 1503601741480L) {

				/* 56 */ response.setAccounts(new ArrayList());
				/* 57 */ response.setIdError(400);
				/* 58 */ response.setMensajeError("Servicio no disponible.");

				/* 61 */ return response;
			}

			/* 64 */ AccountMC acc = new AccountMC();
			/* 65 */ acc.setAct_number(mcAccount.getActNumber().trim());
			/* 66 */ acc.setAct_type(mcAccount.getActType().trim());
			/* 67 */ acc.setBank_code(mcAccount.getBankCode().trim());
			/* 68 */ acc.setBanorte_clave_id("");
			/* 69 */ acc.setContact(
					(mcAccount.getContact() == null) ? "" : Business.removeAcentos(mcAccount.getContact()).trim());
			/* 70 */ acc.setEmail(mcAccount.getEmail().trim());
			/* 71 */ acc.setHolder_name(Business.removeAcentos(mcAccount.getHolderName()).trim());
			/* 72 */ acc.setPhone((mcAccount.getPhone() == null) ? "" : mcAccount.getPhone().trim());
			/* 73 */ acc.setRfc(mcAccount.getRfc().trim());
			/* 74 */ acc.setStatus(1);
			/* 75 */ this.mapper.insertAccount(acc);
			/* 76 */ String responsBD = this.mapper.H2HBanorte_Business(mcAccount.getIdUsuario(), 0L, acc.getId(),
					mcAccount.getAlias(), 2, 0L, "", "", mcAccount.getIdioma(), idApp);
			/* 77 */ response = (MCSignUpResponse) this.gson.fromJson(responsBD, MCSignUpResponse.class);
			/* 78 */ LOGGER.debug("[RESPONSE BD] " + responsBD);
			/* 79 */ return response;
			/* 80 */ } catch (Exception ex) {
			/* 81 */ LOGGER.error("[ERROR AL REGISTAR CUENTA USUARIO]", ex);
			/* 82 */ response.setIdError(400);
			/* 83 */ if (mcAccount.getIdioma().equals("es")) {
				/* 84 */ response.setMensajeError("Error al procesar datos, contacte a soporte.");
			} else {
				/* 86 */ response.setMensajeError("Error processing data, contact support.");
				/* 87 */ }
			return response;
		}
	}

	public MCSignUpResponse getToAccount(long idUsuario, String idioma, int idApp) {
		/* 224 */ MCSignUpResponse response = new MCSignUpResponse();
		try {
			/* 226 */ String activo = this.mapper.getParameter("@ACTIVETRANSFERS");

			/* 228 */ String responseBD = this.mapper.H2HBanorte_Business(idUsuario, 0L, 0L, "", 3, 0L, "", "", idioma,
					idApp);

			/* 230 */ if (Long.parseLong(activo) == 1L || Long.parseLong(activo) == 1503601741480L) {
				/* 231 */ response = (MCSignUpResponse) this.gson.fromJson(responseBD, MCSignUpResponse.class);
			} else {
				/* 233 */ response.setAccounts(new ArrayList());
				/* 234 */ response.setIdError(400);
				/* 235 */ response.setMensajeError("Servicio no disponible");
			}
			/* 237 */ LOGGER.debug("RETORNANDO CUENTAS: " + this.gson.toJson(response));
			/* 238 */ } catch (Exception ex) {
			/* 239 */ LOGGER.error("[ERROR AL OBTENER CUANTAS ASOCIADAS ] [" + idUsuario + "]", ex);
		}

		/* 243 */ return response;
	}

	public LoginResponse login() {
		try {
			/* 248 */ LoginRequest login = new LoginRequest();
			/* 249 */ login.setLogin("addcel_banorte");
			/* 250 */ login.setPassword("B4N0rt3%");
			/* 251 */ LOGGER.debug("iniciando login");
			/* 252 */ LoginResponse response = this.h2hclient.login(login, "es");
			/* 253 */ LOGGER.debug("LOGUIN: " + response.getToken() + " " + response.getResult().isSuccess() + " "
					+ response.getResult().getMessage());
			/* 254 */ return response;
			/* 255 */ } catch (Exception ex) {
			/* 256 */ LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			/* 257 */ return new LoginResponse();
		}
	}

	public LogoutResponse logout() {
		try {
			/* 263 */ LogoutRequest logout = new LogoutRequest();
			/* 264 */ logout.setLogin("addcel_banorte");
			/* 265 */ logout.setToken(this.login.getToken());
			/* 266 */ LogoutResponse response = this.h2hclient.logout(logout, "es");
			/* 267 */ LOGGER.debug("[RESPUESTA DE LOGOUT]" + response.getResult().isSuccess());
			/* 268 */ return response;
			/* 269 */ } catch (Exception ex) {
			/* 270 */ LOGGER.error("[ERROR EN LOGOUT]", ex);
			/* 271 */ return new LogoutResponse();
		}
	}

	public MCSignUpResponse updateAccount(UpdateAccountRequest request, int idApp) {
		/* 282 */ MCSignUpResponse response = new MCSignUpResponse();
		try {
			/* 284 */ LOGGER.debug("[INICIANDO ACTUALIZACION CUENTA ]" + this.gson.toJson(request));
			/* 285 */ String responseDB = this.mapper.H2HBanorte_Business(request.getIdUsuario(), 0L, 0L,
					request.getAlias().trim(), 4, request.getIdAccount(), request.getTelefono().trim(),
					request.getEmail().trim(), request.getIdioma().trim(), idApp);

			/* 287 */ response = (MCSignUpResponse) this.gson.fromJson(responseDB, MCSignUpResponse.class);
			/* 288 */ } catch (Exception ex) {
			/* 289 */ response.setIdError(99);
			/* 290 */ if (request.getIdioma().equalsIgnoreCase("es")) {
				/* 291 */ response.setMensajeError("Error inesperado, vuelva a intentarlo");
			} else {
				/* 293 */ response.setMensajeError("Unexpected error, try again");
				/* 294 */ }
			LOGGER.error("[ERROR AL ACTUALIZAR CUENTA DESTINO ] " + this.gson.toJson(request), ex);
		}
		/* 296 */ LOGGER.debug("RETORNANDO RESPUESTA : " + this.gson.toJson(response));
		/* 297 */ return response;
	}

	public MCSignUpResponse deleteAccount(long idUsuario, long idAccount, String idioma, int idApp) {
		/* 302 */ MCSignUpResponse response = new MCSignUpResponse();
		try {
			/* 304 */ LOGGER.debug("[INICIANDO ELIMINACION DE CUENTA ]" + idUsuario);
			/* 305 */ String responseDB = this.mapper.H2HBanorte_Business(idUsuario, 0L, 0L, "", 5, idAccount, "", "",
					idioma, idApp);

			/* 307 */ response = (MCSignUpResponse) this.gson.fromJson(responseDB, MCSignUpResponse.class);
			/* 308 */ } catch (Exception ex) {
			/* 309 */ if (idioma.equalsIgnoreCase("es")) {
				/* 310 */ response.setMensajeError("Error inesperado, vuelva a intentarlo");
			} else {
				/* 312 */ response.setMensajeError("Unexpected error, try again");
				/* 313 */ }
			LOGGER.error("[ERROR AL ELIMINAR CUENTA DESTINO ] " + idUsuario + "-" + idAccount, ex);
		}
		/* 315 */ LOGGER.debug("RETORNANDO RESPUESTA: " + this.gson.toJson(response));
		/* 316 */ return response;
	}

	public TransactionEnquiryResponse TransactionEnquiry(String transactionReference, String idioma, int idApp) {
		try {
			/* 324 */ this.login = login();
			/* 325 */ TransactionEnquiryRequest request = new TransactionEnquiryRequest();
			/* 326 */ request.setToken(this.login.getToken());
			/* 327 */ request.setTransactionReference(transactionReference);
			/* 328 */ LOGGER.debug("[CONSULTANDO ESTATUS DE TRANSACCION] " + request.getTransactionReference());
			/* 329 */ TransactionEnquiryResponse response = this.h2hclient.TransactionEnquiry(request, idioma);
			/* 330 */ LOGGER.debug("[FINALIZANDO CONSULTA DE ESTATUS]" + request.getTransactionReference());
			/* 331 */ return response;
			/* 332 */ } catch (Exception ex) {
			/* 333 */ LOGGER.error("[ERROR AL CONSULTAR ESTATUS DE TRANSACCION] " + transactionReference, ex);
			/* 334 */ return null;
		} finally {
			/* 336 */ logout();
		}
	}

	public BankCodesResponse getBankCodes(int idApp) {
		/* 341 */ BankCodesResponse response = new BankCodesResponse();
		try {
			/* 343 */ LOGGER.debug("[INICIANDO OBTENCION DE CATALOGO DE BANCOS]");
			/* 344 */ List<Bank> catalogo = this.mapper.getCatalogoBancos();
			/* 345 */ List<BankCodes> bankCa = new ArrayList<>();

			/* 347 */ for (Bank bank : catalogo) {

				/* 349 */ BankCodes b = new BankCodes();
//				/* 350 */ b.setClave((String) bank.getClave());
					String clave = String.valueOf(bank.getClave());
				/* 350 */ b.setClave(clave);
				/* 351 */ b.setComision_fija(0.0D);
				/* 352 */ b.setComision_porcentaje(0.0D);
				/* 353 */ b.setId(bank.getId().intValue());
				/* 354 */ b.setNombre_corto(bank.getDescripcion());
				/* 355 */ b.setNombre_razon_social(bank.getDescripcion());
				/* 356 */ bankCa.add(b);
			}

			/* 359 */ response.setBanks(bankCa);
			/* 360 */ response.setIdError(0);
			/* 361 */ response.setMensajeError("");
			/* 362 */ LOGGER.debug("LISTA DE BANCOS: " + this.gson.toJson(response));
			/* 363 */ } catch (Exception ex) {
			/* 364 */ LOGGER.error("[ERROR AL OBTENER BNANCOS]", ex);
			/* 365 */ response.setIdError(99);
			/* 366 */ response.setMensajeError("Error inesperado, vuelva a intentarlo");
		}
		/* 368 */ return response;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/H2HService.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */