package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.utils.MailSender;
import java.security.KeyStore;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class EmailSenderClient {
	/* 29 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.EmailSenderClient.class);

	/* 31 */ public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER = (X509HostnameVerifier) new AllowAllHostnameVerifier();

	/* 35 */ private RestTemplate client = getRestemplate();

	public boolean SendMail(MailSender email, String url) {
		/* 41 */ boolean send = false;

		try {
			/* 44 */ HttpHeaders headers = getHeaders();
			/* 45 */ HttpEntity<MailSender> request = new HttpEntity(email, (MultiValueMap) headers);
			/* 46 */ String response = (String) this.client.postForObject(url, request, String.class, new Object[0]);
			/* 47 */ LOGGER.debug("RESPUESTA MAILSENDER: " + response);
			/* 48 */ send = true;
			/* 49 */ } catch (Exception ex) {
			/* 50 */ LOGGER.error("ERROR AL ENVIAR EMAIL", ex);
		}

		/* 53 */ return send;
	}

	private HttpHeaders getHeaders() {
		try {
			/* 59 */ HttpHeaders headers = new HttpHeaders();
			/* 60 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 61 */ return headers;
			/* 62 */ } catch (Exception ex) {
			/* 63 */ LOGGER.error("ERROR AL CREAR HEADER", ex);
			/* 64 */ return null;
		}
	}

	private RestTemplate getRestemplate() {
		try {
			/* 72 */ KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			/* 73 */ keyStore.load(getClass().getResourceAsStream("KeyStore.jks"), "addcel".toCharArray());
			/* 74 */ LOGGER.debug("Key store cargado " + keyStore.size());
			/* 75 */ SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory((
			/* 76 */ new SSLContextBuilder())
					/* 77 */ .loadTrustMaterial(null, (TrustStrategy) new TrustSelfSignedStrategy())
					/* 78 */ .loadKeyMaterial(keyStore, "addcel".toCharArray())/* 79 */ .build(),
					/* 80 */ ALLOW_ALL_HOSTNAME_VERIFIER);

			/* 82 */ CloseableHttpClient httpClient = HttpClients.custom()
					.setSSLSocketFactory((LayeredConnectionSocketFactory) socketFactory).build();

			/* 84 */ HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
					(HttpClient) httpClient);
			/* 85 */ RestTemplate restTemplate = new RestTemplate(
					(ClientHttpRequestFactory) httpComponentsClientHttpRequestFactory);
			/* 86 */ return restTemplate;
			/* 87 */ } catch (Exception ex) {
			/* 88 */ LOGGER.error("Error al cargar store ", ex);
			/* 89 */ return null;
		}
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/EmailSenderClient.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */