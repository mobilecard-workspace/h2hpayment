package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.EnqueueAccountSignUpRequest;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryRequest;
import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class H2HCLIENT {
	/* 30 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.H2HCLIENT.class);

	private static final String SERVER = "http://localhost:80/";

	private static final String URL_H2H_LOGIN = "http://localhost:80/H2HPaymentService/v1/login?lan=@es";

	private static final String URL_H2H_LOGOUT = "http://localhost:80/H2HPaymentService/v1/logout?lan=@es";

	private static final String URL_H2H_ENQUEUE_PAYMENT = "http://localhost:80/H2HPaymentService/v1/enqueuePayment?lan=@es";
	private static final String URL_H2H_ENQUEUE_SING_UP = "http://localhost:80/H2HPaymentService/v1/enqueueAccountSignUp?lan=@es";
	private static final String URL_H2H_TRANSACTION_ENQUIRY = "http://localhost:80/H2HPaymentService/v1/transactionEnquiry?lan=@es";
	/* 41 */ private Gson gson = new Gson();
	/* 42 */ RestTemplate restTemplate = new RestTemplate();

	public LoginResponse login(LoginRequest login, String idioma) {
		try {
			/* 47 */ HttpHeaders headers = new HttpHeaders();

			/* 49 */ HttpEntity<LoginRequest> request = new HttpEntity(login, (MultiValueMap) headers);

			/* 51 */ LoginResponse responseObject = (LoginResponse) this.restTemplate.postForObject(
					"http://localhost:80/H2HPaymentService/v1/login?lan=@es".replace("@es", idioma), request,
					LoginResponse.class, new Object[0]);
			/* 52 */ return responseObject;
		}
		/* 54 */ catch (Exception ex) {
			/* 55 */ LOGGER.error("[ERROR AL LOGUEARSE ] [" + login.getLogin() + "]", ex);
			/* 56 */ return new LoginResponse();
		}
	}

	public LogoutResponse logout(LogoutRequest logout, String idioma) {
		try {
			/* 63 */ HttpHeaders headers = new HttpHeaders();

			/* 65 */ HttpEntity<LogoutRequest> request = new HttpEntity(logout, (MultiValueMap) headers);

			/* 67 */ LogoutResponse responseObject = (LogoutResponse) this.restTemplate.postForObject(
					"http://localhost:80/H2HPaymentService/v1/logout?lan=@es".replace("@es", idioma), request,
					LogoutResponse.class, new Object[0]);
			/* 68 */ return responseObject;
		}
		/* 70 */ catch (Exception ex) {
			/* 71 */ LOGGER.error("[ERROR LOGOUT ] [" + logout.getLogin() + "]", ex);
			/* 72 */ return new LogoutResponse();
		}
	}

	public EnqueueTransactionResponse EnqueuePayment(EnqueuePaymentRequest payment, String idioma) {
		/* 77 */ EnqueueTransactionResponse responseObject = new EnqueueTransactionResponse();

		try {
			/* 80 */ HttpHeaders headers = new HttpHeaders();

			/* 82 */ HttpEntity<EnqueuePaymentRequest> request = new HttpEntity(payment, (MultiValueMap) headers);
			/* 83 */ LOGGER.debug("JSON REQUEST: " + this.gson.toJson(payment));
			/* 84 */ responseObject = (EnqueueTransactionResponse) this.restTemplate.postForObject(
					"http://localhost:80/H2HPaymentService/v1/enqueuePayment?lan=@es".replace("@es", idioma), request,
					EnqueueTransactionResponse.class, new Object[0]);
			/* 85 */ return responseObject;
		}
		/* 87 */ catch (HttpClientErrorException client) {
			/* 88 */ LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			/* 89 */ if (client.getRawStatusCode() == 400 || client.getRawStatusCode() == 401)
				/* 90 */ responseObject = (EnqueueTransactionResponse) this.gson
						.fromJson(client.getResponseBodyAsString(), EnqueueTransactionResponse.class);
			/* 91 */ return responseObject;
			/* 92 */ } catch (Exception ex) {
			/* 93 */ LOGGER.error("[ERROR AL ENCOLAR PAGO] [" + payment.getClientReference() + "]", ex);
			/* 94 */ return new EnqueueTransactionResponse();
		}
	}

	public EnqueueTransactionResponse EnqueueAccountSignUp(EnqueueAccountSignUpRequest AccountSignUp, String idioma) {
		/* 102 */ EnqueueTransactionResponse responseObject = new EnqueueTransactionResponse();

		try {
			/* 105 */ HttpHeaders headers = new HttpHeaders();

			/* 109 */ HttpEntity<EnqueueAccountSignUpRequest> request = new HttpEntity(AccountSignUp,
					(MultiValueMap) headers);
			/* 110 */ LOGGER.debug("JSON REQUEST: " + this.gson.toJson(AccountSignUp));
			/* 111 */ responseObject = (EnqueueTransactionResponse) this.restTemplate.postForObject(
					"http://localhost:80/H2HPaymentService/v1/enqueueAccountSignUp?lan=@es".replace("@es", idioma),
					request, EnqueueTransactionResponse.class, new Object[0]);
			/* 112 */ return responseObject;
		}
		/* 114 */ catch (HttpClientErrorException client) {
			/* 115 */ LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			/* 116 */ if (client.getRawStatusCode() == 400 || client.getRawStatusCode() == 401)
				/* 117 */ responseObject = (EnqueueTransactionResponse) this.gson
						.fromJson(client.getResponseBodyAsString(), EnqueueTransactionResponse.class);
			/* 118 */ return responseObject;
			/* 119 */ } catch (Exception ex) {
			/* 120 */ LOGGER.error("[ERROR EN ALTA DE REGISTRO] [" + AccountSignUp.getClientReference() + "]", ex);
			/* 121 */ return new EnqueueTransactionResponse();
		}
	}

	public TransactionEnquiryResponse TransactionEnquiry(TransactionEnquiryRequest TransactionEnquiry, String idioma) {
		/* 129 */ TransactionEnquiryResponse responseObject = new TransactionEnquiryResponse();

		try {
			/* 132 */ HttpHeaders headers = new HttpHeaders();

			/* 134 */ HttpEntity<TransactionEnquiryRequest> request = new HttpEntity(TransactionEnquiry,
					(MultiValueMap) headers);
			/* 135 */ LOGGER.debug("JSON REQUEST: " + this.gson.toJson(TransactionEnquiry));
			/* 136 */ responseObject = (TransactionEnquiryResponse) this.restTemplate.postForObject(
					"http://localhost:80/H2HPaymentService/v1/transactionEnquiry?lan=@es".replace("@es", idioma),
					request, TransactionEnquiryResponse.class, new Object[0]);
			/* 137 */ return responseObject;
		}
		/* 139 */ catch (HttpClientErrorException client) {
			/* 140 */ LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			/* 141 */ if (client.getRawStatusCode() == 400 || client.getRawStatusCode() == 401)
				/* 142 */ responseObject = (TransactionEnquiryResponse) this.gson
						.fromJson(client.getResponseBodyAsString(), TransactionEnquiryResponse.class);
			/* 143 */ return responseObject;
		}
		/* 145 */ catch (Exception ex) {
			/* 146 */ LOGGER.error(
					"[ERROR AL CONSULTAR TRANSACCION ] [" + TransactionEnquiry.getTransactionReference() + "]", ex);
			/* 147 */ return new TransactionEnquiryResponse();
		}
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/H2HCLIENT.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */