package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.AddMoneyReq;
import com.addcel.mch2hpayment.client.model.AddMoneyRes;
import com.addcel.mch2hpayment.client.model.BillPocketClient;
import com.addcel.mch2hpayment.client.model.BillPocketResponse;
import com.addcel.mch2hpayment.client.model.MoneySendReq;
import com.addcel.mch2hpayment.client.model.MoneySendRes;
import com.addcel.mch2hpayment.client.model.PrevivaleClient;
import com.addcel.mch2hpayment.client.model.STPClient;
import com.addcel.mch2hpayment.client.model.ThreatMetrixClient;
import com.addcel.mch2hpayment.client.model.ThreatMetrixRequest;
import com.addcel.mch2hpayment.client.model.ThreatMetrixResponse;
import com.addcel.mch2hpayment.client.model.TokenClientMC;
import com.addcel.mch2hpayment.client.model.ValidateTokenRequest;
import com.addcel.mch2hpayment.client.model.ValidateTokenResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.H2HMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.AccountPrevivale;
import com.addcel.mch2hpayment.mybatis.model.vo.Bank;
import com.addcel.mch2hpayment.mybatis.model.vo.BillPocketAuthorization;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.AmexAutorization;
import com.addcel.mch2hpayment.spring.model.BP3DSRequest;
import com.addcel.mch2hpayment.spring.model.BPPaymentRequest;
import com.addcel.mch2hpayment.spring.model.BPPaymentResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.RespuestaAmex;
import com.addcel.mch2hpayment.spring.model.Token;
import com.addcel.mch2hpayment.spring.services.AmexService;
import com.addcel.mch2hpayment.spring.services.UtilsService;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Utils;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class PaymentService {
	/* 65 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.PaymentService.class);

	private String messageId;

	/* 69 */ private Gson GSON = new Gson();

	/* 71 */ private SimpleDateFormat SDF = new SimpleDateFormat("ddhhmmssSSS");

	@Autowired
	H2HMapper mapper;

	@Autowired
	AmexService amexService;

	@Autowired
	ThreatMetrixClient tmClient;

	@Autowired
	BillPocketClient billClient;

	@Autowired
	TokenClientMC tokenClient;

	@Autowired
	STPClient stpClient;

	@Autowired
	PrevivaleClient previvaleClient;

	public Token getToken(String data, String profileId, String sessionId, String remoteAddr, Integer idApp) {
		/* 95 */ Token respuesta = new Token();

		/* 97 */ String json = null;
		/* 98 */ PaymentRequest pago = null;
		/* 99 */ ThreatMetrixResponse validate = null;
		try {
			/* 101 */ if (sessionId != null) {

				/* 104 */ LOGGER.info("JSON: {}", data);
				/* 105 */ json = Utils.decryptJson(data);
				/* 106 */ LOGGER.info("JSON: {}", json);
				/* 107 */ pago = (PaymentRequest) this.GSON.fromJson(json, PaymentRequest.class);
				/* 108 */ validate = validatePayment(pago, sessionId, idApp);
				/* 109 */ if ("high".equals(validate.getTmxRisk())) {
					/* 110 */ respuesta.setMensajeError(
							"Se ha detectado actividad inusual en la transaccion. No se puede realizar el pago.");
					/* 111 */ respuesta.setIdError(900);
				} else {
					/* 113 */ if ("neutral".equals(validate.getTmxRisk())) {
						/* 114 */ respuesta.setSecure(true);
					}
					/* 116 */ String token = this.mapper.getToken();
					/* 117 */ LOGGER.info("TOKEN BD: " + token);
					/* 118 */ token = AddcelCrypto.encryptSensitive(this.SDF.format(new Date()), token);
					/* 119 */ LOGGER.info("TOKEN BD: " + token);
					/* 120 */ token = String.valueOf(token) + "#" + pago.getIdUser() + "#" + pago.getIdCard() + "#" +
					/* 121 */ sessionId + "#" + remoteAddr + "#" + pago.getAmount() + "#" + pago.getComision();
					/* 122 */ LOGGER.info("TOKEN GENERADO: " + token);
					/* 123 */ token = AddcelCrypto.encryptSensitive(this.SDF.format(new Date()), token);
					/* 124 */ LOGGER.info("TOKEN GENERADO: " + token);
					/* 125 */ respuesta.setAccountId(Utils.digest(String.valueOf(pago.getIdUser())));
					/* 126 */ respuesta.setToken(token);
					/* 127 */ respuesta.setMensajeError("Success");
					/* 128 */ json = this.GSON.toJson(respuesta);
					/* 129 */ json = AddcelCrypto.encryptSensitive(this.SDF.format(new Date()), json);
				}
			} else {
				/* 132 */ LOGGER.error("ERROR - LA PETICION NO INCLUYE SESSION ID TMX");
				/* 133 */ respuesta.setMensajeError("Peticion incompleta.");
				/* 134 */ respuesta.setIdError(400);
			}
			/* 136 */ } catch (Exception e) {
			/* 137 */ e.printStackTrace();
			/* 138 */ LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: [" + e.getCause() + "]");
			/* 139 */ respuesta.setMensajeError("No fue posible consultar el token en BD");
			/* 140 */ respuesta.setIdError(-1);
		}
		/* 142 */ LOGGER.info("CONSULTA DE TOKEN FINALIZADA");
		/* 143 */ return respuesta;
	}

	public boolean validaToken(String header, String remoteAddr, String id, BPPaymentRequest data, String accountId,
			int idApp, int idPais, String idioma) {
		/* 148 */ String[] headerArray = null;
		/* 149 */ String token = null;
		/* 150 */ String ip = null;
		/* 151 */ int tokenBd = 0;

		/* 154 */ Double amount = null;
		/* 155 */ Double comision = null;

		try {
			/* 158 */ LOGGER.debug("HEADER: {} - IP - {}", header, String.valueOf(remoteAddr) + " SESSION ID: " + id);

			/* 160 */ headerArray = header.split("#");
			/* 161 */ token = header;

			/* 163 */ LOGGER.debug("token descifrado: " + AddcelCrypto.decryptSensitive(token));

			/* 165 */ ValidateTokenRequest validate = new ValidateTokenRequest();
			/* 166 */ validate.setAccountId(accountId);
			/* 167 */ validate.setIdUsuario(data.getIdUser());
			/* 168 */ validate
					.setJson(AddcelCrypto.encryptSensitive(this.SDF.format(new Date()), this.GSON.toJson(data)));
			/* 169 */ validate.setToken(token);

			/* 171 */ LOGGER.debug("VALIDANDO TOKEN... " + this.GSON.toJson(validate));
			/* 172 */ ValidateTokenResponse resToken = this.tokenClient.validateToken(validate, Integer.valueOf(idApp),
					Integer.valueOf(idPais), idioma, token);

			/* 174 */ if (resToken != null && resToken.getCode().intValue() == 0) {
				/* 175 */ LOGGER.debug("TOKEN VALIDO...");
				/* 176 */ return true;
			}

			/* 179 */ } catch (Exception e) {
			/* 180 */ LOGGER.error("ERROR AL VALIDAR TOKEN: ", e);
		}

		/* 183 */ return false;
	}

	public BPPaymentResponse processPayment(Integer idApp, Integer idPais, String idioma, BPPaymentRequest data) {
		/* 187 */ BPPaymentResponse response = null;
		/* 188 */ BillPocketResponse responseBP = null;
		/* 189 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
		/* 190 */ String devolucion = "";
		try {
			/* 192 */ LOGGER.debug("INCIANDO TRANSFERENCIA " + this.GSON.toJson(data));

			/* 194 */ Proveedor proveedor = this.mapper.getProveedor("H2HBanorte");
			/* 195 */ Card card = this.mapper.getCard(data.getIdUser().longValue(), data.getIdCard().longValue(),
					idApp.intValue());
			/* 196 */ AccountRecord account = this.mapper.getAccount(data.getAccountId().longValue());
			/* 197 */ String rules_response = this.mapper.RulesH2HBanorte(data.getIdUser().longValue(),
					data.getAccountId().longValue(), data.getIdCard().longValue(), data.getAmount(),
					UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6), idioma, idApp.intValue());
			/* 198 */ McBaseResponse Rules = (McBaseResponse) this.GSON.fromJson(rules_response, McBaseResponse.class);

			/* 201 */ bitacora_banorte.setAccount_id(data.getAccountId().longValue());
			/* 202 */ bitacora_banorte.setAmount(data.getAmount().doubleValue());
			/* 203 */ bitacora_banorte.setId_card(data.getIdCard().longValue());
			/* 204 */ bitacora_banorte.setId_usuario(data.getIdUser().longValue());
			/* 205 */ bitacora_banorte.setIdioma(idioma);
			/* 206 */ bitacora_banorte.setComision(data.getComision().doubleValue());
			/* 207 */ bitacora_banorte.setConcepto("Transferencia Electronica MobileCard a " + account.getHolderName());
			/* 208 */ bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			/* 209 */ bitacora_banorte.setId_aplicacion(idApp.intValue());
			/* 210 */ bitacora_banorte.setLat(data.getLat().doubleValue());
			/* 211 */ bitacora_banorte.setLon(data.getLon().doubleValue());

			/* 213 */ bitacora_banorte.setNombre_destino(account.getHolderName());
			/* 214 */ bitacora_banorte.setCuenta_clabe_destino(account.getActNumber());
			/* 215 */ bitacora_banorte.setBanco_destino(account.getBankCode());
			/* 216 */ bitacora_banorte.setId_bitacora(0L);
			/* 217 */ bitacora_banorte.setBank_reference("0");
			/* 218 */ bitacora_banorte.setStatus3DS("-1");
			/* 219 */ bitacora_banorte.setResultado_payw("");
			/* 220 */ bitacora_banorte.setResultado_aut("");
			/* 221 */ bitacora_banorte.setCodigo_aut("");
			/* 222 */ bitacora_banorte.setTexto("");

			/* 225 */ if (Rules.getIdError() != 0) {
				/* 226 */ LOGGER.info("REGLAS INVALIDAS DETECTADAS " + rules_response);
				/* 227 */ response = new BPPaymentResponse();
				/* 228 */ response.setMessage(Rules.getMensajeError());
				/* 229 */ response.setCode(Integer.valueOf(Rules.getIdError()));
				/* 230 */ response.setMaskedPAN(Business.maskCard(Business.getSMS(card.getNumerotarjeta()), "*"));
				/* 231 */ return response;
			}

			/* 234 */ Bank ba = this.mapper.getCatalogoBancosByClave(account.getBankCode());
			/* 235 */ if (ba == null) {
				/* 236 */ response = new BPPaymentResponse();
				/* 237 */ if (idioma.equals("es")) {
					/* 238 */ response.setMessage(
							"Por cambio en las políticas, únicamente se aceptarán cuentas clabes, por favor actualiza tus beneficiarios");
				} else {
					/* 240 */ response.setMessage(
							"For change in policies, only clabe accounts will be accepted, please update your beneficiaries");
					/* 241 */ }
				response.setCode(Integer.valueOf(800));
				/* 242 */ response.setMaskedPAN(Business.maskCard(Business.getSMS(card.getNumerotarjeta()), "*"));
				/* 243 */ return response;
			}

			/* 246 */ if (card.getIdfranquicia() == 3) {

				/* 248 */ LOGGER.debug("INSERTANDO BITACORA PAGO AMEX");

				/* 249 */ TBitacoraVO bitacora = guardaTBitacora(0L, data.getIdUser().longValue(),
						data.getAmount().doubleValue(), 0.0D, data.getComision().doubleValue(),
						AddcelCrypto.decryptTarjeta(card.getNumerotarjeta()), idPais.intValue(), data.getImei(),
//						data.getSoftware(), (String) data.getLat(), (String) data.getLon(), idApp.intValue(),
						data.getSoftware(), String.valueOf(data.getLat()), String.valueOf(data.getLon()),
						idApp.intValue(), account.getActNumber());
				/* 250 */ responseBP = AmexAutorization(data.getAmount().doubleValue(),
						data.getComision().doubleValue(), AddcelCrypto.decryptTarjeta(card.getNumerotarjeta()),
						AddcelCrypto.decryptTarjeta(card.getVigencia()).replace("/", ""),
						AddcelCrypto.decryptTarjeta(card.getCt()), "");
				/* 251 */ if (responseBP.getCode().intValue() == 0) {
					/* 252 */ LOGGER.debug("ACTUALIZANDO BITACORA PAGO AMEX {}" + bitacora.getIdBitacora());
					/* 253 */ this.mapper.updateTbitacora(bitacora.getIdBitacora(), 1, responseBP.getAuthNumber());
				}
			} else {

				/* 257 */ responseBP = authorization(idApp, idPais, idioma, data, proveedor, account);
			}

			/* 260 */ response = new BPPaymentResponse();
			/* 261 */ response.setAmount(responseBP.getAmount() + data.getComision().doubleValue());
			/* 262 */ response.setAuthNumber(responseBP.getAuthNumber());
			/* 263 */ response.setDateTime(responseBP.getDateTime());
			/* 264 */ response.setIdTransaccion(responseBP.getIdTransaccion());
			/* 265 */ response.setMaskedPAN(responseBP.getMaskedPAN());
			/* 266 */ response.setOpId(responseBP.getOpId());
			/* 267 */ response.setStatus(responseBP.getStatus());
			/* 268 */ response.setTicketUrl(responseBP.getTicketUrl());
			/* 269 */ response.setTxnISOCode(responseBP.getTxnISOCode());
			/* 270 */ response.setMessage(responseBP.getMessage());
			/* 271 */ response.setCode(responseBP.getCode());
			/* 272 */ bitacora_banorte.setId_bitacora(responseBP.getIdTransaccion());
			/* 273 */ bitacora_banorte.setCodigo_aut(responseBP.getAuthNumber());
//			/* 274 */ bitacora_banorte.setStatus3DS((String) responseBP.getCode());
			bitacora_banorte.setStatus3DS(String.valueOf(responseBP.getCode()));
			/* 275 */ bitacora_banorte.setTexto(String.valueOf(bitacora_banorte.getTexto()) + "Response BillPocket:"
					+ responseBP.getCode() + ":" + responseBP.getMessage());

			/* 278 */ if (responseBP.getCode().intValue() == 0) {
				/* 280 */ MoneySendReq request = new MoneySendReq();
				/* 281 */ request.setConceptoPago("Transferencia Electronica MobileCard a " + account.getHolderName());
				/* 282 */ request.setCuentaBeneficiario(account.getActNumber());
				/* 283 */ request.setIdUsuario(data.getIdUser());
				/* 284 */ request.setInstitucionContraparte(account.getBankCode());
				/* 285 */ DecimalFormat formato1 = new DecimalFormat("#.00");

				/* 288 */ request.setMonto(formato1.format(data.getAmount()).replace(",", "."));
				/* 289 */ request.setNombreBeneficiario(account.getHolderName());
				/* 290 */ request.setTipoCuentaBeneficiario("40");
				/* 291 */ LOGGER.debug("ENVIANDO PRIMER INTENTO...");

				/* 293 */ MoneySendRes respSTP = this.stpClient.moneySend(request, idApp.intValue(), idPais.intValue(),
						idioma);

				/* 295 */ if (respSTP.getMessage().equals("Error validando la firma")) {

					/* 297 */ LOGGER.debug("SEGUNDO INTENTO STP ...");
					/* 298 */ respSTP = this.stpClient.moneySend(request, idApp.intValue(), idPais.intValue(), idioma);
				}

				/* 301 */ if (respSTP.getCode().intValue() != 1000) {
					/* 302 */ LOGGER.debug("NO SE PUDO TRANSFERIR DINERO CON STP: {} {}",
							respSTP.getCode() + ":" + data.getIdUser(), respSTP.getMessage());
					/* 303 */ LOGGER.debug("PREVIVALE: " + card.getPrevivale());
					/* 304 */ if (card.getPrevivale() != null && card.getPrevivale().intValue() == 1) {
						/* 305 */ LOGGER.debug("INICIANDO DEVOLUCION A TARJETA PREVIVALE :" + responseBP.getAmount()
								+ ":" + responseBP.getIdTransaccion() + ":" + data.getIdUser());
						/* 306 */ AddMoneyReq addMoney = new AddMoneyReq();
						/* 307 */ addMoney.setCantidad(Double.valueOf(responseBP.getAmount()));
						/* 308 */ addMoney.setIdUsuario(data.getIdUser());
						/* 309 */ addMoney.setTarjeta(card.getNumerotarjeta());
						/* 310 */ AddMoneyRes respPre = this.previvaleClient.agregarDinero(addMoney, idApp.intValue(),
								idPais.intValue(), idioma);

						/* 312 */ response.setCode(Integer.valueOf(6000));
						/* 313 */ if (idioma.equals("es")) {
							/* 314 */ response.setMessage("No se pudo realizar la transferencia " + respSTP.getMessage()
									+ " Respuesta devolucion: " + respPre.getMessage());
						} else {
							/* 316 */ response.setMessage("The transfer could not be made " + respSTP.getMessage()
									+ " Refund response: " + respPre.getMessage());
						}

						/* 319 */ if (respPre.getCode().intValue() == 1000) {
							/* 320 */ devolucion = ":Devolucion Aprobada: " + respPre.getData().getIdPeticion();
						} else {
							/* 322 */ devolucion = ":Error devolucion";
						}
						/* 324 */ LOGGER.debug("FINALIZANDO DEVOLUCION A TARJETA PREVIVALE");
					} else {

						/* 328 */ BillPocketResponse respRefund = this.billClient.Refund(responseBP.getIdTransaccion(),
								idApp, idPais, idioma);

						/* 330 */ response.setCode(Integer.valueOf(6000));
						/* 331 */ if (idioma.equals("es")) {
							/* 332 */ response.setMessage("No se pudo realizar la transferencia " + respSTP.getMessage()
									+ " Respuesta devolucion: " + respRefund.getMessage());
						} else {
							/* 334 */ response.setMessage("The transfer could not be made " + respSTP.getMessage()
									+ " Refund response: " + respRefund.getMessage());
						}
					}
				}

//				/* 340 */ bitacora_banorte.setBank_reference((String) respSTP.getIdOperacion());
				bitacora_banorte.setBank_reference(String.valueOf(respSTP.getIdOperacion()));
				/* 341 */ bitacora_banorte.setResultado_payw(respSTP.getIdPeticionWsStp());
				/* 342 */ bitacora_banorte.setResultado_aut(
						respSTP.getCode() + ":" + ((respSTP.getReferencia() != null) ? respSTP.getReferencia() : ""));
				/* 343 */ bitacora_banorte.setTexto(String.valueOf(bitacora_banorte.getTexto()) + "*Respuesta STP:"
						+ respSTP.getCode() + ":" + respSTP.getMessage() + devolucion);
			}

		}
		/* 347 */ catch (Exception e) {
			/* 348 */ LOGGER.error("error al procesar transferencia", e);
			/* 349 */ response = new BPPaymentResponse();
			/* 350 */ response.setCode(Integer.valueOf(600));
			/* 351 */ if (idioma.equals("es")) {
				/* 352 */ response.setMessage("ERROR AL PROCESAR TRANSFERENCIA, CONTACTE A SOPORTE");
			} else {
				/* 354 */ response.setMessage("ERROR WHEN PROCESSING TRANSFER, CONTACT SUPPORT");
				/* 355 */ }
			e.printStackTrace();
		} finally {
			/* 357 */ LOGGER.debug("insetando bitacora");
			/* 358 */ insertaBitacoraBanorte(bitacora_banorte);
		}

		/* 361 */ LOGGER.debug("RETORNANDO RESPUESTA: " + this.GSON.toJson(response));
		/* 362 */ return response;
	}

	public BillPocketResponse authorization(Integer idApp, Integer idPais, String idioma, BPPaymentRequest data,
			Proveedor proveedor, AccountRecord accountDes) {
		/* 366 */ BillPocketAuthorization authorization = new BillPocketAuthorization();
		/* 367 */ BillPocketResponse response = null;
		try {
			/* 369 */ RestTemplate restTemplate = new RestTemplate();
			/* 370 */ authorization.setIdUsuario(Long.valueOf(data.getIdUser().longValue()).longValue());
			/* 371 */ authorization.setIdProveedor(Integer.valueOf(proveedor.getId_proveedor().intValue()));
			/* 372 */ authorization.setIdProducto(Integer.valueOf(1));
			/* 373 */ authorization.setConcepto("Transferencia Electronica MobileCard a " + accountDes.getHolderName());
			/* 374 */ authorization.setImei(data.getImei());
			/* 375 */ authorization.setSoftware(data.getSoftware());
			/* 376 */ authorization.setModelo(data.getModel());
			/* 377 */ authorization.setLat(data.getLat().doubleValue());
			/* 378 */ authorization.setLon(data.getLon().doubleValue());
			/* 379 */ authorization.setIdTarjeta(data.getIdCard().longValue());
			/* 380 */ authorization.setCargo(data.getAmount().doubleValue());
			/* 381 */ authorization.setComision(data.getComision().doubleValue());
			/* 382 */ authorization.setReferencia(data.getReferencia());
			/* 383 */ authorization.setEmisor("");
			/* 384 */ authorization.setOperacion("");
			/* 385 */ HttpHeaders headers = new HttpHeaders();
			/* 386 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 387 */ HttpEntity<BillPocketAuthorization> request = new HttpEntity(authorization,
					(MultiValueMap) headers);
			/* 388 */ LOGGER.debug("ENVIANDO A BILLPOCKET: " + this.GSON.toJson(authorization));
			/* 389 */ ResponseEntity<String> respBP = restTemplate.exchange(
					"http://localhost/BillPocket/" + idApp + "/" + idPais + "/" + idioma + "/authorization",
					/* 390 */ HttpMethod.POST, request, String.class, new Object[0]);
			/* 391 */ LOGGER.info("RESPUESTA DE BILL POCKET - {}", respBP.getBody());
			/* 392 */ response = (BillPocketResponse) this.GSON.fromJson((String) respBP.getBody(),
					BillPocketResponse.class);
			/* 393 */ } catch (Exception e) {
			/* 394 */ e.printStackTrace();
		}
		/* 396 */ return response;
	}

	private BillPocketResponse AmexAutorization(double amount, double comision, String pan, String vigencia, String cvv,
			String domicilio) {
		/* 401 */ RespuestaAmex respAmex = null;
		/* 402 */ BillPocketResponse response = null;
		try {
			/* 404 */ double total = amount;
			/* 405 */ AmexAutorization pago = new AmexAutorization();
			/* 406 */ pago.setAfiliacion("9353192983");
			/* 407 */ pago.setComision(comision);
			/* 408 */ pago.setCvv2(cvv);
			/* 409 */ pago.setDom_amex(domicilio);
			/* 410 */ pago.setMonto(total);
			/* 411 */ pago.setProducto("Tranferencia");
			/* 412 */ pago.setTarjeta(pan);
			/* 413 */ pago.setVigencia(vigencia);

			/* 415 */ respAmex = this.amexService.solicitaAutorizacionAmex(pago);

			/* 417 */ response = new BillPocketResponse();
			/* 418 */ response.setAmount(amount);
			/* 419 */ response.setDateTime((new Date()).getTime());
			/* 420 */ response.setMaskedPAN(Business.maskCard(pan, "*"));
			/* 421 */ response.setOpId(Integer.valueOf(0));
			/* 422 */ response.setTicketUrl("");
			/* 423 */ response.setTxnISOCode("");

			/* 427 */ if (respAmex != null) {
				/* 428 */ response.setAuthNumber(respAmex.getTransaction());
				/* 429 */ response.setIdTransaccion(Long.parseLong(respAmex.getTransaction()));
				/* 430 */ response.setStatus(Integer.valueOf(1));
				/* 431 */ response.setMessage(respAmex.getDsc());
				/* 432 */ response.setCode(Integer.valueOf(0));
			} else {
				/* 434 */ response.setAuthNumber("");
				/* 435 */ response.setIdTransaccion(0L);
				/* 436 */ response.setStatus(Integer.valueOf(0));
				/* 437 */ response.setMessage("ERROR AL PROCESAR COBRO AMERICAN EXPRESS");
				/* 438 */ response.setCode(Integer.valueOf(100));
			}

			/* 441 */ } catch (Exception e) {
			/* 442 */ LOGGER.error("ERROR EN APROVICIONAMIENTO AMEX ", e);
			/* 443 */ response.setAuthNumber("");
			/* 444 */ response.setIdTransaccion(0L);
			/* 445 */ response.setStatus(Integer.valueOf(0));
			/* 446 */ response.setMessage("ERROR AL PROCESAR COBRO AMERICAN EXPRESS");
			/* 447 */ response.setCode(Integer.valueOf(100));
		}
		/* 449 */ return response;
	}

	private void insertaBitacoraBanorte(TBitacoraBanorte bitacora) {
		try {
			/* 456 */ if (bitacora.getId_bitacora() > 0L) {
				/* 457 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
				/* 458 */ bitacora_banorte.setAccount_id(bitacora.getAccount_id());
				/* 459 */ bitacora_banorte.setAmount(bitacora.getAmount());
				/* 460 */ bitacora_banorte.setId_bitacora(bitacora.getId_bitacora());
				/* 461 */ bitacora_banorte.setId_card(bitacora.getId_card());
				/* 462 */ bitacora_banorte.setId_usuario(bitacora.getId_usuario());
				/* 463 */ bitacora_banorte.setIdioma(bitacora.getIdioma());
				/* 464 */ bitacora_banorte.setTransaction_number(0L);
				/* 465 */ bitacora_banorte.setBank_reference(bitacora.getBank_reference());
				/* 466 */ bitacora_banorte.setComision(bitacora.getComision());
				/* 467 */ bitacora_banorte.setTexto(/* 468 */ Normalizer
						.normalize(bitacora.getTexto(), Normalizer.Form.NFD)/* 469 */ .replaceAll("[^\\p{ASCII}]", ""));
				/* 470 */ bitacora_banorte.setReferencia("");
				/* 471 */ bitacora_banorte
						.setResultado_payw((bitacora.getResultado_payw() == null) ? "" : bitacora.getResultado_payw());
				/* 472 */ bitacora_banorte
						.setResultado_aut((bitacora.getResultado_aut() == null) ? "" : bitacora.getResultado_aut());
				/* 473 */ bitacora_banorte.setBanco_emisor("");
				/* 474 */ bitacora_banorte.setTipo_tarjeta("");
				/* 475 */ bitacora_banorte.setMarca_tarjeta("");
				/* 476 */ bitacora_banorte
						.setCodigo_aut((bitacora.getCodigo_aut() == null) ? "" : bitacora.getCodigo_aut());
				/* 477 */ bitacora_banorte
						.setStatus3DS((bitacora.getStatus3DS() == null) ? "" : bitacora.getStatus3DS());
				/* 478 */ bitacora_banorte.setConcepto(bitacora.getConcepto());
				/* 479 */ bitacora_banorte.setTarjeta_transferencia(bitacora.getTarjeta_transferencia());
				/* 480 */ bitacora_banorte.setNombre_destino(bitacora.getNombre_destino());
				/* 481 */ bitacora_banorte.setCuenta_clabe_destino(bitacora.getCuenta_clabe_destino());
				/* 482 */ bitacora_banorte.setBanco_destino(bitacora.getBanco_destino());
				/* 483 */ bitacora_banorte.setId_aplicacion(bitacora.getId_aplicacion());
				/* 484 */ bitacora_banorte.setLat(bitacora.getLat());
				/* 485 */ bitacora_banorte.setLon(bitacora.getLon());

				/* 487 */ this.mapper.insertTbitacoraMCBanorte(bitacora_banorte);
			}

			/* 490 */ } catch (Exception ex) {
			/* 491 */ LOGGER.error("ERROR AL INSERTAR REGISTRO EN BITACORA BANORTE", ex);
		}
	}

	public BPPaymentResponse processPayment3DSBP(Integer idApp, Integer idPais, String idioma, BP3DSRequest data) {
		/* 533 */ BPPaymentResponse response = null;

		/* 535 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
		/* 536 */ String devolucion = "";
		/* 537 */ AddMoneyRes TransferPrev = new AddMoneyRes();
		/* 538 */ int statusTranfers = 0;
		/* 539 */ long idPeticion = 0L;
		try {
			/* 541 */ LOGGER.debug("INCIANDO TRANSFERENCIA 3DS" + this.GSON.toJson(data));

			/* 543 */ response = new BPPaymentResponse();
			/* 544 */ String activo = this.mapper.getParameter("@ACTIVETRANSFERS");
			/* 545 */ if (activo.equals("0") || data.getIdUser().longValue() != 1503601741480L) {
				/* 546 */ LOGGER.debug("RETORNANDO SERVICIO NO DISPONIBLE...");
				/* 547 */ response.setCode(Integer.valueOf(400));
				/* 548 */ response.setMessage("Servicio no disponible.");
				/* 549 */ return response;
			}

			/* 553 */ Proveedor proveedor = this.mapper.getProveedor("H2HBanorte");
			/* 554 */ Card card = this.mapper.getCard(data.getIdUser().longValue(), data.getIdCard().longValue(),
					idApp.intValue());
			/* 555 */ AccountRecord account = this.mapper.getAccount(data.getAccountId().longValue());

			/* 560 */ bitacora_banorte.setAccount_id(data.getAccountId().longValue());
			/* 561 */ bitacora_banorte.setAmount(data.getAmount().doubleValue());
			/* 562 */ bitacora_banorte.setId_card(data.getIdCard().longValue());
			/* 563 */ bitacora_banorte.setId_usuario(data.getIdUser().longValue());
			/* 564 */ bitacora_banorte.setIdioma(idioma);
			/* 565 */ bitacora_banorte.setComision(data.getComision().doubleValue());
			/* 566 */ bitacora_banorte.setConcepto("Transferencia Electronica MobileCard a " + account.getHolderName());
			/* 567 */ bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			/* 568 */ bitacora_banorte.setId_aplicacion(idApp.intValue());
			/* 569 */ bitacora_banorte.setLat(data.getLat().doubleValue());
			/* 570 */ bitacora_banorte.setLon(data.getLon().doubleValue());

			/* 572 */ bitacora_banorte.setNombre_destino(account.getHolderName());
			/* 573 */ bitacora_banorte.setCuenta_clabe_destino(account.getActNumber());
			/* 574 */ bitacora_banorte.setBanco_destino(account.getBankCode());
			/* 575 */ bitacora_banorte.setId_bitacora(data.getIdBitacora());
			/* 576 */ bitacora_banorte.setBank_reference("0");
			/* 577 */ bitacora_banorte.setStatus3DS("-1");
			/* 578 */ bitacora_banorte.setResultado_payw("");
			/* 579 */ bitacora_banorte.setResultado_aut("");
			/* 580 */ bitacora_banorte.setCodigo_aut("");
			/* 581 */ bitacora_banorte.setTexto("");

			/* 593 */ Bank ba = this.mapper.getCatalogoBancosByClave(account.getBankCode());
			/* 594 */ if (ba == null) {
				/* 595 */ response = new BPPaymentResponse();
				/* 596 */ if (idioma.equals("es")) {
					/* 597 */ response.setMessage(
							"Por cambio en las políticas, únicamente se aceptarán cuentas clabes, por favor actualiza tus beneficiarios");
				} else {
					/* 599 */ response.setMessage(
							"For change in policies, only clabe accounts will be accepted, please update your beneficiaries");
					/* 600 */ }
				response.setCode(Integer.valueOf(800));
				/* 601 */ response.setMaskedPAN(Business.maskCard(Business.getSMS(card.getNumerotarjeta()), "*"));
				/* 602 */ return response;
			}

			/* 607 */ response = new BPPaymentResponse();
			/* 608 */ response.setAmount(data.getAmount().doubleValue() + data.getComision().doubleValue());
			/* 609 */ response.setAuthNumber("");
			/* 610 */ response.setDateTime((new Date()).getTime());
			/* 611 */ response.setIdTransaccion(0L);
			/* 612 */ response.setMaskedPAN("");
			/* 613 */ response.setOpId(Integer.valueOf(0));
			/* 614 */ response.setStatus(Integer.valueOf(data.getStatus()));
			/* 615 */ response.setTicketUrl("");
			/* 616 */ response.setTxnISOCode("");
			/* 617 */ response.setMessage("");
			/* 618 */ response.setCode(Integer.valueOf(0));
			/* 619 */ bitacora_banorte.setId_bitacora(data.getIdBitacora());
			/* 620 */ bitacora_banorte.setCodigo_aut("");
			/* 621 */ bitacora_banorte.setStatus3DS("");
			/* 622 */ bitacora_banorte.setTexto("");

			/* 625 */ if (data.getStatus() == 1) {
				/* 626 */ LOGGER.debug("COBRO 3DS EXITOSO....");
				/* 627 */ bitacora_banorte.setStatus3DS("1");

				/* 629 */ MoneySendReq request = new MoneySendReq();
				/* 630 */ request.setConceptoPago("Transferencia Electronica MobileCard a " + account.getHolderName());
				/* 631 */ request.setCuentaBeneficiario(account.getActNumber());
				/* 632 */ request.setIdUsuario(data.getIdUser());
				/* 633 */ request.setInstitucionContraparte(account.getBankCode());
				/* 634 */ DecimalFormat formato1 = new DecimalFormat("#.00");

				/* 636 */ request.setMonto(formato1.format(data.getAmount()));
				/* 637 */ request.setNombreBeneficiario(account.getHolderName());
				/* 638 */ request.setTipoCuentaBeneficiario("40");

				/* 640 */ MoneySendRes respSTP = new MoneySendRes();

				/* 642 */ if (!account.getActNumber().startsWith("646180")) {
					/* 643 */ LOGGER.debug("ENVIANDO PRIMER INTENTO...");

					/* 645 */ respSTP = this.stpClient.moneySend(request, idApp.intValue(), idPais.intValue(), idioma);

					/* 647 */ if (respSTP.getMessage().equals("Error validando la firma")) {

						/* 649 */ LOGGER.debug("SEGUNDO INTENTO STP ...");
						/* 650 */ respSTP = this.stpClient.moneySend(request, idApp.intValue(), idPais.intValue(),
								idioma);
					}

					/* 653 */ if (respSTP != null && respSTP.getCode().intValue() == 1000) {
						/* 654 */ statusTranfers = 1;
					}
				} else {
					/* 657 */ AccountPrevivale accountprev = this.mapper.getAccountPrevivale(account.getActNumber());

					/* 659 */ if (accountprev != null
							&& (accountprev.getIdComercio() > 0L || accountprev.getIdUsuario() > 0L)) {
						/* 660 */ AddMoneyReq addMoney = new AddMoneyReq();

						// 31-03-2020
						// Se modifica la cantidad porque actualmente se esta depositando el monto más
						// la comisión
						// Se deja seteada solo el mont
						/* 661 */ // addMoney.setCantidad(Double.valueOf(data.getAmount().doubleValue() + data.getComision().doubleValue()));
						
						LOGGER.debug("--- APLICANDO CAMBIO DE COMISION : " + data.getAmount().doubleValue() + " ---" );
						addMoney.setCantidad(Double.valueOf(data.getAmount().doubleValue()));
						/* 662 */ if (accountprev.getIdUsuario() > 0L) {
							/* 663 */ addMoney.setIdUsuario(data.getIdUser());
						} else {
							/* 665 */ addMoney.setIdEstablecimiento(Long.valueOf(accountprev.getIdComercio()));
						}
						/* 667 */ addMoney.setTarjeta(accountprev.getPan());
						/* 668 */ TransferPrev = this.previvaleClient.agregarDinero(addMoney, idApp.intValue(),
								idPais.intValue(), idioma);

						/* 670 */ if (TransferPrev != null && TransferPrev.getCode().intValue() == 1000)
							/* 671 */ statusTranfers = 1;
					} else {
						/* 673 */ LOGGER.debug("NO SE ENCONTRO CUENTA CLABE ASIGNADA A USUARIO O COMERCIO");
						/* 674 */ response = new BPPaymentResponse();
						/* 675 */ if (idioma.equals("es")) {
							/* 676 */ response.setMessage("La cuenta destino presenta errores, contacte a soporte");
						} else {
							/* 678 */ response.setMessage("Destination account has errors, contact support");
							/* 679 */ }
						response.setCode(Integer.valueOf(300));
						/* 680 */ return response;
					}
				}

				try {
					/* 686 */ if (respSTP != null && respSTP.getIdPeticionWsStp() != null
							&& !respSTP.getIdPeticionWsStp().isEmpty() && !respSTP.getIdPeticionWsStp().equals("0")) {
						/* 687 */ idPeticion = Long.parseLong(respSTP.getIdPeticionWsStp());
						/* 688 */ } else if (TransferPrev != null && TransferPrev.getData() != null
								&& TransferPrev.getData().getIdPeticion().intValue() != 0) {
//						/* 689 */ idPeticion = Long.parseLong((String) TransferPrev.getData().getIdPeticion());
						idPeticion = Long.parseLong(String.valueOf(TransferPrev.getData().getIdPeticion()));
						/* 690 */ }
					bitacora_banorte.setTransaction_number(idPeticion);
					/* 691 */ } catch (Exception ex) {
					/* 692 */ LOGGER.error("ERROR AL GUARDAR NUMERO DE TRANSACCION STP", ex);
				}

				/* 695 */ if (statusTranfers != 1) {
					/* 696 */ LOGGER.debug("NO SE PUDO TRANSFERIR DINERO CON STP o PREVIVALE: {} {}",
							respSTP.getCode() + ":" + data.getIdUser(), respSTP.getMessage());
					/* 697 */ LOGGER.debug("PREVIVALE: " + card.getPrevivale());
					/* 698 */ if (card.getPrevivale() != null && card.getPrevivale().intValue() == 1) {
						/* 699 */ LOGGER.debug("INICIANDO DEVOLUCION A TARJETA PREVIVALE :" + data.getAmount() + ":"
								+ data.getIdBitacora() + ":" + data.getIdUser());
						/* 700 */ AddMoneyReq addMoney = new AddMoneyReq();
						/* 701 */ addMoney.setCantidad(
								Double.valueOf(data.getAmount().doubleValue() + data.getComision().doubleValue()));
						/* 702 */ addMoney.setIdUsuario(data.getIdUser());
						/* 703 */ addMoney.setTarjeta(card.getNumerotarjeta());
						/* 704 */ AddMoneyRes respPre = this.previvaleClient.agregarDinero(addMoney, idApp.intValue(),
								idPais.intValue(), idioma);

						/* 706 */ response.setCode(Integer.valueOf(6000));
						/* 707 */ if (idioma.equals("es")) {
							/* 708 */ response.setMessage("No se pudo realizar la transferencia" + respSTP.getMessage()
									+ " Respuesta devolucion: " + respPre.getMessage());
						} else {
							/* 710 */ response.setMessage("The transfer could not be made" + respSTP.getMessage()
									+ " Refund response: " + respPre.getMessage());
						}

						/* 713 */ if (respPre.getCode().intValue() == 1000) {
							/* 714 */ devolucion = ":Devolucion Aprobada:" + respPre.getData().getIdPeticion();
						} else {
							/* 716 */ devolucion = ":Error devolucion";
						}
						/* 718 */ LOGGER.debug("FINALIZANDO DEVOLUCION A TARJETA PREVIVALE");
					} else {

						/* 722 */ BillPocketResponse respRefund = this.billClient.Refund(data.getIdBitacora(), idApp,
								idPais, idioma);

						/* 724 */ response.setCode(Integer.valueOf(6000));
						/* 725 */ if (idioma.equals("es")) {
							/* 726 */ response.setMessage("No se pudo realizar la transferencia" + respSTP.getMessage()
									+ " Respuesta devolucion: " + respRefund.getMessage());
						} else {
							/* 728 */ response.setMessage("The transfer could not be made" + respSTP.getMessage()
									+ " Refund response: " + respRefund.getMessage());
						}
					}
				}

//				/* 734 */ bitacora_banorte.setBank_reference((String) respSTP.getIdOperacion());
				bitacora_banorte.setBank_reference(String.valueOf(respSTP.getIdOperacion()));
				/* 735 */ bitacora_banorte.setResultado_payw(respSTP.getIdPeticionWsStp());
				/* 736 */ bitacora_banorte.setResultado_aut(
						respSTP.getCode() + ":" + ((respSTP.getReferencia() != null) ? respSTP.getReferencia() : ""));
				/* 737 */ bitacora_banorte.setTexto(String.valueOf(bitacora_banorte.getTexto()) + "*Respuesta STP:"
						+ respSTP.getCode() + ":" + respSTP.getMessage() + devolucion);
			} else {

				/* 740 */ LOGGER.debug("COBRO NO EXITOSO, NO SE REALIZA TRANSFERENCIA...");
			}

			/* 743 */ } catch (Exception e) {
			/* 744 */ LOGGER.error("error al procesar transferencia", e);
			/* 745 */ response = new BPPaymentResponse();
			/* 746 */ response.setCode(Integer.valueOf(600));
			/* 747 */ if (idioma.equals("es")) {
				/* 748 */ response.setMessage("ERROR AL PROCESAR TRANSFERENCIA, CONTACTE A SOPORTE");
			} else {
				/* 750 */ response.setMessage("ERROR WHEN PROCESSING TRANSFER, CONTACT SUPPORT");
				/* 751 */ }
			e.printStackTrace();
		} finally {
			/* 753 */ LOGGER.debug("insetando bitacora");
			/* 754 */ insertaBitacoraBanorte(bitacora_banorte);
		}

		/* 757 */ LOGGER.debug("RETORNANDO RESPUESTA: " + this.GSON.toJson(response));
		/* 758 */ return response;
	}

	private ThreatMetrixResponse validatePayment(PaymentRequest pago, String sessionId, Integer idApp) {
		/* 764 */ ThreatMetrixRequest request = new ThreatMetrixRequest();
		/* 765 */ ThreatMetrixResponse validate = null;
		/* 766 */ User usuarioVO = null;
		/* 767 */ Card card = null;

		try {
			/* 770 */ usuarioVO = this.mapper.getUserData(pago.getIdUser(), idApp.intValue());
			/* 771 */ card = this.mapper.getCard(pago.getIdUser(), pago.getIdCard(), idApp.intValue());
			/* 772 */ LOGGER
					.info("DATOS  DE USUARIO: " + usuarioVO.getUsr_nombre() + ", TARJETA: " + card.getNumerotarjeta() +
					/* 773 */ ", VIGENCIA: " + card.getVigencia() + ", CVV: " + card.getCt() + ",  ID TARJETA: "
							+ pago.getIdCard());
			/* 774 */ request.setApplicationName("mobilecardmx");
			/* 775 */ request.setCantidad(pago.getAmount() + pago.getComision());
			/* 776 */ request.setCardNumber(AddcelCrypto.decryptTarjeta(card.getNumerotarjeta()).substring(0, 6));
			/* 777 */ request.setCorreo(usuarioVO.geteMail());
			/* 778 */ request.setNombre(usuarioVO.getUsr_nombre());
			/* 779 */ request.setSessionId(sessionId);
			/* 780 */ request.setTelefono(usuarioVO.getUsr_telefono());
			/* 781 */ request.setUsername(usuarioVO.getUsr_nombre());
			/* 782 */ validate = this.tmClient.investigatePayment(request);
			/* 783 */ } catch (Exception e) {
			/* 784 */ e.printStackTrace();
			/* 785 */ validate = new ThreatMetrixResponse();
			/* 786 */ validate.setCode(Integer.valueOf(500));
			/* 787 */ validate.setMessage("No se puedo realizar la validacion, intente de nuevo. ");
		}
		/* 789 */ LOGGER.info("RESPONSE VALIDATE USER THREATMETRIX - {}", this.GSON.toJson(validate));
		/* 790 */ return validate;
	}

	private String digest(String text) {
		/* 794 */ String digest = "";
		/* 795 */ BigInteger bigIntDgst = null;
		try {
			/* 797 */ MessageDigest md = MessageDigest.getInstance("MD5");
			/* 798 */ md.update(text.getBytes(), 0, text.length());
			/* 799 */ bigIntDgst = new BigInteger(1, md.digest());
			/* 800 */ digest = bigIntDgst.toString(16);
			/* 801 */ digest = String.format("%040x", new Object[] { bigIntDgst });
			/* 802 */ } catch (NoSuchAlgorithmException e) {
			/* 803 */ LOGGER.info("Error al encriptar - digest", e);
		}
		/* 805 */ return digest;
	}

	public boolean isActive(Integer idApp) {
		/* 809 */ boolean active = true;
		/* 810 */ String band = null;

		/* 812 */ band = this.mapper.isAppActive(idApp);
		/* 813 */ if (band != null)
			/* 814 */ if (band.equals("T")) {
				/* 815 */ active = true;
			} else {
				/* 817 */ active = false;
			}
		/* 819 */ if (active) {
			/* 820 */ LOGGER.debug("SERVICIO AUTORIZADO PARA ID {}", idApp);
		} else {
			/* 822 */ LOGGER.debug("SERVICIO DENEGADO PARA ID {}", idApp);
			/* 823 */ }
		return active;
	}

	private TBitacoraVO guardaTBitacora(long idEstablecimiento, long idUsuario, double amount, double propina,
			double comision, String pan, int idPais, String imei, String software, String cx, String cy, int idApp,
			String destino) {
		/* 829 */ TBitacoraVO bitacora = new TBitacoraVO();
		try {
			/* 831 */ Proveedor proveedor = this.mapper.getProveedor("H2HBanorte");
			/* 832 */ bitacora.setTipo("");
			/* 833 */ bitacora.setSoftware(software);
			/* 834 */ bitacora.setModelo("");
			/* 835 */ bitacora.setWkey("");
			/* 836 */ bitacora.setImei(imei);
			/* 837 */ bitacora.setIdUsuario((new StringBuilder(String.valueOf(idUsuario))).toString());
			/* 838 */ bitacora.setIdProducto(1);
			/* 839 */ bitacora.setCar_id(1);
			/* 840 */ bitacora.setCargo(amount + propina);
			/* 841 */ bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			/* 842 */ bitacora.setConcepto("Transferencia:  Monto: " + amount + " Comision: " + comision);
			/* 843 */ bitacora.setImei("");
			/* 844 */ bitacora.setDestino("");
			/* 845 */ bitacora.setTarjeta_compra(AddcelCrypto.encryptTarjeta(pan));
			/* 846 */ bitacora.setAfiliacion("");
			/* 847 */ bitacora.setComision(comision);
			/* 848 */ bitacora.setCx(cx);
			/* 849 */ bitacora.setCy(cy);
			/* 850 */ bitacora.setPin("");
			/* 851 */ bitacora.setBitTicket("");
			/* 852 */ bitacora.setIdPais(idPais);
			/* 853 */ bitacora.setBitStatus(0);
			/* 854 */ bitacora.setId_aplicacion(idApp);
			/* 855 */ bitacora.setBitCodigoError(0);
			/* 856 */ bitacora.setDestino(destino);
			/* 857 */ this.mapper.addBitacora(bitacora);
		}
		/* 859 */ catch (Exception e) {
			/* 860 */ LOGGER.error("ERROR AL GUARDAR BITACORA SCAN & PAY", e);
		}
		/* 862 */ return bitacora;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/PaymentService.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */