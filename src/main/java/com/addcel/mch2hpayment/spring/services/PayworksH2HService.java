package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AccountRecord;
import com.addcel.mch2hpayment.client.model.EnqueuePaymentRequest;
import com.addcel.mch2hpayment.client.model.EnqueueTransactionResponse;
import com.addcel.mch2hpayment.client.model.LoginRequest;
import com.addcel.mch2hpayment.client.model.LoginResponse;
import com.addcel.mch2hpayment.client.model.LogoutRequest;
import com.addcel.mch2hpayment.client.model.LogoutResponse;
import com.addcel.mch2hpayment.mybatis.model.mapper.H2HMapper;
import com.addcel.mch2hpayment.mybatis.model.vo.AfiliacionVO;
import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.mybatis.model.vo.Card;
import com.addcel.mch2hpayment.mybatis.model.vo.ProjectMC;
import com.addcel.mch2hpayment.mybatis.model.vo.Proveedor;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.TBitacoraVO;
import com.addcel.mch2hpayment.mybatis.model.vo.TransaccionBanorte;
import com.addcel.mch2hpayment.mybatis.model.vo.User;
import com.addcel.mch2hpayment.spring.model.AmexAutorization;
import com.addcel.mch2hpayment.spring.model.MCTransafersResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.PayworksVO;
import com.addcel.mch2hpayment.spring.model.RespuestaAmex;
import com.addcel.mch2hpayment.spring.services.AmexService;
import com.addcel.mch2hpayment.spring.services.H2HCLIENT;
import com.addcel.mch2hpayment.spring.services.UtilsService;
import com.addcel.mch2hpayment.utils.Business;
import com.addcel.mch2hpayment.utils.Constantes;
import com.google.gson.Gson;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

@Service
public class PayworksH2HService {
	/* 64 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.PayworksH2HService.class);

	private String messageId;
	/* 67 */ private Gson gson = new Gson();
	/* 68 */ private SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
	/* 69 */ private H2HCLIENT h2hclient = new H2HCLIENT();

	private LoginResponse login;

	@Autowired
	H2HMapper mapper;

	@Autowired
	AmexService amexService;

	public ModelAndView send3DSBanorte(PaymentRequest request, int idApp, double lat, double lon) {
		/* 80 */ this.messageId = (new StringBuilder(String.valueOf(request.getIdUser()))).toString();
		/* 81 */ ModelAndView view = new ModelAndView();
		/* 82 */ AfiliacionVO afiliacion = null;

		try {
			/* 85 */ Calendar calendario = new GregorianCalendar();
			/* 86 */ int hora = calendario.get(11);
			/* 87 */ Card card = this.mapper.getCard(request.getIdUser(), request.getIdCard(), idApp);

			/* 105 */ String rules_response = this.mapper.RulesH2HBanorte(request.getIdUser(), request.getAccountId(),
					request.getIdCard(), Double.valueOf(request.getAmount()),
					UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6), request.getIdioma(), idApp);
			/* 106 */ McBaseResponse Rules = (McBaseResponse) this.gson.fromJson(rules_response, McBaseResponse.class);
			/* 107 */ if (Rules.getIdError() != 0) {
				/* 108 */ LOGGER.info("REGLAS INVALIDAS DETECTADAS " + rules_response);
				/* 109 */ view = new ModelAndView("payworks/pagina-error");
				/* 110 */ view.addObject("idApp", Integer.valueOf(idApp));
				/* 111 */ view.addObject("mensajeError", Rules.getMensajeError());
				/* 112 */ return view;
			}

			/* 117 */ User user = this.mapper.getUserData(request.getIdUser(), idApp);
			/* 118 */ if (user.getId_usr_status() != 1) {
				/* 119 */ LOGGER.info("[DETECTAMOS USUARIO INACTIVO] " + this.gson.toJson(request));
				/* 120 */ view = new ModelAndView("payworks/pagina-error");
				/* 121 */ view.addObject("idApp", Integer.valueOf(idApp));
				/* 122 */ if (request.getIdioma().equals("es")) {
					/* 123 */ view.addObject("mensajeError",
							"Existen problemas con su usuario, Para aclaraciones marque 01-800-925-5001.");
				} else {
					/* 125 */ view.addObject("mensajeError",
							"There are problems with your user. For clarifications call 01-800-925-5001.");
				}
				/* 127 */ return view;
			}

			/* 130 */ AccountRecord toAccount = this.mapper.getAccount(request.getAccountId());

			/* 132 */ if (toAccount.getBankCode().equals("072") || toAccount.getBankCode().equals("032")) {
				/* 134 */ if (toAccount.getActType().equals("CLABE") || toAccount.getActNumber().length() > 11) {

					/* 137 */ view = new ModelAndView("payworks/pagina-error");
					/* 138 */ view.addObject("idApp", Integer.valueOf(idApp));
					/* 139 */ if (request.getIdioma().equals("es")) {
						/* 140 */ view.addObject("mensajeError",
								/* 141 */ "Hemos detectado inconsistencias en la cuenta destino, favor de verificarla");
					} else {
						/* 143 */ view.addObject("mensajeError",
								"We have detected inconsistencies in the destination account, please verify it");
					}
					/* 145 */ return view;
				}
			}

			/* 152 */ if (card.getEstado() == -1) {
				/* 153 */ LOGGER.info("[SE DETECTO UNA TARJETA BLOQUEADA] " + request.getIdUser());
				/* 154 */ view = new ModelAndView("payworks/pagina-error");
				/* 155 */ view.addObject("idApp", Integer.valueOf(idApp));
				/* 156 */ if (request.getIdioma().equals("es")) {
					/* 157 */ view.addObject("mensajeError",
							"La tarjeta seleccionada se encuentra bloqueda para realizar movimientos. Para aclaraciones marque 01-800-925-5001.");
				} else {
					/* 159 */ view.addObject("mensajeError",
							"The selected card is locked to make movements. For clarifications call 01-800-925-5001.");
				}
				/* 161 */ return view;
			}

			/* 166 */ BankCodes bank = this.mapper.getBankcodesByClave(toAccount.getBankCode());
			/* 167 */ double comisionCalc = bank.getComision_fija()
					+ (Double.valueOf(request.getAmount()).doubleValue() + bank.getComision_fija())
							* bank.getComision_porcentaje();
			/* 168 */ if (request.getComision() == 0.0D) {
				/* 169 */ request.setComision(comisionCalc);
			}

			/* 175 */ Proveedor proveedor = this.mapper.getProveedor("H2HBanorte");
			/* 176 */ afiliacion = this.mapper.buscaAfiliacion("BANORTEH2HN");

			/* 180 */ if (card.getIdfranquicia() == 3) {
				/* 181 */ LOGGER
						.info("[SE DETECTO UNA TARJETA AMEX] " + request.getIdUser() + " " + request.getIdCard());
				/* 182 */ view = new ModelAndView("payworks/pagina-error");
				/* 183 */ view.addObject("idApp", Integer.valueOf(idApp));
				/* 184 */ if (request.getIdioma().equals("es")) {
					/* 185 */ view.addObject("mensajeError",
							"Estimado usuario, estamos trabajando para poder realizar transferencias con tarjetas American Express.");
				} else {
					/* 187 */ view.addObject("mensajeError",
							"Dear User, we are working to make transfers with American Express cards.");
				}
				/* 189 */ return view;
			}

			/* 195 */ int bin = this.mapper.isBinValido(UtilsService.getSMS(card.getNumerotarjeta()).substring(0, 6));

			/* 197 */ if (bin == 0) {
				/* 198 */ LOGGER.info("[SE DETECTO UNA TARJETA NO BIN VALIDO] " + request.getIdUser() + " ");
				/* 199 */ view = new ModelAndView("payworks/pagina-error");
				/* 200 */ view.addObject("idApp", Integer.valueOf(idApp));
				/* 201 */ if (request.getIdioma().equals("es")) {
					/* 202 */ view.addObject("mensajeError",
							"Estimado usuario, las transferencia solo aplica para tarjetas Mexicanas.");
				} else {
					/* 204 */ view.addObject("mensajeError", "Dear User, the transfer only applies to Mexican cards.");
				}
				/* 206 */ return view;
			}

			/* 209 */ String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			/* 210 */ String vig = UtilsService.getSMS(card.getVigencia());
			/* 211 */ String ct = UtilsService.getSMS(card.getCt());

			/* 214 */ LOGGER.debug("Comision calculada: " + comisionCalc);
			/* 215 */ LOGGER.debug("comision request: " + request.getComision());
			/* 216 */ LOGGER.debug("amount " + request.getAmount());
			/* 217 */ String cardType = (card.getIdfranquicia() == 1) ? "VISA" : "MC";
			/* 218 */ double comision = request.getComision();
			/* 219 */ BigDecimal bd = new BigDecimal(comision);
			/* 220 */ bd = bd.setScale(2, RoundingMode.HALF_DOWN);
			/* 221 */ comision = bd.doubleValue();

			/* 224 */ TBitacoraVO bitacora = new TBitacoraVO();
			/* 225 */ bitacora.setTipo("");
			/* 226 */ bitacora.setSoftware("");
			/* 227 */ bitacora.setModelo("");
			/* 228 */ bitacora.setWkey("");
			/* 229 */ bitacora.setIdUsuario((new StringBuilder(String.valueOf(request.getIdUser()))).toString());
			/* 230 */ bitacora.setIdProducto(1);
			/* 231 */ bitacora.setCar_id(1);
			/* 232 */ bitacora.setCargo(request.getAmount() + comision);
			/* 233 */ bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			/* 234 */ bitacora.setConcepto("Transferencia H2HBANORTE(" + ((idApp == 1) ? "Mobilecard" : "Telecom")
					+ "): Concepto: " + request.getConcept() + " Monto: " +
					/* 235 */ request.getAmount() + " Comision: " + comision);
			/* 236 */ bitacora.setImei("");
			/* 237 */ bitacora.setDestino((new StringBuilder(String.valueOf(request.getAccountId()))).toString());
			/* 238 */ bitacora.setTarjeta_compra(card.getNumerotarjeta());
			/* 239 */ bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
			/* 240 */ bitacora.setComision(comision);
			/* 241 */ bitacora.setCx("");
			/* 242 */ bitacora.setCy("");
			/* 243 */ bitacora.setPin("");
			/* 244 */ bitacora.setId_aplicacion(idApp);
			/* 245 */ this.mapper.addBitacora(bitacora);

			/* 247 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			/* 248 */ bitacora_banorte.setAccount_id(request.getAccountId());
			/* 249 */ bitacora_banorte.setAmount(request.getAmount());
			/* 250 */ bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			/* 251 */ bitacora_banorte.setId_card(request.getIdCard());
			/* 252 */ bitacora_banorte.setId_usuario(request.getIdUser());
			/* 253 */ bitacora_banorte.setIdioma(request.getIdioma());
			/* 254 */ bitacora_banorte.setTransaction_number(0L);
			/* 255 */ bitacora_banorte.setBank_reference("");
			/* 256 */ bitacora_banorte.setComision(comision);
			/* 257 */ bitacora_banorte.setTexto("");
			/* 258 */ bitacora_banorte.setReferencia("");
			/* 259 */ bitacora_banorte.setResultado_payw("");
			/* 260 */ bitacora_banorte.setResultado_aut("");
			/* 261 */ bitacora_banorte.setBanco_emisor("");
			/* 262 */ bitacora_banorte.setTipo_tarjeta("");
			/* 263 */ bitacora_banorte.setMarca_tarjeta("");
			/* 264 */ bitacora_banorte.setCodigo_aut("");
			/* 265 */ bitacora_banorte.setStatus3DS("");
			/* 266 */ bitacora_banorte.setConcepto(request.getConcept());
			/* 267 */ bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			/* 268 */ bitacora_banorte.setNombre_destino(toAccount.getHolderName());
			/* 269 */ bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
			/* 270 */ bitacora_banorte.setBanco_destino(toAccount.getBankCode());
			/* 271 */ bitacora_banorte.setId_aplicacion(idApp);
			/* 272 */ bitacora_banorte.setLat(lat);
			/* 273 */ bitacora_banorte.setLon(lon);

			/* 275 */ this.mapper.insertTbitacoraMCBanorte(bitacora_banorte);

			/* 277 */ double total = comision + request.getAmount();

			/* 280 */ PayworksVO payVO = new PayworksVO();
			/* 281 */ payVO.setCard(numcard);
			/* 282 */ payVO.setCardType(cardType);
			/* 283 */ payVO.setExpires(vig);
			/* 284 */ payVO.setForwardPath(afiliacion.getForwardPath().split(" ")[0]);
			/* 285 */ payVO.setMerchantId(afiliacion.getMerchantId());
			/* 286 */ payVO.setReference3D((new StringBuilder(String.valueOf(bitacora.getIdBitacora()))).toString());
			/* 287 */ payVO
					.setTotal(Business.formatoMontoPayworks((new StringBuilder(String.valueOf(total))).toString()));

			/* 289 */ LOGGER.info("ENVIO DE INFO AL 3DS BANORTE[" + this.messageId + "]" + "["
					+ bitacora.getIdBitacora() + "] " + this.gson.toJson(payVO));

			/* 291 */ view.setViewName("payworks/comercio-send");
			/* 292 */ view.addObject("payworks", payVO);
			/* 293 */ view.addObject("idApp", Integer.valueOf(idApp));
		}
		/* 295 */ catch (Exception ex) {

			/* 297 */ LOGGER.error("[ERROR AL ENVIAR COBRO 3DS][" + this.messageId + "]", ex);

			/* 299 */ view = new ModelAndView("payworks/pagina-error");
			/* 300 */ view.addObject("idApp", Integer.valueOf(idApp));
			/* 301 */ if (request.getIdioma().equals("es")) {
				/* 302 */ view.addObject("mensajeError",
						/* 303 */ "Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. Por favor vuelva a intentarlo en unos minutos.");
			} else {

				/* 306 */ view.addObject("mensajeError",
						"Sorry for the inconvenience, we detected an error when authorizing your transaction. Please try again in a few minutes.");
			}
		}

		/* 310 */ return view;
	}

	public ModelAndView payworksRec3DRespuesta(String cadena, ModelMap modelo) {
		/* 316 */ HashMap<String, Object> resp = new HashMap<>();
		/* 317 */ ModelAndView mav = null;
		/* 318 */ int idapp = 1;
		try {
			/* 320 */ cadena = cadena.replace(" ", "+");
			/* 321 */ LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			/* 322 */ String[] spl = cadena.split("&");
			/* 323 */ String[] data = null;
			byte b;
			int i;
			String[] arrayOfString1;
			/* 324 */ for (i = (arrayOfString1 = spl).length, b = 0; b < i;) {
				String cad = arrayOfString1[b];
				/* 325 */ LOGGER.debug("data:  " + cad);
				/* 326 */ data = cad.split("=");
				/* 327 */ resp.put(data[0], (data.length >= 2) ? data[1] : "");

				b++;
			}

			/* 332 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			/* 333 */ bitacora_banorte.setId_bitacora(Long.parseLong((String) resp.get("Reference3D")));
			/* 334 */ bitacora_banorte.setTexto(
					(resp.get("Status") != null) ? (String) Constantes.errorPayW.get(resp.get("Status")) : "");
			/* 335 */ bitacora_banorte.setReferencia("");
			/* 336 */ bitacora_banorte.setResultado_payw("");
			/* 337 */ bitacora_banorte.setResultado_aut("");
			/* 338 */ bitacora_banorte.setBanco_emisor("");
			/* 339 */ bitacora_banorte.setTipo_tarjeta("");
			/* 340 */ bitacora_banorte.setMarca_tarjeta((String) resp.get("CardType"));
			/* 341 */ bitacora_banorte.setCodigo_aut("");
			/* 342 */ bitacora_banorte.setBank_reference("");
			/* 343 */ bitacora_banorte.setTransaction_number(0L);
			/* 344 */ bitacora_banorte.setStatus3DS((resp.get("Status") != null) ? (String) resp.get("Status") : "");

			/* 346 */ this.mapper.updateBitacoraBanorteOBJ(bitacora_banorte);

			/* 348 */ TBitacoraBanorte bitacoraBanorte = this.mapper
					.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
			/* 349 */ idapp = bitacoraBanorte.getId_aplicacion();

			/* 351 */ if (resp.containsKey("Status") && "200".equals(resp.get("Status")) && resp.get("XID") != null
					&& !((String) resp.get("XID")).isEmpty() && resp.get("CAVV") != null
					&& !((String) resp.get("CAVV")).isEmpty()) {
				/* 352 */ AfiliacionVO afiliacion = this.mapper.buscaAfiliacion("BANORTEH2HN");
				/* 353 */ LOGGER.debug("idBitacora " + resp.get("Reference3D").toString());

				/* 355 */ Card card = this.mapper.getCard(bitacoraBanorte.getId_usuario(), bitacoraBanorte.getId_card(),
						bitacoraBanorte.getId_aplicacion());

				/* 358 */ mav = new ModelAndView("payworks/comercioPAYW2");
				/* 359 */ mav.addObject("payworks", resp);
				/* 360 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));

				/* 364 */ resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				/* 365 */ resp.put("USUARIO", afiliacion.getUsuario());
				/* 366 */ resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				/* 367 */ resp.put("CMD_TRANS", "VENTA");
				/* 368 */ resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				/* 369 */ resp.put("Total", Business.formatoMontoPayworks((String) resp.get("Total")));
				/* 370 */ resp.put("MODO", "PRD");
				/* 371 */ resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				/* 372 */ resp.put("NUMERO_TARJETA", resp.get("Number"));
				/* 373 */ resp.put("FECHA_EXP", ((String) resp.get("Expires")).replaceAll("%2F", ""));
				/* 374 */ resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt()));
				/* 375 */ resp.put("MODO_ENTRADA", "MANUAL");
				/* 376 */ resp.put("URL_RESPUESTA", afiliacion.getForwardPath().split(" ")[1]);
				/* 377 */ resp.put("IDIOMA_RESPUESTA", "ES");
				/* 378 */ resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt()));
				/* 379 */ resp.put("ESTATUS_3D", "");

				/* 381 */ if (resp.get("XID") != null) {
					/* 382 */ resp.put("XID", ((String) resp.get("XID")).replaceAll("%3D", "="));
					/* 383 */ resp.put("CAVV", ((String) resp.get("CAVV")).replaceAll("%3D", "="));
				}

				/* 386 */ LOGGER.info("DATOS ENVIADO A PAYWORK2: " + resp.toString());

				/* 388 */ return sendAuthorizationRequestPayworks(resp, Integer.valueOf(idapp));
			}

			/* 391 */ LOGGER.debug("[PAGO RECHAZADO]");

			/* 393 */ mav = new ModelAndView("payworks/pagina-error");
			/* 394 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));

			/* 397 */ if (resp.get("XID") == null || ((String) resp.get("XID")).isEmpty() || resp.get("CAVV") == null
					|| ((String) resp.get("CAVV")).isEmpty()) {

				/* 399 */ mav.addObject("mensajeError", "El pago fue rechazado. " + (
				/* 400 */ (resp.get("Status") != null) ? ("Clave: " + resp.get("Status")) : "") + ", Descripcion: "
						+ "Su transacción no pudo ser procesada, porque su tarjeta no pudo ser autenticada bajo el programa 3dSecure");
			} else {

				/* 403 */ mav.addObject("mensajeError", "El pago fue rechazado. " + (
				/* 404 */ (resp.get("Status") != null) ? ("Clave: " + resp.get("Status")) : "") + ", Descripcion: "
						+ (String) Constantes.errorPayW.get(resp.get("Status")));
			}

		}
		/* 408 */ catch (Exception ex) {
			/* 409 */ LOGGER.error("ERROR AL PROCESAR RESPUESTA 3DS BANORTE " + cadena, ex);
			/* 410 */ LOGGER.error("ERROR ", ex);
			/* 411 */ mav = new ModelAndView("payworks/pagina-error");
			/* 412 */ mav.addObject("idApp", Integer.valueOf(idapp));
			/* 413 */ mav.addObject("mensajeError", "Error inesperado, vuelva a intertarlo");
		}

		/* 417 */ return mav;
	}

	private ModelAndView payDummy(int idApp, long idBitacora, Double amount, Double comision) {
		/* 421 */ ModelAndView mav = new ModelAndView("payworks/exito");
		/* 422 */ SecureRandom sr = new SecureRandom();
		/* 423 */ sr.nextBytes(new byte[1]);
		/* 424 */ String referencia = (new StringBuilder(String.valueOf(sr.nextInt() + 10000))).toString();
		/* 425 */ String codAut = (new StringBuilder(String.valueOf(sr.nextInt() + 10000))).toString();

		/* 427 */ mav.addObject("mensajeError",
				"<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/><tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+

						/* 429 */ this.DFormat.format(new Date()) + "</td></tr>" +
						/* 430 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+ referencia + "</td></tr>" +

						/* 432 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+ codAut + "</td></tr>" +
						/* 433 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+ idBitacora + "</td></tr>" +
						/* 434 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
						+ amount + "</td></tr>" +
						/* 435 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
						+ comision + "</td></tr>" +
						/* 436 */ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>");

		/* 438 */ mav.addObject("idApp", Integer.valueOf(idApp));
		/* 439 */ return mav;
	}

	public ModelAndView payworks2RecRespuesta(String NUMERO_CONTROL, String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW, String FECHA_REQ_CTE, String CODIGO_AUT, String CODIGO_PAYW,
			String RESULTADO_AUT, String BANCO_EMISOR, String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		/* 446 */ ModelAndView mav = new ModelAndView("payworks/exito");

		/* 448 */ int idapp = 1;
		try {
			/* 450 */ LOGGER
					.debug("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL + " REFERENCIA: " + REFERENCIA +
					/* 451 */ " FECHA_RSP_CTE: " + FECHA_RSP_CTE + " TEXTO: " + TEXTO + " RESULTADO_AUT: "
							+ RESULTADO_AUT +
							/* 452 */ " RESULTADO_PAYW: " + RESULTADO_PAYW + " FECHA_REQ_CTE: " + FECHA_REQ_CTE
							+ " CODIGO_PAYW: " + CODIGO_PAYW +
							/* 453 */ " CODIGO_PAYW: " + CODIGO_AUT + " BANCO_EMISOR: " + BANCO_EMISOR
							+ " TIPO_TARJETA: " + TIPO_TARJETA +
							/* 454 */ " MARCA_TARJETA: " + MARCA_TARJETA);
			/* 455 */ TransaccionBanorte TBanorte = new TransaccionBanorte();
			/* 456 */ TBanorte.setBANCO_EMISOR(BANCO_EMISOR);
			/* 457 */ TBanorte.setCODIGO_AUT(CODIGO_AUT);
			/* 458 */ TBanorte.setCODIGO_PAYW(CODIGO_PAYW);
			/* 459 */ TBanorte.setFECHA_REQ_CTE(FECHA_REQ_CTE);
			/* 460 */ TBanorte.setFECHA_RSP_CTE(FECHA_RSP_CTE);
			/* 461 */ TBanorte.setID_AFILIACION(ID_AFILIACION);
			/* 462 */ TBanorte.setMARCA_TARJETA(MARCA_TARJETA);
			/* 463 */ TBanorte.setNUMERO_CONTROL(NUMERO_CONTROL);
			/* 464 */ TBanorte.setREFERENCIA(REFERENCIA);
			/* 465 */ TBanorte.setRESULTADO_AUT(RESULTADO_AUT);
			/* 466 */ TBanorte.setRESULTADO_PAYW(RESULTADO_PAYW);
			/* 467 */ TBanorte.setTEXTO(TEXTO);
			/* 468 */ TBanorte.setTIPO_TARJETA(TIPO_TARJETA);
			/* 469 */ this.mapper.insertaTransactionBanorte(TBanorte);

			/* 471 */ long id_bitacora = Long.parseLong(NUMERO_CONTROL);
			/* 472 */ TBitacoraBanorte bitacoraBanorte = this.mapper.getBitacoraBanorte(id_bitacora);

			/* 474 */ idapp = bitacoraBanorte.getId_aplicacion();
			/* 475 */ User user = this.mapper.getUserData(bitacoraBanorte.getId_usuario(),
					bitacoraBanorte.getId_aplicacion());
			/* 476 */ String ReferenceBanorte = "0";
			/* 477 */ String status3DS = bitacoraBanorte.getStatus3DS();

			/* 479 */ if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {

				/* 481 */ PaymentRequest paymentRequest = new PaymentRequest();
				/* 482 */ paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
				/* 483 */ paymentRequest.setAmount(bitacoraBanorte.getAmount());
				/* 484 */ paymentRequest.setConcept("Transferencia");
				/* 485 */ paymentRequest.setIdCard(bitacoraBanorte.getId_card());
				/* 486 */ paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
				/* 487 */ paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
				/* 488 */ paymentRequest.setAuthProcom(CODIGO_AUT);
				/* 489 */ paymentRequest.setRefProcom(REFERENCIA);
				/* 490 */ mav = EnqueuePayment(paymentRequest, id_bitacora, bitacoraBanorte.getId_aplicacion());

				/* 493 */ MCTransafersResponse responseTransfer = (MCTransafersResponse) this.gson
						.fromJson((String) mav.getModel().get("json"), MCTransafersResponse.class);

				/* 495 */ if (responseTransfer.getIdError() == 0) {

					/* 497 */ mav.addObject("mensajeError",
							"<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/><tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+

									/* 499 */ this.DFormat.format(new Date()) + "</td></tr>" +
									/* 500 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+ REFERENCIA + "</td></tr>" +

									/* 502 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+ CODIGO_AUT + "</td></tr>" +
									/* 503 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+ NUMERO_CONTROL + "</td></tr>" +
									/* 504 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
									+ bitacoraBanorte.getAmount() + "</td></tr>" +
									/* 505 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
									+ bitacoraBanorte.getComision() + "</td></tr>" +
									/* 506 */ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001."
									+ "</td></tr>");

					/* 508 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));

					/* 510 */ HashMap<String, String> datos = new HashMap<>();
					/* 511 */ datos.put("NOMBRE", user.getUsr_nombre());
					/* 512 */ datos.put("FECHA", this.DFormat.format(new Date()));
					/* 513 */ datos.put("AUTBAN", CODIGO_AUT);
					/* 514 */ datos.put("IMPORTE",
							(new StringBuilder(String.valueOf(bitacoraBanorte.getAmount()))).toString());
					/* 515 */ datos.put("COMISION",
							(new StringBuilder(String.valueOf(bitacoraBanorte.getComision()))).toString());

					/* 517 */ ProjectMC project = this.mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");

					/* 519 */ Business.SendMailMicrosoft(user.geteMail(),
							this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_ES"),
							this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
				} else {

					/* 522 */ String msg = "Ocurrio un problema al realizar transferencia<br><br>"
							+ responseTransfer.getMensajeError() +
							/* 523 */ "<br>Para aclaraciones marque 01-800-925-5001.";
					/* 524 */ mav = new ModelAndView("payworks/pagina-error");
					/* 525 */ mav.addObject("mensajeError", msg);
					/* 526 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));
				}

				/* 531 */ status3DS = "200";

			} else {

				/* 538 */ String msg =

						/* 540 */ String
								.valueOf("D".equalsIgnoreCase(RESULTADO_PAYW) ? "DECLINADA"
										: ("R".equalsIgnoreCase(RESULTADO_PAYW) ? "RECHAZADA"
												: ("T".equalsIgnoreCase(RESULTADO_PAYW)
														? "Sin respuesta del autorizador"
														: "Repuesta no Valida")))
								+
								/* 541 */ ": " + TEXTO;
				/* 542 */ LOGGER.debug("[Mensaje Banorte] " + msg);

				/* 544 */ msg = "El pago fue " + (
				/* 545 */ "D".equalsIgnoreCase(RESULTADO_PAYW) ? "DECLINADO" : (
				/* 546 */ "R".equalsIgnoreCase(RESULTADO_PAYW) ? "RECHAZADO" : (
				/* 547 */ "T".equalsIgnoreCase(RESULTADO_PAYW) ? "Sin respuesta del autorizador"
						: "Repuesta no Valida"))) + BANCO_EMISOR + ", " + TIPO_TARJETA +
				/* 548 */ " . " + ((CODIGO_PAYW != null) ? ("Clave: " + CODIGO_PAYW)
						: ((RESULTADO_AUT != null) ? RESULTADO_AUT : "")) + ", Descripcion: " + TEXTO;
				/* 549 */ mav = new ModelAndView("payworks/pagina-error");
				/* 550 */ mav.addObject("mensajeError", msg);
				/* 551 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));
			}

			/* 555 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			/* 556 */ bitacora_banorte.setId_bitacora(id_bitacora);
			/* 557 */ bitacora_banorte.setTexto(TEXTO);
			/* 558 */ bitacora_banorte.setReferencia(REFERENCIA);
			/* 559 */ bitacora_banorte.setResultado_payw(RESULTADO_PAYW);
			/* 560 */ bitacora_banorte.setResultado_aut(RESULTADO_AUT);
			/* 561 */ bitacora_banorte.setBanco_emisor(BANCO_EMISOR);
			/* 562 */ bitacora_banorte.setTipo_tarjeta(TIPO_TARJETA);
			/* 563 */ bitacora_banorte.setMarca_tarjeta(MARCA_TARJETA);
			/* 564 */ bitacora_banorte.setCodigo_aut(CODIGO_AUT);
			/* 565 */ bitacora_banorte.setBank_reference(ReferenceBanorte);
			/* 566 */ bitacora_banorte.setTransaction_number(Long.parseLong(ReferenceBanorte));
			/* 567 */ bitacora_banorte.setStatus3DS("200");

			/* 569 */ this.mapper.updateBitacoraBanorteOBJ(bitacora_banorte);

		}
		/* 572 */ catch (Exception ex) {
			/* 573 */ LOGGER.error("[ERROR AL PROCESAR RESPUESTA BANORTE payworks2RecRespuesta] " + NUMERO_CONTROL, ex);
			/* 574 */ mav = new ModelAndView("payworks/pagina-error");
			/* 575 */ mav.addObject("idApp", Integer.valueOf(idapp));
			/* 576 */ mav.addObject("mensajeError",
					/* 577 */ "Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. Por favor vuelva a intentarlo en unos minutos.");
		}

		/* 580 */ return mav;
	}

	public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora, int idApp) {
		/* 588 */ EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
		/* 589 */ EnqueueTransactionResponse response = new EnqueueTransactionResponse();
		/* 590 */ MCTransafersResponse responseTransfer = new MCTransafersResponse();
		/* 591 */ ModelAndView view = new ModelAndView("payworks/exito");
		/* 592 */ McBaseResponse McResponse = new McBaseResponse();
		try {
			/* 594 */ this.login = login();
			/* 595 */ TBitacoraVO bitacora = new TBitacoraVO();

			/* 598 */ responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
			/* 599 */ responseTransfer.setComision(1.0D);
			/* 600 */ responseTransfer.setFecha(this.DFormat.format(new Date()));
			/* 601 */ responseTransfer.setMontoTransfer(paymentRequest.getAmount());

			/* 605 */ User user = this.mapper.getUserData(paymentRequest.getIdUser(), idApp);
			/* 606 */ Card card = this.mapper.getCard(paymentRequest.getIdUser(), paymentRequest.getIdCard(), idApp);
			/* 607 */ AccountRecord fromAccount = new AccountRecord();

			/* 609 */ responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));

			/* 611 */ fromAccount.setActNumber("0276948693");
			/* 612 */ fromAccount.setActType("ACT");
			/* 613 */ fromAccount.setBankCode("072");
			/* 614 */ fromAccount.setContact("Jorge Silva Gallegos");
			/* 615 */ fromAccount.setEmail("jorge@mobilecardmx.com");
			/* 616 */ fromAccount.setHolderName("ADCEL SA DE CV");
			/* 617 */ fromAccount.setPhone("5555403124");
			/* 618 */ fromAccount.setRfc("ADC090715DW3");

			/* 620 */ AccountRecord toAccount = this.mapper.getAccount(paymentRequest.getAccountId());

			/* 627 */ payment.setAmount(paymentRequest.getAmount());
			/* 628 */ payment.setClientReference((new StringBuilder(String.valueOf(idBitacora))).toString());
			/* 629 */ payment.setDescription(paymentRequest.getConcept());
			/* 630 */ payment.setFromAccount(fromAccount);
			/* 631 */ payment.setToAccount(toAccount);
			/* 632 */ payment.setToken(this.login.getToken());
			/* 633 */ response = this.h2hclient.EnqueuePayment(payment, paymentRequest.getIdioma());

			/* 635 */ LOGGER.debug("[RESPUESTA PAGO] " + this.gson.toJson(response));

			/* 637 */ if (response.getResult().isSuccess()) {

				/* 639 */ LOGGER.debug("[Transaccion guardada] Exito");
				/* 640 */ this.mapper.updateTbitacora(idBitacora, 1, paymentRequest.getAuthProcom());
				/* 641 */ HashMap<String, String> datos = new HashMap<>();
				/* 642 */ datos.put("NOMBRE", toAccount.getContact());
				/* 643 */ datos.put("FECHA", this.DFormat.format(new Date()));

				/* 645 */ datos.put("IMPORTE", (new StringBuilder(String.valueOf(payment.getAmount()))).toString());
				/* 646 */ datos.put("CUENTA", toAccount.getActNumber());

				/* 648 */ ProjectMC project = this.mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");

				/* 650 */ Business.SendMailMicrosoft(user.geteMail(),
						this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_DEST_ES"),
						this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());

			} else {

				/* 657 */ LOGGER.debug("[Transaccion guardada] error al cobrar");
				/* 658 */ this.mapper.updateTbitacora(idBitacora, 2, "");
			}

			/* 662 */ responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 99);
			/* 663 */ responseTransfer.setMensajeError(
					(response.getResult().getMessage() == null) ? "" : response.getResult().getMessage());
			/* 664 */ responseTransfer.setReferenceBanorte(response.getTransactionReference());
			/* 665 */ responseTransfer.setRefProcom(paymentRequest.getRefProcom());

			/* 667 */ McResponse.setIdError(response.getResult().isSuccess() ? 0 : 99);
			/* 668 */ McResponse.setMensajeError(
					(response.getResult().getMessage() == null) ? "" : response.getResult().getMessage());

			/* 670 */ view.addObject("json", this.gson.toJson(responseTransfer));
			/* 671 */ view.addObject("idApp", Integer.valueOf(idApp));
			/* 672 */ return view;
			/* 673 */ } catch (Exception ex) {

			/* 675 */ LOGGER.error("[ERROR AL ENCOLAR PAGO] ", ex);
			/* 676 */ responseTransfer.setIdError(99);
			/* 677 */ responseTransfer.setMensajeError("Error al procesar Transaferencia");
			/* 678 */ view.addObject("json", this.gson.toJson(responseTransfer));
			/* 679 */ view.addObject("idApp", Integer.valueOf(idApp));
			/* 680 */ return view;
		} finally {
			/* 682 */ logout();
		}
	}

	private ModelAndView SendAmex(User user, Card card, PaymentRequest request, int idApp, Proveedor proveedor,
			AfiliacionVO afiliacion, AccountRecord toAccount, double lat, double lon) {
		/* 689 */ ModelAndView mav = new ModelAndView("payworks/exito");

		try {
			/* 692 */ double comision = request.getComision();
			/* 693 */ BigDecimal bd = new BigDecimal(comision);
			/* 694 */ bd = bd.setScale(2, RoundingMode.HALF_DOWN);
			/* 695 */ comision = bd.doubleValue();
			/* 696 */ double total = comision + request.getAmount();

			/* 698 */ TBitacoraVO bitacora = new TBitacoraVO();
			/* 699 */ bitacora.setTipo("");
			/* 700 */ bitacora.setSoftware("");
			/* 701 */ bitacora.setModelo("");
			/* 702 */ bitacora.setWkey("");
			/* 703 */ bitacora.setIdUsuario((new StringBuilder(String.valueOf(request.getIdUser()))).toString());
			/* 704 */ bitacora.setIdProducto(1);
			/* 705 */ bitacora.setCar_id(1);
			/* 706 */ bitacora.setCargo(request.getAmount() + comision);
			/* 707 */ bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			/* 708 */ bitacora.setConcepto("Transferencia H2HBANORTE(" + ((idApp == 1) ? "Mobilecard" : "Telecom")
					+ " AMEX): Concepto: " + request.getConcept() + " Monto: " +
					/* 709 */ request.getAmount() + " Comision: " + comision);
			/* 710 */ bitacora.setImei("");
			/* 711 */ bitacora.setDestino((new StringBuilder(String.valueOf(request.getAccountId()))).toString());
			/* 712 */ bitacora.setTarjeta_compra(card.getNumerotarjeta());
			/* 713 */ bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
			/* 714 */ bitacora.setComision(comision);
			/* 715 */ bitacora.setCx("");
			/* 716 */ bitacora.setCy("");
			/* 717 */ bitacora.setPin("");
			/* 718 */ bitacora.setId_aplicacion(idApp);
			/* 719 */ this.mapper.addBitacora(bitacora);

			/* 721 */ TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			/* 722 */ bitacora_banorte.setAccount_id(request.getAccountId());
			/* 723 */ bitacora_banorte.setAmount(request.getAmount());
			/* 724 */ bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			/* 725 */ bitacora_banorte.setId_card(request.getIdCard());
			/* 726 */ bitacora_banorte.setId_usuario(request.getIdUser());
			/* 727 */ bitacora_banorte.setIdioma(request.getIdioma());
			/* 728 */ bitacora_banorte.setTransaction_number(0L);
			/* 729 */ bitacora_banorte.setBank_reference("");
			/* 730 */ bitacora_banorte.setComision(comision);
			/* 731 */ bitacora_banorte.setTexto("");
			/* 732 */ bitacora_banorte.setReferencia("");
			/* 733 */ bitacora_banorte.setResultado_payw("");
			/* 734 */ bitacora_banorte.setResultado_aut("");
			/* 735 */ bitacora_banorte.setBanco_emisor("");
			/* 736 */ bitacora_banorte.setTipo_tarjeta("");
			/* 737 */ bitacora_banorte.setMarca_tarjeta("");
			/* 738 */ bitacora_banorte.setCodigo_aut("");
			/* 739 */ bitacora_banorte.setStatus3DS("");
			/* 740 */ bitacora_banorte.setConcepto(request.getConcept());
			/* 741 */ bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			/* 742 */ bitacora_banorte.setNombre_destino(toAccount.getHolderName());
			/* 743 */ bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
			/* 744 */ bitacora_banorte.setBanco_destino(toAccount.getBankCode());
			/* 745 */ bitacora_banorte.setId_aplicacion(idApp);
			/* 746 */ bitacora_banorte.setLat(lat);
			/* 747 */ bitacora_banorte.setLon(lon);

			/* 749 */ this.mapper.insertTbitacoraMCBanorte(bitacora_banorte);

			/* 753 */ AmexAutorization pago = new AmexAutorization();
			/* 754 */ pago.setAfiliacion("9353192983");
			/* 755 */ pago.setComision(comision);
			/* 756 */ pago.setCvv2(UtilsService.getSMS(card.getCt()));
			/* 757 */ pago.setDom_amex((card.getUsrDomAmex() == null) ? "" : card.getUsrDomAmex());
			/* 758 */ pago.setMonto(total);
			/* 759 */ pago.setProducto("TransfersH2H");
			/* 760 */ pago.setTarjeta(UtilsService.getSMS(card.getNumerotarjeta()));
			/* 761 */ pago.setVigencia(UtilsService.getSMS(card.getVigencia()));

			/* 764 */ RespuestaAmex respAmex = this.amexService.solicitaAutorizacionAmex(pago);

			/* 766 */ if (respAmex.getCode().equals("000")) {
				/* 768 */ PaymentRequest paymentRequest = new PaymentRequest();
				/* 769 */ paymentRequest.setAccountId(request.getAccountId());
				/* 770 */ paymentRequest.setAmount(request.getAmount());
				/* 771 */ paymentRequest.setConcept("Transferencia");
				/* 772 */ paymentRequest.setIdCard(request.getIdCard());
				/* 773 */ paymentRequest.setIdioma(request.getIdioma());
				/* 774 */ paymentRequest.setIdUser(request.getIdUser());
				/* 775 */ paymentRequest.setAuthProcom(respAmex.getTransaction());
				/* 776 */ paymentRequest.setRefProcom(respAmex.getTransaction());
				/* 777 */ mav = EnqueuePayment(paymentRequest, bitacora.getIdBitacora(), idApp);

				/* 779 */ MCTransafersResponse responseTransfer = (MCTransafersResponse) this.gson
						.fromJson((String) mav.getModel().get("json"), MCTransafersResponse.class);
				/* 780 */ if (responseTransfer.getIdError() == 0) {

					/* 782 */ mav.addObject("mensajeError",
							"<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/><tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+

									/* 784 */ this.DFormat.format(new Date()) + "</td></tr>" +

									/* 787 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+ respAmex.getTransaction() + "</td></tr>" +
									/* 788 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
									+ bitacora.getIdBitacora() + "</td></tr>" +
									/* 789 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
									+ request.getAmount() + "</td></tr>" +
									/* 790 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
									+ request.getComision() + "</td></tr>" +
									/* 791 */ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001."
									+ "</td></tr>");

					/* 793 */ mav.addObject("idApp", Integer.valueOf(idApp));

					/* 795 */ HashMap<String, String> datos = new HashMap<>();
					/* 796 */ datos.put("NOMBRE", user.getUsr_nombre());
					/* 797 */ datos.put("FECHA", this.DFormat.format(new Date()));
					/* 798 */ datos.put("AUTBAN", respAmex.getTransaction());
					/* 799 */ datos.put("IMPORTE", (new StringBuilder(String.valueOf(request.getAmount()))).toString());
					/* 800 */ datos.put("COMISION", (new StringBuilder(String.valueOf(comision))).toString());

					/* 802 */ ProjectMC project = this.mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");

					/* 804 */ Business.SendMailMicrosoft(user.geteMail(),
							this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_EMAIL_ES"),
							this.mapper.getParameter("@MESSAGE_BANORTE_TRANSFER_SUBJECT_ES"), datos, project.getUrl());
				} else {

					/* 807 */ String msg = "Ocurrio un problema al realizar transferencia<br><br>"
							+ responseTransfer.getMensajeError() +
							/* 808 */ "<br>Para aclaraciones marque 01-800-925-5001.";
					/* 809 */ mav = new ModelAndView("payworks/pagina-error");
					/* 810 */ mav.addObject("mensajeError", msg);
					/* 811 */ mav.addObject("idApp", Integer.valueOf(idApp));
				}

				/* 815 */ bitacora_banorte = new TBitacoraBanorte();
				/* 816 */ bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
				/* 817 */ bitacora_banorte.setTexto("TRANSFERENCIA AMEX");
				/* 818 */ bitacora_banorte.setReferencia(respAmex.getTransaction());
				/* 819 */ bitacora_banorte.setResultado_payw("A");
				/* 820 */ bitacora_banorte.setResultado_aut(respAmex.getTransaction());
				/* 821 */ bitacora_banorte.setBanco_emisor("AMEX");
				/* 822 */ bitacora_banorte.setTipo_tarjeta("AMEX");
				/* 823 */ bitacora_banorte.setMarca_tarjeta("AMEX");
				/* 824 */ bitacora_banorte.setCodigo_aut(respAmex.getTransaction());
				/* 825 */ bitacora_banorte.setBank_reference(responseTransfer.getReferenceBanorte());
				/* 826 */ bitacora_banorte
						.setTransaction_number(Long.parseLong(responseTransfer.getReferenceBanorte()));
				/* 827 */ bitacora_banorte.setStatus3DS("200");
				/* 828 */ this.mapper.updateBitacoraBanorteOBJ(bitacora_banorte);

			} else {

				/* 834 */ String msg = "El pago fue RECHAZADO Descripcion: " + respAmex.getErrorDsc();
				/* 835 */ mav = new ModelAndView("payworks/pagina-error");
				/* 836 */ mav.addObject("mensajeError", msg);
				/* 837 */ mav.addObject("idApp", Integer.valueOf(idApp));
			}

			/* 840 */ } catch (Exception ex) {
			/* 841 */ LOGGER.error("[ERROR AL PROCESAR PAGO AMEX] " + request.getIdUser(), ex);
			/* 842 */ mav = new ModelAndView("payworks/pagina-error");
			/* 843 */ mav.addObject("idApp", Integer.valueOf(idApp));
			/* 844 */ mav.addObject("mensajeError",
					/* 845 */ "Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. Por favor vuelva a intentarlo en unos minutos.");
		}

		/* 849 */ return mav;
	}

	public ModelAndView sendAuthorizationRequestPayworks(HashMap<String, Object> resp, Integer idApp) {
		/* 854 */ RestTemplate restTemplate = new RestTemplate();
		/* 855 */ ModelAndView mav = null;

		try {
			/* 858 */ HttpHeaders headers = new HttpHeaders();
			/* 859 */ HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			/* 860 */ headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			/* 861 */ headers.add("Content-Type", "application/x-www-form-urlencoded");
			/* 862 */ LinkedMultiValueMap linkedMultiValueMap = new LinkedMultiValueMap();
			/* 863 */ linkedMultiValueMap.add("ID_AFILIACION", resp.get("ID_AFILIACION"));
			/* 864 */ linkedMultiValueMap.add("USUARIO", resp.get("USUARIO"));
			/* 865 */ linkedMultiValueMap.add("CLAVE_USR", resp.get("CLAVE_USR"));
			/* 866 */ linkedMultiValueMap.add("CMD_TRANS", "VENTA");
			/* 867 */ linkedMultiValueMap.add("ID_TERMINAL", resp.get("ID_TERMINAL"));
			/* 868 */ linkedMultiValueMap.add("MONTO", resp.get("Total"));
			/* 869 */ linkedMultiValueMap.add("MODO", "PRD");
			/* 870 */ linkedMultiValueMap.add("NUMERO_CONTROL", resp.get("Reference3D"));
			/* 871 */ linkedMultiValueMap.add("NUMERO_TARJETA", resp.get("Number"));
			/* 872 */ linkedMultiValueMap.add("FECHA_EXP", ((String) resp.get("Expires")).replaceAll("%2F", ""));
			/* 873 */ linkedMultiValueMap.add("CODIGO_SEGURIDAD", resp.get("CODIGO_SEGURIDAD"));
			/* 874 */ linkedMultiValueMap.add("MODO_ENTRADA", "MANUAL");
			/* 875 */ linkedMultiValueMap.add("URL_RESPUESTA", resp.get("URL_RESPUESTA"));
			/* 876 */ linkedMultiValueMap.add("IDIOMA_RESPUESTA", "ES");
			/* 877 */ linkedMultiValueMap.add("ESTATUS_3D", "");
			/* 878 */ linkedMultiValueMap.add("ECI", "");
			/* 879 */ if (resp.get("XID") != null && resp.get("CAVV") != null) {
				/* 880 */ linkedMultiValueMap.add("XID", ((String) resp.get("XID")).replaceAll("%3D", "="));
				/* 881 */ linkedMultiValueMap.add("CAVV", ((String) resp.get("CAVV")).replaceAll("%3D", "="));
			}

			/* 884 */ HttpEntity<MultiValueMap<String, String>> request = new HttpEntity(linkedMultiValueMap,
					(MultiValueMap) headers);
			/* 885 */ LOGGER.debug("INFO SEND PAYWORKS: " + linkedMultiValueMap.toString());

			/* 887 */ CloseableHttpClient closeableHttpClient = HttpClientBuilder.create()
//					/* 888 */ .setRedirectStrategy((RedirectStrategy) new Object(this))
					.setRedirectStrategy((RedirectStrategy) new Object())

					/* 897 */ .build();
			/* 898 */ factory.setHttpClient((HttpClient) closeableHttpClient);
			/* 899 */ restTemplate.setRequestFactory((ClientHttpRequestFactory) factory);

			/* 901 */ ResponseEntity<String> response = restTemplate.postForEntity("https://via.pagosbanorte.com/payw2",
					request, String.class, new Object[0]);
			/* 902 */ LOGGER.info("AUTORIZACION SOLICITADA A PAYWORKS ", response.getBody());
			/* 903 */ TBitacoraBanorte bitacoraBanorte = this.mapper
					.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));

			/* 905 */ if (bitacoraBanorte.getResultado_payw() != null
					&& bitacoraBanorte.getResultado_payw().equals("A")) {
				/* 906 */ mav = new ModelAndView("payworks/exito");
				/* 907 */ mav.addObject("mensajeError",
						"<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/><tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
								+

								/* 909 */ this.DFormat.format(new Date()) + "</td></tr>" +
								/* 910 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
								+ bitacoraBanorte.getReferencia() + "</td></tr>" +

								/* 912 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
								+ bitacoraBanorte.getCodigo_aut() + "</td></tr>" +
								/* 913 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
								+ bitacoraBanorte.getId_bitacora() + "</td></tr>" +
								/* 914 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
								+ bitacoraBanorte.getAmount() + "</td></tr>" +
								/* 915 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
								+ bitacoraBanorte.getComision() + "</td></tr>" +
								/* 916 */ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001."
								+ "</td></tr>");

				/* 918 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));
			} else {

				/* 921 */ String msg = "El pago fue " + (
				/* 922 */ "D".equalsIgnoreCase(bitacoraBanorte.getResultado_payw()) ? "DECLINADO " : (
				/* 923 */ "R".equalsIgnoreCase(bitacoraBanorte.getResultado_payw()) ? "RECHAZADO " : (
				/* 924 */ "T".equalsIgnoreCase(bitacoraBanorte.getResultado_payw()) ? "Sin respuesta del autorizador"
						: "Repuesta no Valida "))) + bitacoraBanorte.getBanco_emisor() + ", "
						+ bitacoraBanorte.getTipo_tarjeta() +
						/* 925 */ " . "
						+ ((bitacoraBanorte.getResultado_aut() != null)
								? ("Clave: " + bitacoraBanorte.getResultado_payw())
								: ((bitacoraBanorte.getResultado_aut() != null) ? bitacoraBanorte.getResultado_aut()
										: ""))
						+ ", Descripcion: " + bitacoraBanorte.getTexto();
				/* 926 */ mav = new ModelAndView("payworks/pagina-error");
				/* 927 */ mav.addObject("mensajeError", msg);
				/* 928 */ mav.addObject("idApp", Integer.valueOf(bitacoraBanorte.getId_aplicacion()));

			}

		}
		/* 934 */ catch (Exception ex) {
			/* 935 */ LOGGER.error("ERROR AL ENVIAR PETICION PAYWORKS REST: ", ex);
			/* 936 */ mav = new ModelAndView("payworks/pagina-error");
			/* 937 */ mav.addObject("idApp", idApp);
			/* 938 */ mav.addObject("mensajeError", "Error inesperado, vuelva a intertarlo");
		}

		/* 941 */ return mav;
	}

	public LoginResponse login() {
		try {
			/* 949 */ LoginRequest login = new LoginRequest();
			/* 950 */ login.setLogin("addcel_banorte");
			/* 951 */ login.setPassword("B4N0rt3%");
			/* 952 */ LOGGER.debug("iniciando login");
			/* 953 */ LoginResponse response = this.h2hclient.login(login, "es");
			/* 954 */ LOGGER.debug("LOGUIN: " + response.getToken() + " " + response.getResult().isSuccess() + " "
					+ response.getResult().getMessage());
			/* 955 */ return response;
			/* 956 */ } catch (Exception ex) {
			/* 957 */ LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			/* 958 */ return new LoginResponse();
		}
	}

	public LogoutResponse logout() {
		try {
			/* 964 */ LogoutRequest logout = new LogoutRequest();
			/* 965 */ logout.setLogin("addcel_banorte");
			/* 966 */ logout.setToken(this.login.getToken());
			/* 967 */ LogoutResponse response = this.h2hclient.logout(logout, "es");
			/* 968 */ LOGGER.debug("[RESPUESTA DE LOGOUT]" + response.getResult().isSuccess());
			/* 969 */ return response;
			/* 970 */ } catch (Exception ex) {
			/* 971 */ LOGGER.error("[ERROR EN LOGOUT]", ex);
			/* 972 */ return new LogoutResponse();
		}
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/PayworksH2HService.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */