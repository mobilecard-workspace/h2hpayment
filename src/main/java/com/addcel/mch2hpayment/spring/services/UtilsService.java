package com.addcel.mch2hpayment.spring.services;

import crypto.Crypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UtilsService {
	/* 13 */ private static final Logger logger = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.UtilsService.class);

	private static final String patron = "dd/MM/yyyy";

	private static final String TelefonoServicio = "5525963513";

	public static String setSMS(String Telefono) {
		/* 23 */ return Crypto.aesEncrypt(parsePass("5525963513"), Telefono);
	}

	public static String getSMS(String Telefono) {
		/* 27 */ return Crypto.aesDecrypt(parsePass("5525963513"), Telefono);
	}

	public static String parsePass(String pass) {
		/* 31 */ int len = pass.length();
		/* 32 */ String key = "";

		/* 34 */ for (int i = 0; i < 32 / len; i++) {
			/* 35 */ key = String.valueOf(key) + pass;
		}

		/* 38 */ int carry = 0;
		/* 39 */ while (key.length() < 32) {
			/* 40 */ key = String.valueOf(key) + pass.charAt(carry);
			/* 41 */ carry++;
		}
		/* 43 */ return key;
	}

	public static boolean isEmpty(String cadena) {
		/* 48 */ boolean resp = false;
		/* 49 */ if (cadena == null) {
			/* 50 */ resp = true;
			/* 51 */ } else if ("".equals(cadena)) {
			/* 52 */ resp = true;
		}
		/* 54 */ return resp;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/UtilsService.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */