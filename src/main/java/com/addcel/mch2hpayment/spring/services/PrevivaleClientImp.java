package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AddMoneyReq;
import com.addcel.mch2hpayment.client.model.AddMoneyRes;
import com.addcel.mch2hpayment.client.model.PrevivaleClient;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class PrevivaleClientImp implements PrevivaleClient {
	/* 18 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.PrevivaleClientImp.class);

	/* 20 */ private static String SERVER = "http://localhost:80/";
	/* 21 */ private static final String URL_AGREGA_DINERO = String.valueOf(SERVER)
			+ "PreviVale/api/@idApp/@idPais/@idioma/agregarDinero";

	/* 23 */ private Gson gson = new Gson();
	/* 24 */ private RestTemplate restTemplate = new RestTemplate();

	public AddMoneyRes agregarDinero(AddMoneyReq req, int idApp, int idPais, String idioma) {
		try {
			/* 32 */ HttpHeaders headers = new HttpHeaders();
			/* 33 */ HttpEntity<AddMoneyReq> request = new HttpEntity(req, (MultiValueMap) headers);

			/* 35 */ LOGGER.debug("REQUEST  PREVIVALE: " + this.gson.toJson(req));
			/* 36 */ AddMoneyRes response = (AddMoneyRes) this.restTemplate.postForObject(
					URL_AGREGA_DINERO.replace("@idApp", (new StringBuilder(String.valueOf(idApp))).toString())
							.replace("@idPais", (new StringBuilder(String.valueOf(idApp))).toString())
							.replace("@idioma", idioma),
					request, AddMoneyRes.class, new Object[0]);
			/* 37 */ LOGGER.debug("RESPONSE PREVIVALE: " + this.gson.toJson(response));

			/* 39 */ return response;
			/* 40 */ } catch (Exception ex) {
			/* 41 */ LOGGER.error("ERROR AL AGREGAR DINERO", ex);
			/* 42 */ return new AddMoneyRes(Integer.valueOf(-1));
		}
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/PrevivaleClientImp.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */