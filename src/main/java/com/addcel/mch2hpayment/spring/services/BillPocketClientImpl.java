package com.addcel.mch2hpayment.spring.services;

import com.addcel.mch2hpayment.client.model.AutorizationManualRequest;
import com.addcel.mch2hpayment.client.model.BillPocketClient;
import com.addcel.mch2hpayment.client.model.BillPocketResponse;
import com.addcel.mch2hpayment.client.model.RefundReq;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class BillPocketClientImpl implements BillPocketClient {
	/* 23 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.services.BillPocketClientImpl.class);

	private static final String PATH_AUTHORIZATION_MANUAL = "http://localhost/BillPocket/{idApp}/{idPais}/{idioma}/authorization/manual";

	private static final String PATH_REFUND = "http://localhost/BillPocket/{idApp}/{idPais}/{idioma}/refund";

	/* 30 */ private Gson GSON = new Gson();

	public BillPocketResponse AutorizationManual(AutorizationManualRequest requestManual, Integer idApp, Integer idPais,
			String idioma) {
		/* 35 */ BillPocketResponse response = null;

		try {
			/* 38 */ RestTemplate restTemplate = new RestTemplate();
			/* 39 */ HttpHeaders headers = new HttpHeaders();
			/* 40 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 41 */ HttpEntity<AutorizationManualRequest> request = new HttpEntity(requestManual,
					(MultiValueMap) headers);

			/* 43 */ ResponseEntity<String> respBP = restTemplate
					.exchange(
							"http://localhost/BillPocket/{idApp}/{idPais}/{idioma}/authorization/manual"
//									.replace("{idApp}", (CharSequence) idApp).replace("{idPais}", (CharSequence) idPais)
									.replace("{idApp}", (CharSequence) idApp.toString()).replace("{idPais}", (CharSequence) idPais.toString())
									.replace("{idioma}", idioma),
							HttpMethod.POST, request, String.class, new Object[0]);
			/* 44 */ LOGGER.info("RESPUESTA DE BILL POCKET - {}", respBP.getBody());
			/* 45 */ response = (BillPocketResponse) this.GSON.fromJson((String) respBP.getBody(),
					BillPocketResponse.class);
			/* 46 */ } catch (Exception ex) {
			/* 47 */ LOGGER.error("ERROR AL MOMENTO DE ENVIAR PETICON BILLPOCKET", ex);
		}

		/* 50 */ return response;
	}

	public BillPocketResponse Refund(long idTransaccion, Integer idApp, Integer idPais, String idioma) {
		/* 56 */ BillPocketResponse response = new BillPocketResponse();
		try {
			/* 58 */ LOGGER.debug("INICIANDO PROCESO DE REEMBOLSO {}", Long.valueOf(idTransaccion));
			/* 59 */ RestTemplate restTemplate = new RestTemplate();

			/* 61 */ RefundReq req = new RefundReq();
			/* 62 */ req.setIdTransaccion(idTransaccion);

			/* 64 */ HttpHeaders headers = new HttpHeaders();
			/* 65 */ headers.setContentType(MediaType.APPLICATION_JSON);

			/* 67 */ UriComponentsBuilder uriBuilder = UriComponentsBuilder
					.fromHttpUrl("http://localhost/BillPocket/{idApp}/{idPais}/{idioma}/refund"
							.replace("{idApp}", (CharSequence) idApp.toString()).replace("{idPais}", (CharSequence) idPais.toString())
							.replace("{idioma}", idioma))
					/* 68 */ .queryParam("idTransaccion", new Object[] { Long.valueOf(idTransaccion) });

			/* 71 */ HttpEntity<RefundReq> request = new HttpEntity((MultiValueMap) headers);

			/* 73 */ ResponseEntity<String> respBP = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET,
					request, String.class, new Object[0]);
			/* 74 */ LOGGER.info("RESPUESTA DE BILL POCKET REFUND - {}", respBP.getBody());
			/* 75 */ response = (BillPocketResponse) this.GSON.fromJson((String) respBP.getBody(),
					BillPocketResponse.class);
		}
		/* 77 */ catch (Exception ex) {
			/* 78 */ LOGGER.error("ERROR AL REEMBOLSAR MONTO BILLPOCKET", ex);
			/* 79 */ response.setCode(Integer.valueOf(5000));
			/* 80 */ if (idioma.equals("es")) {
				/* 81 */ response.setMessage("Error al procesar reembolso");
			} else {
				/* 83 */ response.setMessage("Error processing refund");
			}
		}
		/* 86 */ return response;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/services/BillPocketClientImpl.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */