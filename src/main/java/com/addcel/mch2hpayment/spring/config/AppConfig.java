package com.addcel.mch2hpayment.spring.config;

import javax.sql.DataSource;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.addcel.mch2hpayment.spring" })
@MapperScan({ "com.addcel.mch2hpayment.mybatis.model.mapper" })
@PropertySource({ "classpath:properties/datasource.properties" })
public class AppConfig extends WebMvcConfigurerAdapter {
	/* 44 */ public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER = (X509HostnameVerifier) new AllowAllHostnameVerifier();

	@Value("${jdbc.url}")
	private String jdbcUrl;

	@Value("${jdbc.driverClassName}")
	private String driverClassName;

	@Value("${jdbc.username}")
	private String username;

	@Value("${jdbc.password}")
	private String password;

	public DataSource dataSource() {
		/* 61 */ JndiObjectFactoryBean dsource = new JndiObjectFactoryBean();
		/* 62 */ dsource.setJndiName("java:/MobilecardDS");
		/* 63 */ dsource.setExpectedType(DataSource.class);
		try {
			/* 65 */ dsource.afterPropertiesSet();
			/* 66 */ } catch (IllegalArgumentException | javax.naming.NamingException e) {
			/* 67 */ e.printStackTrace();
			/* 68 */ throw new RuntimeException(e);
		}
		/* 70 */ return (DataSource) dsource.getObject();
	}

	@Bean
	public DataSourceTransactionManager transactionManager() {
		/* 81 */ return new DataSourceTransactionManager(dataSource());
	}

	@Bean
	public SqlSessionFactoryBean sqlSessionFactoryBean() throws Exception {
		/* 90 */ SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		/* 91 */ sessionFactory.setDataSource(dataSource());

		/* 93 */ sessionFactory
				.setMapperLocations((new PathMatchingResourcePatternResolver()).getResources("classpath:mapper/*.xml"));
		/* 94 */ sessionFactory.setTypeAliasesPackage("com.addcel.mch2hpayment.mybatis.model.vo");

		/* 96 */ return sessionFactory;
	}

	@Bean
	public InternalResourceViewResolver jspViewResolver() {
		/* 102 */ InternalResourceViewResolver bean = new InternalResourceViewResolver();
		/* 103 */ bean.setPrefix("/WEB-INF/views/");
		/* 104 */ bean.setSuffix(".jsp");
		/* 105 */ return bean;
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		/* 116 */ registry.addResourceHandler(new String[] { "/css/**" })
				.addResourceLocations(new String[] { "/resources/core/style/" });
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/config/AppConfig.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */