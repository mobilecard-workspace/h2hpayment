package com.addcel.mch2hpayment.spring.config;

import com.addcel.mch2hpayment.spring.config.AppConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	protected Class[] getRootConfigClasses() {
		/* 11 */ return new Class[] { AppConfig.class };
	}

	protected Class[] getServletConfigClasses() {
		/* 16 */ return null;
	}

	protected String[] getServletMappings() {
		/* 21 */ return new String[] { "/" };
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/config/AppInitializer.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */