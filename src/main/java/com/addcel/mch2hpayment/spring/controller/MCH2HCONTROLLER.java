package com.addcel.mch2hpayment.spring.controller;

import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.spring.services.MCH2HSERVICE;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MCH2HCONTROLLER {
	/* 37 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.controller.MCH2HCONTROLLER.class);

	@Autowired
	MCH2HSERVICE mcH2HService;

	@RequestMapping(value = { "/pagina-error" }, method = { RequestMethod.GET })
	public ModelAndView pagina_error(@RequestParam("idApp") int idApp) {
		/* 45 */ ModelAndView view = new ModelAndView("payworks/pagina-error");
		/* 46 */ view.addObject("mensajeError", "Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
		/* 47 */ view.addObject("idApp", Integer.valueOf(idApp));
		/* 48 */ return view;
	}

	@RequestMapping(value = { "/payw2" }, method = { RequestMethod.GET })
	public ModelAndView payw2(@RequestParam("idApp") int idApp) {
		/* 53 */ ModelAndView view = new ModelAndView("payworks/comercioPAYW2");
		/* 54 */ view.addObject("mensajeError", "Servicio disponible en el horario de 2:00 hrs a 22:00 hrs.");
		/* 55 */ view.addObject("idApp", Integer.valueOf(idApp));
		/* 56 */ return view;
	}

	@RequestMapping(value = { "/exito" }, method = { RequestMethod.GET })
	public ModelAndView exito(@RequestParam("idApp") int idApp) {
		/* 61 */ ModelAndView view = new ModelAndView("payworks/exito");
		/* 62 */ SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		/* 63 */ view.addObject("mensajeError",
				"<tr><td colspan='2'>Estimado usuario, su transferencia ha sido exitosa.</td></tr><tr><tr/><tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Fecha:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+

						/* 65 */ DFormat.format(new Date()) + "</td></tr>" +
						/* 66 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Referencia:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+ "111111111" + "</td></tr>" +

						/* 68 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>N&uacute;mero de Autorizaci&oacute;n:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+ "0000123" + "</td></tr>" +
						/* 69 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Folio MC:</td><td style='border-bottom-style: double;padding-left: 10px;'> "
						+ "123456" + "</td></tr>" +
						/* 70 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Importe Transferido:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
						+ "123.00" + "</td></tr>" +
						/* 71 */ "<tr><td style='text-align:right;font-weight: bold;border-bottom-style: double;'>Comision:</td><td style='border-bottom-style: double;padding-left: 10px;'> $"
						+ "34.00" + "</td></tr>" +
						/* 72 */ "<tr><td colspan='2'>Para aclaraciones marque 01-800-925-5001." + "</td></tr>");

		/* 76 */ view.addObject("idApp", Integer.valueOf(idApp));
		/* 77 */ return view;
	}

	@RequestMapping(value = { "/send3ds" }, method = { RequestMethod.GET })
	public ModelAndView ejemplosend() {
		/* 89 */ ModelAndView view = new ModelAndView("ejemplosend");

		/* 91 */ return view;
	}

	@RequestMapping(value = { "/updateSMS" }, method = { RequestMethod.GET })
	public ModelAndView actualizauser(@RequestParam long idUser) {
		/* 96 */ ModelAndView view = new ModelAndView("ejemplosend");
		/* 97 */ this.mcH2HService.updateTranfers(idUser);
		/* 98 */ return view;
	}

	@RequestMapping(value = { "/login" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public String login() {
		/* 103 */ this.mcH2HService.login();
		/* 104 */ return "login";
	}

	@RequestMapping(value = { "/logout" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public String logout() {
		/* 109 */ this.mcH2HService.logout();
		/* 110 */ return "login";
	}

	@RequestMapping(value = { "/toAccount" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse getToAccount(@RequestParam("idUsuario") long idUsuario,
			@RequestParam("idioma") String idioma) {
		/* 122 */ return this.mcH2HService.getToAccount(idUsuario, idioma);
	}

	@RequestMapping(value = { "/enqueueAccountSignUp" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse EnqueueAccountSignUp(@RequestBody MCAccount mcAccount) {
		/* 131 */ return this.mcH2HService.EnqueueAccountSignUp(mcAccount);
	}

	@RequestMapping(value = { "/transactionEnquiry" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public TransactionEnquiryResponse TransactionEnquiry() {
		/* 139 */ String transactionReference = "7";
		/* 140 */ String idioma = "es";
		/* 141 */ return this.mcH2HService.TransactionEnquiry(transactionReference, idioma);
	}

	@RequestMapping(value = { "/enqueuePayment" }, method = { RequestMethod.POST })
	public ModelAndView EnqueuePayment(@RequestParam String concept, @RequestParam long idUser,
			@RequestParam long idCard, @RequestParam long accountId, @RequestParam double amount,
			@RequestParam String idioma) {
		/* 154 */ PaymentRequest payment = new PaymentRequest();
		/* 155 */ payment.setAccountId(accountId);
		/* 156 */ payment.setAmount(amount);
		/* 157 */ payment.setConcept(concept);
		/* 158 */ payment.setIdCard(idCard);
		/* 159 */ payment.setIdioma(idioma);
		/* 160 */ payment.setIdUser(idUser);
		/* 161 */ return this.mcH2HService.send3ds(payment);
	}

	@RequestMapping(value = { "/comerciofin" }, method = { RequestMethod.GET })
	public ModelAndView comerciofin() {
		/* 197 */ return this.mcH2HService.comercioFin(new PaymentRequest());
	}

	@RequestMapping(value = { "/comercio-con" }, method = { RequestMethod.POST })
	public ModelAndView respuestaProsa(@RequestParam String EM_Response, @RequestParam String EM_Total,
			@RequestParam String EM_OrderID, @RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum, @RequestParam String EM_Auth,
			@RequestParam String EM_Digest, @RequestParam String cc_numberback) {
		/* 223 */ LOGGER.debug(EM_Response);
		/* 224 */ LOGGER.debug(EM_Total);
		/* 225 */ LOGGER.debug(EM_OrderID);
		/* 226 */ LOGGER.debug(EM_Merchant);
		/* 227 */ LOGGER.debug(EM_Store);
		/* 228 */ LOGGER.debug(EM_Term);
		/* 229 */ LOGGER.debug(EM_RefNum);
		/* 230 */ LOGGER.debug(EM_Auth);
		/* 231 */ LOGGER.debug(EM_Digest);
		/* 232 */ LOGGER.debug(cc_numberback);

		/* 234 */ return this.mcH2HService.ProsaResponse(EM_Response, EM_Total, EM_OrderID, EM_Merchant, EM_Store,
				EM_Term, EM_RefNum, EM_Auth, EM_Digest, cc_numberback);
	}

	@RequestMapping(value = { "/transactions" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse getTransactions(@RequestParam("idUsuario") long idUsuario,
			@RequestParam("idioma") String idioma) {
		/* 257 */ return this.mcH2HService.getToAccount(idUsuario, idioma);
	}

	@RequestMapping(value = { "/bankCodes" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public BankCodesResponse getBankCodes() {
		/* 263 */ return this.mcH2HService.getBankCodes();
	}

	@RequestMapping(value = { "/updateAccount" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse updateAccount(@RequestBody UpdateAccountRequest request) {
		/* 270 */ return this.mcH2HService.updateAccount(request);
	}

	@RequestMapping(value = { "/deleteAccount" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse deleteAccount(@RequestParam("idUsuario") long idUsuario,
			@RequestParam("idAccount") long idAccount, @RequestParam("idioma") String idioma) {
		/* 277 */ return this.mcH2HService.deleteAccount(idUsuario, idAccount, idioma);
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/controller/MCH2HCONTROLLER.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */