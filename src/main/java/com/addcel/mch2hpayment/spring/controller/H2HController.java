package com.addcel.mch2hpayment.spring.controller;

import com.addcel.mch2hpayment.client.model.TransactionEnquiryResponse;
import com.addcel.mch2hpayment.spring.model.BankCodesResponse;
import com.addcel.mch2hpayment.spring.model.MCAccount;
import com.addcel.mch2hpayment.spring.model.MCSignUpResponse;
import com.addcel.mch2hpayment.spring.model.UpdateAccountRequest;
import com.addcel.mch2hpayment.spring.services.H2HService;
import com.addcel.mch2hpayment.spring.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class H2HController {
	/* 25 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.controller.H2HController.class);

	@Autowired
	private H2HService H2HSer;

	@Autowired
	private PaymentService service;

	@RequestMapping(value = { "{idApp}/toAccount" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse getToAccount(@PathVariable("idApp") int idApp, @RequestParam("idUsuario") long idUsuario,
			@RequestParam("idioma") String idioma) {
		/* 41 */ if (this.service.isActive(Integer.valueOf(idApp))) {
			/* 42 */ return this.H2HSer.getToAccount(idUsuario, idioma, idApp);
		}
		/* 44 */ MCSignUpResponse error = new MCSignUpResponse();
		/* 45 */ error.setIdError(401);
		/* 46 */ error.setMensajeError("No estas autorizado para realizar la operacion");
		/* 47 */ return error;
	}

	@RequestMapping(value = { "{idApp}/enqueueAccountSignUp" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse EnqueueAccountSignUp(@PathVariable("idApp") int idApp, @RequestBody MCAccount mcAccount) {
		/* 56 */ if (this.service.isActive(Integer.valueOf(idApp))) {
			/* 57 */ return this.H2HSer.EnqueueAccountSignUp(mcAccount, idApp);
		}
		/* 59 */ MCSignUpResponse error = new MCSignUpResponse();
		/* 60 */ error.setIdError(401);
		/* 61 */ error.setMensajeError("No estas autorizado para realizar la operacion");
		/* 62 */ return error;
	}

	@RequestMapping(value = { "{idApp}/transactionEnquiry" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public TransactionEnquiryResponse TransactionEnquiry(@PathVariable("idApp") int idApp) {
		/* 71 */ String transactionReference = "7";
		/* 72 */ String idioma = "es";
		/* 73 */ return this.H2HSer.TransactionEnquiry(transactionReference, idioma, idApp);
	}

	@RequestMapping(value = { "{idApp}/bankCodes" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public BankCodesResponse getBankCodes(@PathVariable("idApp") int idApp) {
		/* 79 */ if (this.service.isActive(Integer.valueOf(idApp))) {
			/* 80 */ return this.H2HSer.getBankCodes(idApp);
		}
		/* 82 */ BankCodesResponse error = new BankCodesResponse();
		/* 83 */ error.setIdError(401);
		/* 84 */ error.setMensajeError("No estas autorizado para realizar la operacion");
		/* 85 */ return error;
	}

	@RequestMapping(value = { "{idApp}/updateAccount" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse updateAccount(@PathVariable("idApp") int idApp, @RequestBody UpdateAccountRequest request) {
		/* 92 */ if (this.service.isActive(Integer.valueOf(idApp))) {
			/* 93 */ return this.H2HSer.updateAccount(request, idApp);
		}
		/* 95 */ MCSignUpResponse error = new MCSignUpResponse();
		/* 96 */ error.setIdError(401);
		/* 97 */ error.setMensajeError("No estas autorizado para realizar la operacion");
		/* 98 */ return error;
	}

	@RequestMapping(value = { "{idApp}/deleteAccount" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public MCSignUpResponse deleteAccount(@PathVariable("idApp") int idApp, @RequestParam("idUsuario") long idUsuario,
			@RequestParam("idAccount") long idAccount, @RequestParam("idioma") String idioma) {
		/* 105 */ if (this.service.isActive(Integer.valueOf(idApp))) {
			/* 106 */ return this.H2HSer.deleteAccount(idUsuario, idAccount, idioma, idApp);
		}
		/* 108 */ MCSignUpResponse error = new MCSignUpResponse();
		/* 109 */ error.setIdError(401);
		/* 110 */ error.setMensajeError("No estas autorizado para realizar la operacion");
		/* 111 */ return error;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/controller/H2HController.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */