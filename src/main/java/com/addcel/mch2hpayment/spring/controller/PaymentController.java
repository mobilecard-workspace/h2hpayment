package com.addcel.mch2hpayment.spring.controller;

import com.addcel.mch2hpayment.spring.model.BP3DSRequest;
import com.addcel.mch2hpayment.spring.model.BPPaymentRequest;
import com.addcel.mch2hpayment.spring.model.BPPaymentResponse;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.addcel.mch2hpayment.spring.model.Token;
import com.addcel.mch2hpayment.spring.services.PaymentService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {
	private static final String PATH_GET_TOKEN = "/{idApp}/{idioma}/getToken";
	private static final String PROCESA_PAGO = "{idApp}/{idPais}/{idioma}/{accountId}/pagoBP";
	private static final String PROCESA_PAGO_BP_3DS = "{idApp}/{idPais}/{idioma}/pago3dsBP";
	private static final String HEADER_TOKEN = "Authorization";
	private static final String HEADER_PROFILE = "Profile";
	@Autowired
	private PaymentService service;

	@RequestMapping(value = { "/{idApp}/{idioma}/getToken" }, method = { RequestMethod.POST }, headers = {
			"Content-Type=application/json" })
	public ResponseEntity<Token> getToken(@PathVariable Integer idApp, @PathVariable String idioma,
			HttpServletRequest req) {
		try {
			/* 43 */ if (req.getHeader("Authorization").isEmpty() && req.getHeader("Profile").isEmpty()) {
				/* 44 */ return new ResponseEntity(
						new McBaseResponse(-1, "No estas autorizado para realizar esta operacion."),
						HttpStatus.UNAUTHORIZED);
			}
			/* 46 */ return new ResponseEntity(this.service.getToken(req.getHeader("Authorization"),
					req.getHeader("Profile"), /* 47 */ req.getSession().getId(), req.getRemoteAddr(), idApp),
					HttpStatus.OK);
		}
		/* 49 */ catch (Exception e) {
			/* 50 */ e.printStackTrace();
			/* 51 */ return new ResponseEntity(
					new McBaseResponse(-1, "Error interno, favor de contactar a soporte@addcel.com"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = { "{idApp}/{idPais}/{idioma}/{accountId}/pagoBP" }, method = { RequestMethod.POST })
	public ResponseEntity<Object> paymentBillPocket(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @PathVariable String accountId, @RequestBody BPPaymentRequest data,
			HttpServletRequest req) {
		/* 58 */ ResponseEntity<Object> response = null;
		/* 59 */ BPPaymentResponse authorization = null;

		try {
			/* 62 */ if (this.service.validaToken(req.getHeader("Authorization"), req.getRemoteAddr(),
					req.getSession().getId(), data, accountId, idApp.intValue(), idPais.intValue(), idioma)
					&& this.service.isActive(idApp)) {
				/* 63 */ authorization = this.service.processPayment(idApp, idPais, idioma, data);
				/* 64 */ response = new ResponseEntity(authorization, HttpStatus.OK);
			} else {
				/* 66 */ McBaseResponse error = new McBaseResponse();
				/* 67 */ error.setIdError(-10);
				/* 68 */ error.setMensajeError("No esta autorizado para realizar esta operacion.");
				/* 69 */ response = new ResponseEntity(error, HttpStatus.UNAUTHORIZED);
			}
			/* 71 */ } catch (Exception e) {
			/* 72 */ McBaseResponse error = new McBaseResponse();
			/* 73 */ error.setIdError(-10);
			/* 74 */ error.setMensajeError("ERROR: " + e.getLocalizedMessage());
			/* 75 */ response = new ResponseEntity(error, HttpStatus.UNAUTHORIZED);
			/* 76 */ e.printStackTrace();
		}
		/* 78 */ return response;
	}

	@RequestMapping(value = { "{idApp}/{idPais}/{idioma}/pago3dsBP" }, method = { RequestMethod.POST })
	public ResponseEntity<Object> paymentBillPocket3DS(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestBody BP3DSRequest data, HttpServletRequest req) {
		/* 84 */ ResponseEntity<Object> response = null;
		/* 85 */ BPPaymentResponse authorization = null;
		try {
			/* 87 */ authorization = this.service.processPayment3DSBP(idApp, idPais, idioma, data);
			/* 88 */ response = new ResponseEntity(authorization, HttpStatus.OK);
			/* 89 */ } catch (Exception e) {
			/* 90 */ McBaseResponse error = new McBaseResponse();
			/* 91 */ error.setIdError(-10);
			/* 92 */ error.setMensajeError("ERROR: " + e.getLocalizedMessage());
			/* 93 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
			/* 94 */ e.printStackTrace();
		}
		/* 96 */ return response;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/controller/PaymentController.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */