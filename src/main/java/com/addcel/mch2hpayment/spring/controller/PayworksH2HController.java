package com.addcel.mch2hpayment.spring.controller;

import com.addcel.mch2hpayment.spring.model.PaymentRequest;
import com.addcel.mch2hpayment.spring.services.PayworksH2HService;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class PayworksH2HController {
	/* 23 */ private static final Logger LOGGER = LoggerFactory
			.getLogger(com.addcel.mch2hpayment.spring.controller.PayworksH2HController.class);

	@Autowired
	private PayworksH2HService payService;

	@RequestMapping(value = { "{idApp}/payworks/enqueuePayment" }, method = { RequestMethod.POST })
	public ModelAndView EnqueuePayment(@PathVariable int idApp, @RequestParam String concept, @RequestParam long idUser,
			@RequestParam long idCard, @RequestParam long accountId, @RequestParam double amount,
			@RequestParam(required = false) double comision, @RequestParam String idioma,
			@RequestParam(required = false) Double lat, @RequestParam(required = false) Double lon,
			HttpServletRequest request) {
		/* 38 */ if (lat == null)
			/* 39 */ lat = Double.valueOf(0.0D);
		/* 40 */ if (lon == null)
			/* 41 */ lon = Double.valueOf(0.0D);
		/* 42 */ PaymentRequest payment = new PaymentRequest();
		/* 43 */ payment.setAccountId(accountId);
		/* 44 */ payment.setAmount(amount);
		/* 45 */ payment.setConcept(concept);
		/* 46 */ payment.setIdCard(idCard);
		/* 47 */ payment.setIdioma(idioma);
		/* 48 */ payment.setIdUser(idUser);
		/* 49 */ payment.setComision(comision);

		/* 51 */ LOGGER.debug("Datos Recibidos:  RemoteAddr: " + request.getRemoteAddr() + " accountId: " + accountId
				+ " amount: " + amount + " concept" + concept + " idCard: " + idCard + " idioma: " + idioma + " idUser"
				+ idUser + " comision" + comision + " idApp" + idApp);
		/* 52 */ return this.payService.send3DSBanorte(payment, idApp, lat.doubleValue(), lon.doubleValue());
	}

	@RequestMapping({ "payworks/payworksRec3DRespuesta" })
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
		/* 60 */ LOGGER.info("Respuesta 3DS Banorte");
		/* 61 */ LOGGER.debug("Resposnse 3ds Banorte: " + cadena);
		/* 62 */ return this.payService.payworksRec3DRespuesta(cadena, modelo);
	}

	@RequestMapping(value = { "payworks/payworks2RecRespuesta" }, method = { RequestMethod.GET })
	public ModelAndView payworks2RecRespuesta(@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW,
			@RequestParam(required = false) String BANCO_EMISOR, @RequestParam String FECHA_REQ_CTE,
			@RequestParam(required = false) String CODIGO_AUT, @RequestParam String ID_AFILIACION,
			@RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		/* 75 */ return this.payService.payworks2RecRespuesta(NUMERO_CONTROL, REFERENCIA, FECHA_RSP_CTE, TEXTO,
				RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, BANCO_EMISOR, ID_AFILIACION,
				TIPO_TARJETA, MARCA_TARJETA, modelo, request);
	}

	@RequestMapping(value = { "transferResponse" }, method = { RequestMethod.POST })
	public ModelAndView transferResponse(@RequestParam String json) {
		/* 80 */ ModelAndView view = new ModelAndView("transfers_response_pay");
		/* 81 */ view.addObject("json", json);
		/* 82 */ return view;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/controller/PayworksH2HController.class Java compiler
 * version: 8 (52.0) JD-Core Version: 1.1.3
 */