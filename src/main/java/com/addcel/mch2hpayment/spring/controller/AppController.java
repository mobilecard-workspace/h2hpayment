package com.addcel.mch2hpayment.spring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.mch2hpayment.mybatis.model.vo.GenericResponse;

@RestController
public class AppController {

	@RequestMapping(value = { "/health" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	public GenericResponse GenericResponse() {
		GenericResponse response = new GenericResponse();
		response.setIdError(401);
		response.setMensajeError("No estas autorizado para realizar la operacion");
		return response;
	}

}
