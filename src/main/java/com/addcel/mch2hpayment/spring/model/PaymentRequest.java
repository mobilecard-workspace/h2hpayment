package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequest {
	private long idUser;
	private long idCard;
	private long accountId;
	private double amount;
	private String concept;
	private String idioma;
	private String authProcom;
	private String refProcom;
	private double comision;

	public void setComision(double comision) {
		/* 23 */ this.comision = comision;
	}

	public double getComision() {
		/* 27 */ return this.comision;
	}

	public void setAuthProcom(String authProcom) {
		/* 31 */ this.authProcom = authProcom;
	}

	public String getAuthProcom() {
		/* 35 */ return this.authProcom;
	}

	public void setRefProcom(String refProcom) {
		/* 39 */ this.refProcom = refProcom;
	}

	public String getRefProcom() {
		/* 43 */ return this.refProcom;
	}

	public long getIdUser() {
		/* 47 */ return this.idUser;
	}

	public void setIdUser(long idUser) {
		/* 51 */ this.idUser = idUser;
	}

	public long getIdCard() {
		/* 55 */ return this.idCard;
	}

	public void setIdCard(long idCard) {
		/* 59 */ this.idCard = idCard;
	}

	public long getAccountId() {
		/* 63 */ return this.accountId;
	}

	public void setAccountId(long accountId) {
		/* 67 */ this.accountId = accountId;
	}

	public double getAmount() {
		/* 71 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 75 */ this.amount = amount;
	}

	public String getConcept() {
		/* 79 */ return this.concept;
	}

	public void setConcept(String concept) {
		/* 83 */ this.concept = concept;
	}

	public String getIdioma() {
		/* 87 */ return this.idioma;
	}

	public void setIdioma(String idioma) {
		/* 91 */ this.idioma = idioma;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/PaymentRequest.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */