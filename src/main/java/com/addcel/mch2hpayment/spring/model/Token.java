package com.addcel.mch2hpayment.spring.model;

import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token extends McBaseResponse {
	private String token;
	private String accountId;
	private boolean secure;

	public String getAccountId() {
		/* 15 */ return this.accountId;
	}

	public void setAccountId(String accountId) {
		/* 19 */ this.accountId = accountId;
	}

	public String getToken() {
		/* 23 */ return this.token;
	}

	public void setToken(String token) {
		/* 27 */ this.token = token;
	}

	public boolean isSecure() {
		/* 31 */ return this.secure;
	}

	public void setSecure(boolean secure) {
		/* 35 */ this.secure = secure;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/Token.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */