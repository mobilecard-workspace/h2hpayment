/*    */ package com.addcel.mch2hpayment.spring.model;
/*    */ 
/*    */ import com.addcel.mch2hpayment.client.model.AccountRecord;
/*    */ import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @JsonIgnoreProperties(ignoreUnknown = true)
/*    */ public class MCAccount
/*    */   extends AccountRecord
/*    */ {
/*    */   private long idUsuario;
/*    */   private String idioma;
/*    */   private String alias;
/*    */   
/*    */   public void setAlias(String alias) {
/* 18 */     this.alias = alias;
/*    */   }
/*    */   
/*    */   public String getAlias() {
/* 22 */     return this.alias;
/*    */   }
/*    */   
/*    */   public void setIdUsuario(long idUsuario) {
/* 26 */     this.idUsuario = idUsuario;
/*    */   }
/*    */   
/*    */   public long getIdUsuario() {
/* 30 */     return this.idUsuario;
/*    */   }
/*    */   
/*    */   public void setIdioma(String idioma) {
/* 34 */     this.idioma = idioma;
/*    */   }
/*    */   
/*    */   public String getIdioma() {
/* 38 */     return this.idioma;
/*    */   }
/*    */ }


/* Location:              /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel/mch2hpayment/spring/model/MCAccount.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */