package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BP3DSRequest {
	private Long idUser;
	private Long idCard;
	private Long accountId;
	private Double amount;
	private String concept;
	private Double comision;
	private String imei;
	private String software;
	private String model;
	private Double lat;
	private Double lon;
	private String referencia;
	private String emisor;
	private String operation;
	private int status;
	private long idBitacora;

	public void setIdBitacora(long idBitacora) {
		/* 32 */ this.idBitacora = idBitacora;
	}

	public long getIdBitacora() {
		/* 36 */ return this.idBitacora;
	}

	public Long getIdUser() {
		/* 40 */ return this.idUser;
	}

	public void setIdUser(Long idUser) {
		/* 43 */ this.idUser = idUser;
	}

	public Long getIdCard() {
		/* 46 */ return this.idCard;
	}

	public void setIdCard(Long idCard) {
		/* 49 */ this.idCard = idCard;
	}

	public Long getAccountId() {
		/* 52 */ return this.accountId;
	}

	public void setAccountId(Long accountId) {
		/* 55 */ this.accountId = accountId;
	}

	public Double getAmount() {
		/* 58 */ return this.amount;
	}

	public void setAmount(Double amount) {
		/* 61 */ this.amount = amount;
	}

	public String getConcept() {
		/* 64 */ return this.concept;
	}

	public void setConcept(String concept) {
		/* 67 */ this.concept = concept;
	}

	public Double getComision() {
		/* 70 */ return this.comision;
	}

	public void setComision(Double comision) {
		/* 73 */ this.comision = comision;
	}

	public String getImei() {
		/* 76 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 79 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 82 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 85 */ this.software = software;
	}

	public String getModel() {
		/* 88 */ return this.model;
	}

	public void setModel(String model) {
		/* 91 */ this.model = model;
	}

	public Double getLat() {
		/* 94 */ return this.lat;
	}

	public void setLat(Double lat) {
		/* 97 */ this.lat = lat;
	}

	public Double getLon() {
		/* 100 */ return this.lon;
	}

	public void setLon(Double lon) {
		/* 103 */ this.lon = lon;
	}

	public String getReferencia() {
		/* 106 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 109 */ this.referencia = referencia;
	}

	public String getEmisor() {
		/* 112 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 115 */ this.emisor = emisor;
	}

	public String getOperation() {
		/* 118 */ return this.operation;
	}

	public void setOperation(String operation) {
		/* 121 */ this.operation = operation;
	}

	public int getStatus() {
		/* 124 */ return this.status;
	}

	public void setStatus(int status) {
		/* 127 */ this.status = status;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/BP3DSRequest.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */