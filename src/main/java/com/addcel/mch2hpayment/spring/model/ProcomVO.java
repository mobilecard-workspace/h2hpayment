package com.addcel.mch2hpayment.spring.model;

public class ProcomVO {
	private String total;
	private String currency;
	private String address;
	private String orderId;
	private String merchant;
	private String store;
	private String term;
	private String digest;
	private String urlBack;
	private String monto;
	private String idServicio;
	private String nombre;
	private String TDC;
	private String tipoTDC;
	private String mes;
	private String anio;
	private String cc_numberback;
	private String cc_cvv;

	public ProcomVO(String total, String currency, String address, String orderId, String merchant, String store,
			String term, String digest, String urlBack, String nombre, String TDC, String tipoTDC, String mes,
			String anio, String cc_numberback, String cc_cvv) {
		/* 29 */ this.total = total;
		/* 30 */ this.currency = currency;
		/* 31 */ this.address = address;
		/* 32 */ this.orderId = orderId;
		/* 33 */ this.merchant = merchant;
		/* 34 */ this.store = store;
		/* 35 */ this.term = term;
		/* 36 */ this.digest = digest;
		/* 37 */ this.urlBack = urlBack;
		/* 38 */ this.nombre = nombre;
		/* 39 */ this.TDC = TDC;
		/* 40 */ this.tipoTDC = tipoTDC;
		/* 41 */ this.mes = mes;
		/* 42 */ this.anio = anio;
		/* 43 */ this.cc_numberback = cc_numberback;
		/* 44 */ this.cc_cvv = cc_cvv;
	}

	public ProcomVO() {
	}

	public void setCc_cvv(String cc_cvv) {
		/* 53 */ this.cc_cvv = cc_cvv;
	}

	public String getCc_cvv() {
		/* 57 */ return this.cc_cvv;
	}

	public String getNombre() {
		/* 61 */ return this.nombre;
	}

	public void setNombre(String nombre) {
		/* 65 */ this.nombre = nombre;
	}

	public String getTDC() {
		/* 69 */ return this.TDC;
	}

	public void setTDC(String tDC) {
		/* 73 */ this.TDC = tDC;
	}

	public String getTipoTDC() {
		/* 77 */ return this.tipoTDC;
	}

	public void setTipoTDC(String tipoTDC) {
		/* 81 */ this.tipoTDC = tipoTDC;
	}

	public String getMes() {
		/* 85 */ return this.mes;
	}

	public void setMes(String mes) {
		/* 89 */ this.mes = mes;
	}

	public String getAnio() {
		/* 93 */ return this.anio;
	}

	public void setAnio(String anio) {
		/* 97 */ this.anio = anio;
	}

	public String getCc_numberback() {
		/* 101 */ return this.cc_numberback;
	}

	public void setCc_numberback(String cc_numberback) {
		/* 105 */ this.cc_numberback = cc_numberback;
	}

	public String getTotal() {
		/* 109 */ return this.total;
	}

	public void setTotal(String total) {
		/* 112 */ this.total = total;
	}

	public String getCurrency() {
		/* 115 */ return this.currency;
	}

	public void setCurrency(String currency) {
		/* 118 */ this.currency = currency;
	}

	public String getAddress() {
		/* 121 */ return this.address;
	}

	public void setAddress(String address) {
		/* 124 */ this.address = address;
	}

	public String getOrderId() {
		/* 127 */ return this.orderId;
	}

	public void setOrderId(String orderId) {
		/* 130 */ this.orderId = orderId;
	}

	public String getMerchant() {
		/* 133 */ return this.merchant;
	}

	public void setMerchant(String merchant) {
		/* 136 */ this.merchant = merchant;
	}

	public String getStore() {
		/* 139 */ return this.store;
	}

	public void setStore(String store) {
		/* 142 */ this.store = store;
	}

	public String getTerm() {
		/* 145 */ return this.term;
	}

	public void setTerm(String term) {
		/* 148 */ this.term = term;
	}

	public String getDigest() {
		/* 151 */ return this.digest;
	}

	public void setDigest(String digest) {
		/* 154 */ this.digest = digest;
	}

	public String getUrlBack() {
		/* 157 */ return this.urlBack;
	}

	public void setUrlBack(String urlBack) {
		/* 160 */ this.urlBack = urlBack;
	}

	public String getMonto() {
		/* 163 */ return this.monto;
	}

	public void setMonto(String monto) {
		/* 166 */ this.monto = monto;
	}

	public String getIdServicio() {
		/* 169 */ return this.idServicio;
	}

	public void setIdServicio(String idServicio) {
		/* 172 */ this.idServicio = idServicio;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/ProcomVO.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */