package com.addcel.mch2hpayment.spring.model;

public class RespuestaAmex {
	private String transaction;
	private String code;
	private String dsc;
	private String error;
	private String errorDsc;

	public String getCode() {
		/* 17 */ return this.code;
	}

	public void setCode(String code) {
		/* 21 */ this.code = code;
	}

	public String getDsc() {
		/* 25 */ return this.dsc;
	}

	public void setDsc(String dsc) {
		/* 29 */ this.dsc = dsc;
	}

	public String getTransaction() {
		/* 33 */ return this.transaction;
	}

	public void setTransaction(String transaction) {
		/* 37 */ this.transaction = transaction;
	}

	public String getError() {
		/* 41 */ return this.error;
	}

	public void setError(String error) {
		/* 45 */ this.error = error;
	}

	public String getErrorDsc() {
		/* 49 */ return this.errorDsc;
	}

	public void setErrorDsc(String errorDsc) {
		/* 53 */ this.errorDsc = errorDsc;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/RespuestaAmex.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */