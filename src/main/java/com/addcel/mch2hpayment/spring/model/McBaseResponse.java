package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class McBaseResponse {
	private int idError;
	private String mensajeError;

	public McBaseResponse() {
	}

	public McBaseResponse(int idError, String mensajeError) {
		/* 16 */ this.idError = idError;
		/* 17 */ this.mensajeError = mensajeError;
	}

	public int getIdError() {
		/* 21 */ return this.idError;
	}

	public void setIdError(int idError) {
		/* 24 */ this.idError = idError;
	}

	public String getMensajeError() {
		/* 27 */ return this.mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		/* 30 */ this.mensajeError = mensajeError;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/McBaseResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */