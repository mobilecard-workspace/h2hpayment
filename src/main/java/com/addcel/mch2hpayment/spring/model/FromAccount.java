package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FromAccount {
	private long id;
	private String alias;
	private double comision_fija;
	private double comision_porcentaje;
	private String telefono;
	private String correo;

	public void setCorreo(String correo) {
		/* 21 */ this.correo = correo;
	}

	public String getCorreo() {
		/* 25 */ return this.correo;
	}

	public void setTelefono(String telefono) {
		/* 29 */ this.telefono = telefono;
	}

	public String getTelefono() {
		/* 33 */ return this.telefono;
	}

	public double getComision_fija() {
		/* 37 */ return this.comision_fija;
	}

	public void setComision_fija(double comision_fija) {
		/* 43 */ this.comision_fija = comision_fija;
	}

	public double getComision_porcentaje() {
		/* 49 */ return this.comision_porcentaje;
	}

	public void setComision_porcentaje(double comision_porcentaje) {
		/* 55 */ this.comision_porcentaje = comision_porcentaje;
	}

	public long getId() {
		/* 61 */ return this.id;
	}

	public void setId(long id) {
		/* 65 */ this.id = id;
	}

	public String getAlias() {
		/* 69 */ return this.alias;
	}

	public void setAlias(String alias) {
		/* 73 */ this.alias = alias;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/FromAccount.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */