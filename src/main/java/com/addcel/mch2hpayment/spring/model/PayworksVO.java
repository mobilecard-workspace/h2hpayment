package com.addcel.mch2hpayment.spring.model;

public class PayworksVO {
	private String Reference3D;
	private String MerchantId;
	private String ForwardPath;
	private String Expires;
	private String Total;
	private String Card;
	private String CardType;

	public String getReference3D() {
		/* 18 */ return this.Reference3D;
	}

	public void setReference3D(String reference3d) {
		/* 22 */ this.Reference3D = reference3d;
	}

	public String getMerchantId() {
		/* 26 */ return this.MerchantId;
	}

	public void setMerchantId(String merchantId) {
		/* 30 */ this.MerchantId = merchantId;
	}

	public String getForwardPath() {
		/* 34 */ return this.ForwardPath;
	}

	public void setForwardPath(String forwardPath) {
		/* 38 */ this.ForwardPath = forwardPath;
	}

	public String getExpires() {
		/* 42 */ return this.Expires;
	}

	public void setExpires(String expires) {
		/* 46 */ this.Expires = expires;
	}

	public String getTotal() {
		/* 50 */ return this.Total;
	}

	public void setTotal(String total) {
		/* 54 */ this.Total = total;
	}

	public String getCard() {
		/* 58 */ return this.Card;
	}

	public void setCard(String card) {
		/* 62 */ this.Card = card;
	}

	public String getCardType() {
		/* 66 */ return this.CardType;
	}

	public void setCardType(String cardType) {
		/* 70 */ this.CardType = cardType;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/PayworksVO.class Java compiler version: 8 (52.0)
 * JD-Core Version: 1.1.3
 */