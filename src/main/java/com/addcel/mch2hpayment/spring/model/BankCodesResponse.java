package com.addcel.mch2hpayment.spring.model;

import com.addcel.mch2hpayment.mybatis.model.vo.BankCodes;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankCodesResponse extends McBaseResponse {
	private List<BankCodes> banks;

	public void setBanks(List<BankCodes> banks) {
		/* 18 */ this.banks = banks;
	}

	public List<BankCodes> getBanks() {
		/* 22 */ return this.banks;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/BankCodesResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */