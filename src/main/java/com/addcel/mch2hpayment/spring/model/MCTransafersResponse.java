package com.addcel.mch2hpayment.spring.model;

import com.addcel.mch2hpayment.spring.model.McBaseResponse;

public class MCTransafersResponse extends McBaseResponse {
	private double montoTransfer;
	private double comision;
	private String authProcom;
	private String referenceBanorte;
	private String refProcom;
	private String tarjeta;
	private String fecha;

	public void setRefProcom(String refProcom) {
		/* 18 */ this.refProcom = refProcom;
	}

	public String getRefProcom() {
		/* 22 */ return this.refProcom;
	}

	public double getMontoTransfer() {
		/* 26 */ return this.montoTransfer;
	}

	public void setMontoTransfer(double montoTransfer) {
		/* 30 */ this.montoTransfer = montoTransfer;
	}

	public double getComision() {
		/* 34 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 38 */ this.comision = comision;
	}

	public String getAuthProcom() {
		/* 42 */ return this.authProcom;
	}

	public void setAuthProcom(String authProcom) {
		/* 46 */ this.authProcom = authProcom;
	}

	public String getReferenceBanorte() {
		/* 50 */ return this.referenceBanorte;
	}

	public void setReferenceBanorte(String referenceBanorte) {
		/* 54 */ this.referenceBanorte = referenceBanorte;
	}

	public String getTarjeta() {
		/* 58 */ return this.tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		/* 62 */ this.tarjeta = tarjeta;
	}

	public String getFecha() {
		/* 66 */ return this.fecha;
	}

	public void setFecha(String fecha) {
		/* 70 */ this.fecha = fecha;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/MCTransafersResponse.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */