package com.addcel.mch2hpayment.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateAccountRequest {
	private long idUsuario;
	private long idAccount;
	private String telefono;
	private String alias;
	private String email;
	private String idioma;

	public long getIdUsuario() {
		/* 20 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 24 */ this.idUsuario = idUsuario;
	}

	public long getIdAccount() {
		/* 28 */ return this.idAccount;
	}

	public void setIdAccount(long idAccount) {
		/* 32 */ this.idAccount = idAccount;
	}

	public String getTelefono() {
		/* 36 */ return this.telefono;
	}

	public void setTelefono(String telefono) {
		/* 40 */ this.telefono = telefono;
	}

	public String getAlias() {
		/* 44 */ return this.alias;
	}

	public void setAlias(String alias) {
		/* 48 */ this.alias = alias;
	}

	public String getEmail() {
		/* 52 */ return this.email;
	}

	public void setEmail(String email) {
		/* 56 */ this.email = email;
	}

	public void setIdioma(String idioma) {
		/* 60 */ this.idioma = idioma;
	}

	public String getIdioma() {
		/* 64 */ return this.idioma;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/UpdateAccountRequest.class Java compiler version:
 * 8 (52.0) JD-Core Version: 1.1.3
 */