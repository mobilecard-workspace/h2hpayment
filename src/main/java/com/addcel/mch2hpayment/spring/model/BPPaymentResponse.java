package com.addcel.mch2hpayment.spring.model;

public class BPPaymentResponse {
	private Integer status;
	private Integer code;
	private Integer opId;
	private String txnISOCode;
	private String authNumber;
	private String ticketUrl;
	private double amount;
	private String maskedPAN;
	private long dateTime;
	private long idTransaccion;
	private String message;

	public void setCode(Integer code) {
		/* 28 */ this.code = code;
	}

	public Integer getCode() {
		/* 32 */ return this.code;
	}

	public void setMessage(String message) {
		/* 40 */ this.message = message;
	}

	public String getMessage() {
		/* 44 */ return this.message;
	}

	public Integer getStatus() {
		/* 48 */ return this.status;
	}

	public void setStatus(Integer status) {
		/* 52 */ this.status = status;
	}

	public Integer getOpId() {
		/* 56 */ return this.opId;
	}

	public void setOpId(Integer opId) {
		/* 60 */ this.opId = opId;
	}

	public String getTxnISOCode() {
		/* 64 */ return this.txnISOCode;
	}

	public void setTxnISOCode(String txnISOCode) {
		/* 68 */ this.txnISOCode = txnISOCode;
	}

	public String getAuthNumber() {
		/* 72 */ return this.authNumber;
	}

	public void setAuthNumber(String authNumber) {
		/* 76 */ this.authNumber = authNumber;
	}

	public String getTicketUrl() {
		/* 80 */ return this.ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		/* 84 */ this.ticketUrl = ticketUrl;
	}

	public double getAmount() {
		/* 88 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 92 */ this.amount = amount;
	}

	public String getMaskedPAN() {
		/* 96 */ return this.maskedPAN;
	}

	public void setMaskedPAN(String maskedPAN) {
		/* 100 */ this.maskedPAN = maskedPAN;
	}

	public long getDateTime() {
		/* 104 */ return this.dateTime;
	}

	public void setDateTime(long dateTime) {
		/* 108 */ this.dateTime = dateTime;
	}

	public long getIdTransaccion() {
		/* 112 */ return this.idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		/* 116 */ this.idTransaccion = idTransaccion;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/BPPaymentResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */