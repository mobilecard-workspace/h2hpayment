package com.addcel.mch2hpayment.spring.model;

public class BPPaymentRequest {
	private Long idUser;
	private Long idCard;
	private Long accountId;
	private Double amount;
	private String concept;
	private Double comision;
	private Integer idProvider;
	private Integer idProduct;
	private String imei;
	private String software;
	private String model;
	private Double lat;
	private Double lon;
	private String referencia;
	private String emisor;
	private String operation;

	public Long getIdUser() {
		/* 29 */ return this.idUser;
	}

	public void setIdUser(Long idUser) {
		/* 33 */ this.idUser = idUser;
	}

	public Long getIdCard() {
		/* 37 */ return this.idCard;
	}

	public void setIdCard(Long idCard) {
		/* 41 */ this.idCard = idCard;
	}

	public Long getAccountId() {
		/* 45 */ return this.accountId;
	}

	public void setAccountId(Long accountId) {
		/* 49 */ this.accountId = accountId;
	}

	public Double getAmount() {
		/* 53 */ return this.amount;
	}

	public void setAmount(Double amount) {
		/* 57 */ this.amount = amount;
	}

	public String getConcept() {
		/* 61 */ return this.concept;
	}

	public void setConcept(String concept) {
		/* 65 */ this.concept = concept;
	}

	public Double getComision() {
		/* 69 */ return this.comision;
	}

	public void setComision(Double comision) {
		/* 73 */ this.comision = comision;
	}

	public Integer getIdProvider() {
		/* 77 */ return this.idProvider;
	}

	public void setIdProvider(Integer idProvider) {
		/* 81 */ this.idProvider = idProvider;
	}

	public Integer getIdProduct() {
		/* 85 */ return this.idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		/* 89 */ this.idProduct = idProduct;
	}

	public String getImei() {
		/* 93 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 97 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 101 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 105 */ this.software = software;
	}

	public String getModel() {
		/* 109 */ return this.model;
	}

	public void setModel(String model) {
		/* 113 */ this.model = model;
	}

	public Double getLat() {
		/* 117 */ return this.lat;
	}

	public void setLat(Double lat) {
		/* 121 */ this.lat = lat;
	}

	public Double getLon() {
		/* 125 */ return this.lon;
	}

	public void setLon(Double lon) {
		/* 129 */ this.lon = lon;
	}

	public String getReferencia() {
		/* 133 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 137 */ this.referencia = referencia;
	}

	public String getEmisor() {
		/* 141 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 145 */ this.emisor = emisor;
	}

	public String getOperation() {
		/* 149 */ return this.operation;
	}

	public void setOperation(String operation) {
		/* 153 */ this.operation = operation;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/BPPaymentRequest.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */