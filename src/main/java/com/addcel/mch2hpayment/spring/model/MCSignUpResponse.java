package com.addcel.mch2hpayment.spring.model;

import com.addcel.mch2hpayment.spring.model.FromAccount;
import com.addcel.mch2hpayment.spring.model.McBaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MCSignUpResponse extends McBaseResponse {
	/* 15 */ private List<FromAccount> accounts = new ArrayList<>();

	public void setAccounts(List<FromAccount> accounts) {
		/* 19 */ this.accounts = accounts;
	}

	public List<FromAccount> getAccounts() {
		/* 23 */ return this.accounts;
	}
}

/*
 * Location:
 * /home/hhernandez/Digital_Edge/MobileCard/Wars/classes.zip!/classes/com/addcel
 * /mch2hpayment/spring/model/MCSignUpResponse.class Java compiler version: 8
 * (52.0) JD-Core Version: 1.1.3
 */